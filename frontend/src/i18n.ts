import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import resources from './resources/resources.json';



i18n.use(initReactI18next).init({
    resources,
    lng: 'en',

    keySeparator: false, // enable to use keys in the form messages.welcome

    interpolation: {
        escapeValue: false, // react already safe from xss
    },
});

export default i18n;
