import { CardItem, SectionDevisModel } from 'common/src';
import React, { useState } from 'react';
import ListCardPage from './listcardpage';


interface SectionDevisProps {
    baseId: number;
    groupId: number;
}

export default function SectionDeDevisPage(props: SectionDevisProps) {
    const [sectionDevisCard, setSectionDevisCard] = useState([] as CardItem[]);
    const [baseId, setBaseId] = useState(0);

    const loadData = async (baseId: number, groupId: number) => {
        let url = groupId == -1 ? '/api/sectiondevis/base/' + baseId : '/api/sectiondevis/base/' + baseId + '/group/' + groupId;
        const result = await fetch(url, {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values: SectionDevisModel[] = (await result.json() as any[]).map(SectionDevisModel.fromJSON);
            console.log(values);
            let _cards: CardItem[] = [];
            values.forEach((document) => {
                let card: CardItem = {
                    alt: document.Titre,
                    img: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
                    title: document.Titre,
                    creationDate: document.Creation_Date.split('T')[0],
                    description: "",
                    imgPath: document.DocDestinationPath
                };
                _cards.push(card);
            });

            setSectionDevisCard(_cards);
        }
    };

    if (baseId !== props.baseId) {
        console.log(props);
        setBaseId(props.baseId);
        loadData(props.baseId, props.groupId);

    }

    return (
        <>
            <div>
                <ListCardPage cards={sectionDevisCard} xs={12} sm={6} md={4} lg={3} />
            </div>
        </>
    );
}
