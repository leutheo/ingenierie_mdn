import React from 'react';
import HomeCarousel from '../bases/carousel';

export default function HomePage() {

    return (
        <>
            <div >
                <HomeCarousel />
            </div>
            <div id='contenairePage'>
                <p>Sous la supervision du Sous-Ministre Adjoint Infrastructure et Environnement, le Ministère de la Défense Nationale (MDN) gère un parc immobilier composé de quelque 2 100 immeubles,
                13 500 ouvrages (notamment 5 500 km de routes, des jetées, des pistes et des secteurs d’entraînement) et plus de 1 000 parcelles de terrains qui représentent une superficie de 2,2 millions d’hectares.
                    La gestion de ce portefeuille est intégrale, qu’il s’agisse de la réfection, de l’entretien et de la réparation des immeubles existant, des nouveaux projets de construction ou de l’aliénation et de la remise en état d’installations désuètes.</p>
            </div>
        </>
    );
}
