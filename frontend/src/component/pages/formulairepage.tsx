import { CardItem } from 'common/src/enum';
import React, { useEffect, useState } from 'react';
import ListCardPage from './listcardpage';

export default function FormulairePage() {
    const [documentCard, setDocumentCard] = useState([] as CardItem[]);

    useEffect(() => {

        let _cardsTest: CardItem[] = [];
        let cardtest1: CardItem = {
            alt: "example",
            img: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
            title: "Question 1",
            imgPath: "",
            creationDate: '2012-01-09',
            description: "Répondez à chaque question de la section en cochant la case appropriée.Si vous répondez « Oui » à une ou à plus d’une question, vous devez fournir des explications sur ce qui est arrivé dans l’espace prévue. Si vous avez besoin de plus d’espace, annexez une feuille distincte."
        };

        let cardtest2: CardItem = {
            alt: "example",
            img: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
            title: "Question 2",
            creationDate: '2012-01-09',
            imgPath: "",
            description: "Répondez à chaque question de la section en cochant la case appropriée.Si vous répondez « Oui » à une ou à plus d’une question, vous devez fournir des explications sur ce qui est arrivé dans l’espace prévue. Si vous avez besoin de plus d’espace, annexez une feuille distincte."
        };


        _cardsTest.push(cardtest1);
        _cardsTest.push(cardtest2);

        setDocumentCard(_cardsTest);
    }, []);

    return (
        <>
            <div>
                <ListCardPage cards={documentCard} xs={24} sm={24} md={6} lg={6} />
            </div>
        </>
    );
}
