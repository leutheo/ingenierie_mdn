import { CloudDownloadOutlined } from '@ant-design/icons';
import { Card } from 'antd';
import 'antd/dist/antd.css';
import { CardItem } from 'common/src/enum';
import React from 'react';

interface CardItemProps {
    cardItem: CardItem;
}

export default function CardPage(props: CardItemProps) {

    const { Meta } = Card;
    return (
        <Card
            // style={{ width: 300 }}
            cover={
                <img
                    alt={props.cardItem.alt}
                    src={props.cardItem.img}
                />
            }
            actions={[
                <a href={'/data/' + props.cardItem.imgPath} download><CloudDownloadOutlined /></a>

            ]}
        >
            <Meta
                //avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                title={props.cardItem.title}
                description={props.cardItem.description}
            />
            <b>Date de creation:</b> {props.cardItem.creationDate}
        </Card>
    );
}
