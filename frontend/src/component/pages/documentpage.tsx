import { DocumentModel } from 'common';
import { CardItem } from 'common/src/enum';
import React, { useState } from 'react';
import ListCardPage from './listcardpage';

interface DocumentProps {
    baseId: number;
}

export default function DocumentPage(props: DocumentProps) {

    const [documentCard, setDocumentCard] = useState([] as CardItem[]);
    const [baseId, setBaseId] = useState(0);

    const loadData = async (baseId: number) => {
        const result = await fetch('/api/document/base/' + baseId, {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values: DocumentModel[] = (await result.json() as any[]).map(DocumentModel.fromJSON);
            console.log(values);
            let _cards: CardItem[] = [];
            values.forEach((document) => {
                let card: CardItem = {
                    alt: document.Titre,
                    img: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
                    title: document.Titre,
                    creationDate: document.Creation_Date.split('T')[0],
                    description: document.Description,
                    imgPath: document.DocDesitnationPath
                };
                _cards.push(card);
            });

            setDocumentCard(_cards);
        }
    };

    if (baseId !== props.baseId) {
        loadData(props.baseId);
        setBaseId(props.baseId);
    }

    return (
        <>
            <div>
                <ListCardPage cards={documentCard} xs={12} sm={6} md={4} lg={3} />
            </div>
        </>
    );
}
