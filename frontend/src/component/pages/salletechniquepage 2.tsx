import { Col, Row } from 'antd';
import { SalleTechniqueModel } from 'common';
import { CardItem } from 'common/src/enum';
import React, { useEffect, useState } from 'react';
import ListCardPage from './listcardpage';

interface SalleTechniqueProps {
    baseId: number;
}

export default function SalleTechniquePage(props: SalleTechniqueProps) {
    const [lienCard, setLienCard] = useState([] as CardItem[]);
    const [documentCard, setDocumentCard] = useState([] as CardItem[]);

    useEffect(() => {

        loadSalleTechniques(props.baseId);

        let _lientCards: CardItem[] = [];
        let cardtest1: CardItem = {
            alt: "example",
            img: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
            title: "Question 1",
            creationDate: '2012-01-09',
            imgPath: "",
            description: "Répondez à chaque question de la section en cochant la case appropriée.Si vous répondez « Oui » à une ou à plus d’une question, vous devez fournir des explications sur ce qui est arrivé dans l’espace prévue. Si vous avez besoin de plus d’espace, annexez une feuille distincte."
        };

        let cardtest2: CardItem = {
            alt: "example",
            imgPath: "",
            img: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
            title: "Question 2",
            creationDate: '2012-01-09',
            description: "Répondez à chaque question de la section en cochant la case appropriée.Si vous répondez « Oui » à une ou à plus d’une question, vous devez fournir des explications sur ce qui est arrivé dans l’espace prévue. Si vous avez besoin de plus d’espace, annexez une feuille distincte."
        };

        _lientCards.push(cardtest1);
        _lientCards.push(cardtest2);

        setLienCard(_lientCards);
    }, []);

    const loadSalleTechniques = async (baseId: number) => {
        const result = await fetch('/api/salletechnique/base/' + baseId, {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values: SalleTechniqueModel[] = (await result.json() as any[]).map(SalleTechniqueModel.fromJSON);
            console.log(values);
            let _cards: CardItem[] = [];
            values.forEach((document) => {
                let card: CardItem = {
                    alt: "Salle 1",
                    img: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
                    title: "Salle 1",
                    creationDate: document.Creation_Date.split('T')[0],
                    description: "Description",
                    imgPath: ""
                };
                _cards.push(card);
            });

            setDocumentCard(_cards);
        }
    };

    return (
        <>
            <Row>
                <Col span={10}>
                    <h2><b>Liens</b></h2>
                    <ListCardPage cards={lienCard} span={12} />
                </Col>
                <Col span={2} className="m-2"></Col>
                <Col span={10}>
                    <h2><b>Documents</b></h2>
                    <ListCardPage cards={documentCard} span={12} />
                </Col>
            </Row>
        </>
    );
}
