import { CardItem } from 'common/src/enum';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import CardPage from './cardpage';

interface CarProps {
    cards: CardItem[];
    xs: number;
    sm: number;
    md: number;
    lg: number;
}

export default function ListCardPage(props: CarProps) {

    return (
        <Row className="p-2" key="listcard">
            {props.cards.map(_card => {
                return (
                    <Col xs={{ span: props.xs }} sm={{ span: props.sm }} md={{ span: props.md }} lg={{ span: props.lg }} className="p-2" key={"card_" + Math.random() * 10}>
                        <CardPage cardItem={_card} />
                    </Col>
                );
            })}
        </Row>
    );
}
