import { Col, Row } from 'antd';
import { SalleTechniqueModel } from 'common';
import { DocumentTechniqueModel, LinksalleTechniqueModel } from 'common/src';
import { CardItem } from 'common/src/enum';
import React, { useState } from 'react';
import ListCardPage from './listcardpage';

interface SalleTechniqueProps {
    baseId: number;
}

export default function SalleTechniquePage(props: SalleTechniqueProps) {
    const [lienCard, setLienCard] = useState([] as CardItem[]);
    const [documentCard, setDocumentCard] = useState([] as CardItem[]);
    const [baseId, setBaseId] = useState(0);

    const loadData = async (baseId: number) => {
        const result = await fetch('/api/salletechnique/base/' + baseId, {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values: SalleTechniqueModel[] = (await result.json() as any[]).map(SalleTechniqueModel.fromJSON);
            console.log(values);
            let _documentCards: CardItem[] = [];
            let _linkCards: CardItem[] = [];
            values.forEach((salleTechniques: SalleTechniqueModel) => {
                salleTechniques.documentSalleTechniques.forEach((document: DocumentTechniqueModel) => {
                    let card: CardItem = {
                        alt: document.Name,
                        img: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
                        title: document.Name,
                        creationDate: document.Creation_Date.split('T')[0],
                        description: "",
                        imgPath: document.DocDestinationPath
                    };
                    _documentCards.push(card);
                });

                salleTechniques.linkSalleTechniques.forEach((link: LinksalleTechniqueModel) => {
                    let card: CardItem = {
                        alt: link.Name,
                        img: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
                        title: link.Name,
                        creationDate: link.Creation_Date.split('T')[0],
                        description: "",
                        imgPath: link.DocDestinationPath
                    };
                    _linkCards.push(card);
                });


            });

            setDocumentCard(_documentCards);
            setLienCard(_linkCards);
        }
    };

    if (baseId !== props.baseId) {
        loadData(props.baseId);
        setBaseId(props.baseId);
    }

    return (
        <>
            <Row>
                <Col span={10}>
                    <h2><b>Liens</b></h2>
                    <ListCardPage cards={lienCard} xs={12} sm={6} md={6} lg={6} />
                </Col>
                <Col span={2} className="m-2"></Col>
                <Col span={10}>
                    <h2><b>Documents</b></h2>
                    <ListCardPage cards={documentCard} xs={12} sm={6} md={6} lg={6} />
                </Col>
            </Row>
        </>
    );
}
