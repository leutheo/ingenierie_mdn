import { Layout, Menu } from 'antd';
import { MenuInfo } from 'rc-menu/lib/interface';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';


type MenugaucheProps = {
    onclick: (menu: MenuInfo) => void;
};
const { SubMenu } = Menu;
const { Sider } = Layout;

export default function Menugauche(props: MenugaucheProps) {
    const { t } = useTranslation();
    const [state, setState] = useState({
        collapsed: false,
    });

    const onCollapse = (collapsed: any) => {
        console.log(collapsed);
        setState({
            collapsed: !state.collapsed,
        });
    };

    return (
        <Sider collapsible={true} collapsed={state.collapsed} onCollapse={onCollapse}>
            <div className='log-container'>
                <Link to='/accueil'><img src='../../../img/uoi.png' alt='Logo_UOI' className={'logo'} /></Link>
            </div>
            <Menu
                theme='dark'
                mode='inline'
                defaultSelectedKeys={['ajouterbase']}
                defaultOpenKeys={['nav']}
            >
                <Menu.Item key='nav' title={t('NavigationMenuGauche')} className={'menuitemgroup'}>
                    {t('NavigationMenuGauche')}
                </Menu.Item>
                <SubMenu key='base' title={t('BaseMenuGauche')} className={'menuitemgroup'}>
                    <Menu.Item key='ajouterbase' className={'menuitem'} onClick={props.onclick}>{t('AddbaseMenuGauche')}</Menu.Item>
                    <Menu.Item key='listebase' className={'menuitem'} onClick={props.onclick}>{t('ListBaseMenuGauche')}</Menu.Item>
                </SubMenu>
                <SubMenu key='document' title={t('DocumentMenuGauche')} className={'menuitemgroup'}>
                    <Menu.Item key='ajouterdocument' className={'menuitem'} onClick={props.onclick}>{t('AddDocumentMenuGauche')}</Menu.Item>
                    <Menu.Item key='listedocument' className={'menuitem'} onClick={props.onclick}>{t('ListDocumentMenuGauche')}</Menu.Item>
                </SubMenu>
                <SubMenu key='sectiondevisgroupe' title={t('QuotesectionGroupeMenuGauche')} className={'menuitemgroup'}>
                    <Menu.Item key='ajoutersectiondevisgroupe' className={'menuitem'} onClick={props.onclick}>{t('AddQuoteSectionGroupeMenuGauche')}</Menu.Item>
                    <Menu.Item key='listesectiondevisgroupe' className={'menuitem'} onClick={props.onclick}>{t('ListQuoteSectionGroupeMenuGauche')}</Menu.Item>
                </SubMenu>
                <SubMenu key='sectiondevis' title={t('QuotesectionMenuGauche')} className={'menuitemgroup'}>
                    <Menu.Item key='ajoutersectiondevis' className={'menuitem'} onClick={props.onclick}>{t('AddQuoteSectionMenuGauche')}</Menu.Item>
                    <Menu.Item key='listesectiondevis' className={'menuitem'} onClick={props.onclick}>{t('ListQuoteSectionMenuGauche')}</Menu.Item>
                </SubMenu>
                <SubMenu key='formulaire' title={t('FormMenuGauche')} className={'menuitemgroup'}>
                    <Menu.Item key='ajouterhalocarbures' className={'menuitem'} onClick={props.onclick}>{t('AddFormMenuGauche')}</Menu.Item>
                    <Menu.Item key='listehalocarbures' className={'menuitem'} onClick={props.onclick}>{t('ListFormMenuGauche')}</Menu.Item>
                </SubMenu>
                <SubMenu key='salletechnique' title={t('TechnicalRoomMenuGauche')} className={'menuitemgroup'}>
                    <Menu.Item key='ajoutersalletechnique' className={'menuitem'} onClick={props.onclick}>{t('AddTechnicalRoomMenuGauche')}</Menu.Item>
                    <Menu.Item key='listesalletechnique' className={'menuitem'} onClick={props.onclick}>{t('ListTechnicalRoomMenuGauche')}</Menu.Item>
                </SubMenu>
                <SubMenu key='contact' title={t('ContactMenuGauche')} className={'menuitemgroup'}>
                    <Menu.Item key='ajoutercontact' className={'menuitem'} onClick={props.onclick}>{t('AddContactMenuGauche')}</Menu.Item>
                    <Menu.Item key='listecontact' className={'menuitem'} onClick={props.onclick}>{t('ListContactMenuGauche')}</Menu.Item>
                </SubMenu>
                <SubMenu key='utilisateur' title={t('UserMenuGauche')} className={'menuitemgroup'}>
                    <Menu.Item key='ajouterutilisateur' className={'menuitem'} onClick={props.onclick}>{t('AddUserMenuGauche')}</Menu.Item>
                    <Menu.Item key='listeutilisateur' className={'menuitem'} onClick={props.onclick}>{t('ListUserMenuGauche')}</Menu.Item>
                </SubMenu>
                <SubMenu key='creationroles' title={t('CreationRolesMenuGauche')} className={'menuitemgroup'}>
                    <Menu.Item key='ajouterrole' className={'menuitem'} onClick={props.onclick}>{t('AddCreationRolesMenuGauche')}</Menu.Item>
                    <Menu.Item key='listerole' className={'menuitem'} onClick={props.onclick}>{t('ListBaseMenuGauche')}</Menu.Item>
                </SubMenu>
            </Menu>
        </Sider>
    );
}
