import { Breadcrumb, Layout } from 'antd';
import 'antd/dist/antd.css';
import { GlobalReducer } from 'common/src/redux/type';
import { APP_URLS } from 'constants/urls';
import { MenuInfo } from 'rc-menu/lib/interface';
import { default as React, useState } from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Footerpage from '../footer/footer';
import Formulairebase from '../formulaires/tousformulaires/base/formulairebase';
import Formulairecontact from '../formulaires/tousformulaires/contact/formulairecontact';
import Formulairedocument from '../formulaires/tousformulaires/document/formulairedocument';
import Formulairehalocarbures from '../formulaires/tousformulaires/halocarbures/formulairehalocarbures';
import Formulairerole from '../formulaires/tousformulaires/role/formulairerole';
import Formulairesalletechnique from '../formulaires/tousformulaires/salletechnique/formulairesalletechnique';
import Formulairesectiondevis from '../formulaires/tousformulaires/sectiondevis/formulairesectiondevis';
import Formulairesectiondevisgroupe from '../formulaires/tousformulaires/sectiondevisgroupe/formulairesectiondevisgroupe';
import Formulaireutilisateur from '../formulaires/tousformulaires/utilisateur/formulaireutilisateur';
import Listebases from '../formulaires/toutelistes/base/listebases';
import Listecontact from '../formulaires/toutelistes/contact/listecontact';
import Listedocument from '../formulaires/toutelistes/document/listedocument';
import Listehalocarbures from '../formulaires/toutelistes/halocarbures/listehalocarbures';
import Profile from '../formulaires/toutelistes/profile/profile';
import Listerole from '../formulaires/toutelistes/role/listerole';
import Listesalletechnique from '../formulaires/toutelistes/salletechnique/listesalletechnique';
import Listesectiondevis from '../formulaires/toutelistes/sectiondevis/listesectiondevis';
import Listesectiondevisgroupe from '../formulaires/toutelistes/sectiondevisgroupe/listesectiondevisgroupe';
import Listeutilisateur from '../formulaires/toutelistes/utilisateur/listeutilisateur';
import Etape1 from '../halocarbures/etape1';
import Headerpage from '../header/header';
import Menugauche from '../menugauche/menugauche';


const { Content } = Layout;
// const { SubMenu } = Menu;

function Admin() {

    //const dispatch = useDispatch();
    const isLoggedIn = useSelector((state: GlobalReducer) => state.stateReducer.isLoggedIn);
    //const userContext = useSelector((state: GlobalReducer) => state.stateReducer.userContext);
    const [domContent, setdomContent] = useState('ajouterbase');


    function changeContent(value: MenuInfo) {
        const text = value.key;

        setdomContent(text.toString());
    }

    function renderSwitch(param: string) {
        switch (param) {
            case 'ajouterbase': return <Formulairebase />;
            case 'listebase': return <Listebases />;
            case 'ajouterdocument': return <Formulairedocument document={undefined} />;
            case 'listedocument': return <Listedocument />;
            case 'ajoutersectiondevisgroupe': return <Formulairesectiondevisgroupe />;
            case 'listesectiondevisgroupe': return <Listesectiondevisgroupe />;
            case 'ajoutersectiondevis': return <Formulairesectiondevis />;
            case 'listesectiondevis': return <Listesectiondevis />;
            case 'ajouterhalocarbures': return <Formulairehalocarbures />;
            case 'listehalocarbures': return <Listehalocarbures />;
            case 'etape': return <Etape1 />;
            case 'ajoutersalletechnique': return <Formulairesalletechnique />;
            case 'listesalletechnique': return <Listesalletechnique />;
            case 'ajoutercontact': return <Formulairecontact />;
            case 'listecontact': return <Listecontact />;
            case 'ajouterutilisateur': return <Formulaireutilisateur />;
            case 'listeutilisateur': return <Listeutilisateur />;
            case 'ajouterrole': return <Formulairerole />;
            case 'listerole': return <Listerole />;
            case 'profile': return <Profile />;
            default: return <h1>No Component match</h1>;
        }
    }

    function loggInPage() {
        return (
            <Redirect to={{ pathname: APP_URLS.CONNEXION, state: { from: APP_URLS.ADMIN } }} />
        );
    }

    function adminPage() {
        return (
            <div id={'layout'}>
                <Layout style={{ minHeight: '100vh' }}>
                    <Menugauche onclick={changeContent} />
                    <Layout className='site-layout'>
                        <Headerpage onclick={changeContent} />
                        <Content style={{ margin: '0 16px' }}>
                            <Breadcrumb style={{ margin: '16px 0' }}>
                                <Breadcrumb.Item>User</Breadcrumb.Item>
                                <Breadcrumb.Item>Bill</Breadcrumb.Item>
                            </Breadcrumb>
                            <div className='site-layout-background'>
                                {renderSwitch(domContent)}
                            </div>
                        </Content>
                        <Footerpage />
                    </Layout>
                </Layout>
            </div>
        );
    }

    return (
        <div> {isLoggedIn ? adminPage() : loggInPage()} </div>
    );

}
export default Admin;
