import { Button, DatePicker, Form, Input, Modal, Select, Space } from 'antd';
import { BaseModel, Formstep2Model } from 'common';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';



const { Option } = Select;

function onChange(date: any, dateString: any) {
    console.log(date, dateString);
}

export default function Etape2() {
    const { t } = useTranslation();

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [step2, setStep2] = useState(undefined as Formstep2Model | undefined);

    const [form] = Form.useForm();

    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    const onFinish = async (values: Formstep2Model) => {
        setStep2(values);
        const step1Json = JSON.stringify(values);
        const result = await (await fetch('/api/steptwo', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: step1Json,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            Modal.success({
                content: 'Confirmation entegistrement étape2',
            });
        } else {
            Modal.error({
                content: 'Échec entegistrement étape2',
            });
        }
    };
    return (
        <div id='etape-halocarbures'>
            <div className='entete-complexe-form'>
                <div className={'entete'}>
                    <h1 className={'majuscule'}>étape 2</h1>
                    <h3>Doit être remplit <span className={'majuscule'}><strong>avant l’arrivée de l’équipement</strong></span> sur les territoires du MDN</h3>
                    <div>
                        <ul>
                            <li>
                                L’inspecteur, coordonnateur de marché, chargé de projet ou parrain d’unité doivent avoir dans le dossier l’étape 1 et 2 dûment
                                remplie et approuvé par le GH (téléphone: (418) 844-5000 poste 3867) ou par son remplaçant. Sinon ils n’ont pas le droit de
                                procéder à l’exécution de l’étape 3.s
                            </li>
                            <li>
                                <strong>Sauf dans le cas d’une <span className={'minuscule'}><strong>urgence</strong></span> (ex. soir), vous pouvez soumettre l’étape 1 et 2 après la réparation.</strong>
                            </li>
                        </ul>
                    </div>
                </div>
                <div>
                    <Form form={form} name='complex-form' labelCol={{ span: 10 }} wrapperCol={{ span: 8 }} initialValues={step2} onFinish={onFinish}>
                        <Form.Item label={t('LabelBaseMilitaireDocument')}>
                            <Input.Group compact={true}>
                                <Form.Item
                                    name={['BaseId']}
                                    noStyle={true}
                                    rules={[{ required: true, message: t('ValidMilitaryBase') }]}
                                >
                                    <Select
                                        placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                        {bases?.map(base => {
                                            return (
                                                <Option key={base.BaseId} value={base.BaseId}>
                                                    {base.Name}
                                                </Option>
                                            );
                                        })}
                                    </Select>
                                </Form.Item>
                            </Input.Group>
                        </Form.Item>
                        <Form.Item label='Date actuelle'
                            name='Actual_date'
                            rules={[
                                {
                                    required: true,
                                    message: t('ActualDate')
                                }
                            ]}
                        >
                            <Space direction='vertical'>
                                <DatePicker onChange={onChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Édifice'
                            name='Edifice'
                            rules={[
                                {
                                    required: true,
                                    message: t('EdificeName')
                                }
                            ]}
                        >
                            <Input placeholder='Numéro édifice' />
                        </Form.Item>
                        <Form.Item label='Pièce'
                            name='Piece'
                            rules={[
                                {
                                    required: true,
                                    message: t('PieceName')
                                }
                            ]}
                        >
                            <Input placeholder='Numéro pièce' />
                        </Form.Item>
                        <div>
                            <h3><strong>INFORMATIONS SUR L’ENTREPRENEUR (si applicable)</strong></h3>
                        </div>
                        <Form.Item label='Nom de la compagnie :'
                            name='Company_Name'
                            rules={[
                                {
                                    required: true,
                                    message: t('CompanyName')
                                }
                            ]}>
                            <Input placeholder='Saisissez le nom de la compagnie' />
                        </Form.Item>
                        <Form.Item label='Numéro de téléphone :'
                            name='Phone_Number'
                            rules={[
                                {
                                    required: true,
                                    message: t('PhoneNumber')
                                }
                            ]}
                        >
                            <Input placeholder='Saisissez un numéro' />
                        </Form.Item>
                        <Form.Item label='Adresse complète :'
                            name='Adress'
                            rules={[
                                {
                                    required: true,
                                    message: t('Adress')
                                }
                            ]}>
                            <Input placeholder='Saisissez une adresse' />
                        </Form.Item>
                        <div>
                            <h3><strong>INFORMATIONS OBLIGATOIRES DE L’ÉQUIPEMENT</strong></h3>
                        </div>
                        <Form.Item label='Manufacturier :'
                            name='Manufacturier'
                            rules={[
                                {
                                    required: true,
                                    message: t('Manufacturier')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de manufacturier' />
                        </Form.Item>
                        <Form.Item label='Type de système :'
                            name='System_Type'
                            rules={[
                                {
                                    required: true,
                                    message: t('SystemType')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de système' />
                        </Form.Item>
                        <Form.Item label='Modèle :'
                            name='Model'
                            rules={[
                                {
                                    required: true,
                                    message: t('ModelName')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de modèle' />
                        </Form.Item>
                        <Form.Item label='Quantité de Réfirgérant :'
                            name='Quantity_refrigerant'
                            rules={[
                                {
                                    required: true,
                                    message: t('QuantityRefrigerant')
                                }
                            ]}>
                            <Input placeholder='Saisissez la quantité' />
                        </Form.Item>
                        <Form.Item label='Numéro de série :'
                            name='Nunber_serie'
                            rules={[
                                {
                                    required: true,
                                    message: t('SeriealNumber')
                                }
                            ]}>
                            <Input placeholder='Saisissez le numéro de série' />
                        </Form.Item>
                        <Form.Item label='Capacité (HP/BTU/KW/Tonne) :'
                            name='Capacity'
                            rules={[
                                {
                                    required: true,
                                    message: t('Capacity')
                                }
                            ]}>
                            <Input placeholder='Saissez la capcité' />
                        </Form.Item>
                        <Form.Item label='Unité de mesure :'>
                            <Input.Group compact={true}>
                                <Form.Item
                                    name={['unitedemesure']}
                                    noStyle={true}
                                    rules={[{ required: true, message: 'Veuillez selectionner une unité de mesure' }]}
                                >
                                    <Select placeholder='Selectionnez une unité de mesure'>
                                        <Option value='BTU'>BTU</Option>
                                        <Option value='HP'>HP</Option>
                                        <Option value='KW'>KW</Option>
                                        <Option value='Tonne'>Tonne</Option>
                                    </Select>
                                </Form.Item>
                            </Input.Group>
                        </Form.Item>
                        <Form.Item label='Remarques et autorisation par GH ou son remplaçant :'
                            name='Infos_sup'
                            rules={[
                                {
                                    required: true,
                                    message: t('AdditionalInformation')
                                }
                            ]}>
                            <Input.TextArea />
                        </Form.Item>
                        <Form.Item label='Nom du GH ou du remplaçant :'
                            name='GH_Name'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidSubstituteName')
                                }
                            ]}>
                            <Input placeholder='Saisissez le nom du GH ou du remplaçant' />
                        </Form.Item>
                        <Form.Item label='Signature du GH (alphanumérique) :'
                            name='GH_Signature'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidSignature')
                                }
                            ]}>
                            <Input placeholder='Saisissez votre signature' />
                        </Form.Item>
                        <div>
                            <h3>N.B : Doit être approuvé par le GH, sinon vous n'avez pas le droit de procéder à l'étape 3.</h3>
                        </div>
                        <Form.Item label=' ' colon={false}>
                            <Button type='primary' htmlType='submit'>
                                Enregistrer
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
};
