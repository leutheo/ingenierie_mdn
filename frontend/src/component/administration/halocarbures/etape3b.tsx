import { Button, DatePicker, Form, Input, Modal, Space } from 'antd';
import { Formstep3BModel } from 'common/src';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';


function onChange(date: any, dateString: any) {
    console.log(date, dateString);
}

export default function Etape3B() {
    const { t } = useTranslation();

    const [step3b, setStep3b] = useState(undefined as Formstep3BModel | undefined);

    const [form] = Form.useForm();

    const onFinish = async (values: Formstep3BModel) => {
        setStep3b(values);
        const step1Json = JSON.stringify(values);
        const result = await (await fetch('/api/stepthreeb', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: step1Json,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            Modal.success({
                content: 'Confirmation entegistrement étape3b',
            });
        } else {
            Modal.error({
                content: 'Échec entegistrement étape3b',
            });
        }
    };
    return (
        <div id='etape-halocarbures'>
            <div className='entete-complexe-form'>
                <div className={'entete'}>
                    <h1 className={'majuscule'}>étape 3B</h1>
                    <h3><strong>Retour au MDN d’un système ayant contenu ou contenant des halocarbures</strong></h3>
                    <div>
                        <p>Lors du retrait de l’unité, l’huile et le réfrigérant doivent être enlevés <strong><span className={'majuscule'}>par un technicien accrédité frigoriste et HRAI/MOPI</span></strong>.
                            Le lieu de disposition doit être indiqué en tout temps.</p>
                    </div>
                </div>
                <div>
                    <Form form={form} name='complex-form' labelCol={{ span: 12 }} wrapperCol={{ span: 8 }} initialValues={step3b} onFinish={onFinish}>
                        <Form.Item label='Date actuelle'
                            name='Actual_date'
                            rules={[
                                {
                                    required: true,
                                    message: t('ActualDate')
                                }
                            ]}>
                            <Space direction='vertical'>
                                <DatePicker onChange={onChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Type de système :'
                            name='System_type'
                            rules={[
                                {
                                    required: true,
                                    message: t('SystemType')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de système' />
                        </Form.Item>
                        <Form.Item label='I/D de l’unité actuelle (plaquette bleue) ou numéro de modèle et  de série :'
                            name='Actual_UnityId'
                            rules={[
                                {
                                    required: true,
                                    message: t('ActualUnityId')
                                }
                            ]}>
                            <Input placeholder='Saisissez unité actuelle' />
                        </Form.Item>
                        <Form.Item label='Lieu et adresse où l’unité a été retournée ou entreposée'
                            name='Unity_adress'
                            rules={[
                                {
                                    required: true,
                                    message: t('UnityAdress')
                                }
                            ]}>
                            <Input placeholder='Saisissez un lieu ou une adresse' />
                        </Form.Item>
                        <Form.Item label='Pourquoi l’unité a-t-elle été enlevée'
                            name='Retrait_reason'
                            rules={[
                                {
                                    required: true,
                                    message: t('RetraitReason')
                                }
                            ]}>
                            <Input placeholder='Saissez la raison' />
                        </Form.Item>
                        <Form.Item label='Quelle est sa condition :'
                            name='Condition'
                            rules={[
                                {
                                    required: true,
                                    message: t('Condition')
                                }
                            ]}>
                            <Input placeholder='Saisissez une indication' />
                        </Form.Item>
                        <div>
                            <h3><strong>N.B</strong> Si l’unité est retournée au MDN. L’unité doit être vide de réfrigérant et <strong>avoir une pression positive de nitrogène</strong> .
                                Une soudure sur les tuyaux de surchauffe et de refroidissement si applicable doit être exécutée selon le code pratique de
                                réfrigération <strong>par un technicien accrédité frigoriste et HRAI / MOPI</strong>.</h3>
                        </div>
                        <div>
                            <h3>Lorsque l’étape <strong>3B</strong>  est complétée, remettre le formulaire ainsi que la <strong>plaquette bleue</strong>  d’identification du GMAO au <strong>GH</strong>  ou son remplaçant.
                                Attendre la confirmation du lieu d’entreposage ou autres directives qui vous seront émises par le GH ou son remplaçant.</h3>
                        </div>
                        <Form.Item label='Nom du technicien :'
                            name='Technicien_Lastname'
                            rules={[
                                {
                                    required: true,
                                    message: t('TechnicienLastname')
                                }
                            ]}>
                            <Input placeholder='Saisissez le nom du technicien' />
                        </Form.Item>
                        <Form.Item label='Signature du technicien (alphanumérique) :'
                            name='Signature'
                            rules={[
                                {
                                    required: true,
                                    message: t('Signature')
                                }
                            ]}>
                            <Input placeholder='Saisissez votre signature' />
                        </Form.Item>
                        <Form.Item label=' ' colon={false}>
                            <Button type='primary' htmlType='submit'>
                                Enregistrer
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
}
