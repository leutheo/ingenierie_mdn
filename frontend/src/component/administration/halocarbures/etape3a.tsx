import { Button, DatePicker, Form, Input, Modal, Space } from 'antd';
import { Formstep3AModel } from 'common/src';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';


function onChange(date: any, dateString: any) {
    console.log(date, dateString);
}

export default function Etape3A() {
    const { t } = useTranslation();

    const [step3a, setStep3a] = useState(undefined as Formstep3AModel | undefined);

    const [form] = Form.useForm();

    const onFinish = async (values: Formstep3AModel) => {
        setStep3a(values);
        const step1Json = JSON.stringify(values);
        const result = await (await fetch('/api/stepthreea', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: step1Json,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            Modal.success({
                content: 'Confirmation entegistrement étape3a',
            });
        } else {
            Modal.error({
                content: 'Échec entegistrement étape3a',
            });
        }
    };
    return (
        <div id='etape-halocarbures'>
            <div className='entete-complexe-form'>
                <div className={'entete'}>
                    <h1 className={'majuscule'}>étape 3A</h1>
                    <h3><span className={'majuscule'}><strong>Élimination d’un système ayant ou contenant des halocarbures</strong></span></h3>
                    <div>
                        <p>Lors du retrait de l’unité, l’huile et le réfrigérant doivent être enlevés <strong><span className={'majuscule'}>par un technicien accrédité frigoriste et HRAI/MOPI</span></strong>.
                            Le lieu de disposition doit être indiqué en tout temps.</p>
                    </div>
                </div>
                <div>
                    <Form form={form} name='complex-form' labelCol={{ span: 10 }} wrapperCol={{ span: 8 }} initialValues={step3a} onFinish={onFinish}>
                        <Form.Item label='Nom de l’unité qui en dispose :'
                            name='Unity_Name'
                            rules={[
                                {
                                    required: true,
                                    message: t('UnityName')
                                }
                            ]}>
                            <Input placeholder='Saisissez le nom de unité' />
                        </Form.Item>
                        <Form.Item label='Nom de la personnne qui en dispose :'
                            name='Name'
                            rules={[
                                {
                                    required: true,
                                    message: t('Name')
                                }
                            ]}>
                            <Input placeholder='Saisissez le nom de la personnne qui en dispose' />
                        </Form.Item>
                        <div>
                            <p>Attendre la confirmation du lieu d’entreposage ou autres directives qui vous seront émises par le <strong>GH</strong> ((418) 844-5000 poste 3867) ou son remplaçant.</p>
                        </div>
                        <Form.Item label='Date actuelle'
                            name='Actual_date'
                            rules={[
                                {
                                    required: true,
                                    message: t('ActualDate')
                                }
                            ]}>
                            <Space direction='vertical'>
                                <DatePicker onChange={onChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Édifice'
                            name='Edifice'
                            rules={[
                                {
                                    required: true,
                                    message: t('EdificeName')
                                }
                            ]}>
                            <Input placeholder='Numéro édifice' />
                        </Form.Item>
                        <Form.Item label='Pièce'
                            name='Piece'
                            rules={[
                                {
                                    required: true,
                                    message: t('PieceName')
                                }
                            ]}>
                            <Input placeholder='Numéro pièce' />
                        </Form.Item>
                        <Form.Item label='Lieu / adresse de la disposition de l’unité :'
                            name='Unity_adress'
                            rules={[
                                {
                                    required: true,
                                    message: t('UnityAdress')
                                }
                            ]}>
                            <Input placeholder='Saisissez adresse' />
                        </Form.Item>
                        <Form.Item label='Raison du Retrait :'
                            name='Retrait_reason'
                            rules={[
                                {
                                    required: true,
                                    message: t('RetraitReason')
                                }
                            ]}>
                            <Input placeholder='Saisissez la raison du retrait' />
                        </Form.Item>
                        <Form.Item label='Manufacturier :'
                            name='Manufacturier'
                            rules={[
                                {
                                    required: true,
                                    message: t('Manufacturier')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de manufacturier' />
                        </Form.Item>
                        <Form.Item label='Modèle :'
                            name='Model'
                            rules={[
                                {
                                    required: true,
                                    message: t('ModelName')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de modèle' />
                        </Form.Item>
                        <Form.Item label='Type de Réfrigérant :'
                            name='Type_refrigerant'
                            rules={[
                                {
                                    required: true,
                                    message: t('TypeRefrigerant')
                                }
                            ]}>
                            <Input placeholder='Saisissez la Type' />
                        </Form.Item>
                        <Form.Item label='Numéro de série :'
                            name='Nunber_serie'
                            rules={[
                                {
                                    required: true,
                                    message: t('SerialNumber')
                                }
                            ]}>
                            <Input placeholder='Saisissez le numéro de série' />
                        </Form.Item>
                        <Form.Item label='Type de système :'
                            name='System_type'
                            rules={[
                                {
                                    required: true,
                                    message: t('SystemType')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de système' />
                        </Form.Item>
                        <Form.Item label='Commentaire du GH :'
                            name='GH_coments'
                            rules={[
                                {
                                    required: true,
                                    message: t('GHComents')
                                }
                            ]}>
                            <Input.TextArea />
                        </Form.Item>
                        <Form.Item label='Nom du technicien :'
                            name='Technicien_Lastname'
                            rules={[
                                {
                                    required: true,
                                    message: t('TechnicienLastname')
                                }
                            ]}>
                            <Input placeholder='Saisissez le nom du technicien' />
                        </Form.Item>
                        <Form.Item label='Signature du technicien (alphanumérique) :'
                            name='Signature'
                            rules={[
                                {
                                    required: true,
                                    message: t('Signature')
                                }
                            ]}>
                            <Input placeholder='Saisissez votre signature' />
                        </Form.Item>
                        <div>
                            <h3>Lorsque l’étape <strong>3A</strong> est complétée, remettre le <strong>formulaire</strong> ainsi que la <strong>plaquette bleue d’identification du GMAO</strong> au <strong>GH</strong> ou à son remplaçant.</h3>
                        </div>
                        <Form.Item label=' ' colon={false}>
                            <Button type='primary' htmlType='submit'>
                                Enregistrer
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
}
