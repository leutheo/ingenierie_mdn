import { Button, DatePicker, Form, Input, Modal, Select, Space } from 'antd';
import { BaseModel, Formstep1Model } from 'common';
import { GlobalReducer } from 'common/src/redux/type';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';


const { Option } = Select;

function onChange(date: any, dateString: any) {
    debugger;
    console.log(date, dateString);
}


export default function Etape1() {
    const { t } = useTranslation();

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [step1, setStep1] = useState(undefined as Formstep1Model | undefined);

    const [form] = Form.useForm();

    const userContext = useSelector((state: GlobalReducer) => state.stateReducer.userContext);

    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    const onFinish = async (values: Formstep1Model) => {
        setStep1(values);
        console.log(values);
        debugger;
        console.log(userContext);
        values.UserId = userContext.user != undefined ? userContext.user.UserId : 0;
        //TODO: comprendre pourquoi cette date n,es pas assigne automatiquement par le form comme les inputs
        //values.Actual_date = "27/07/2021";
        const step1Json = JSON.stringify(values);
        const result = await (await fetch('/api/stepone', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: step1Json,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            Modal.success({
                content: 'Confirmation entegistrement étape1',
            });
        } else {
            Modal.error({
                content: 'Échec entegistrement étape1',
            });
        }
    };

    return (
        <div id='etape-halocarbures'>
            <div className='entete-complexe-form'>
                <div className={'entete'}>
                    <h1 className={'majuscule'}>étape 1</h1>
                    <h2>{t('InfoTechDocumentTitle')}</h2>
                    <h3><strong>À rempir avant tout <span className={'majuscule'}>achat</span> ou <span className={'majuscule'}>remplacement</span> d'un système contenant des halocarbures.</strong></h3>
                </div>
                <div>
                    <Form form={form} name='complex-form' labelCol={{ span: 16 }} wrapperCol={{ span: 8 }} initialValues={step1} onFinish={onFinish}>
                        <Form.Item label={t('LabelBaseMilitaireDocument')}>
                            <Input.Group compact={true}>
                                <Form.Item
                                    name={['BaseId']}
                                    noStyle={true}
                                    rules={[{ required: true, message: t('ValidMilitaryBase') }]}
                                >
                                    <Select
                                        placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                        {bases?.map(base => {
                                            return (
                                                <Option key={base.BaseId} value={base.BaseId}>
                                                    {base.Name}
                                                </Option>
                                            );
                                        })}
                                    </Select>
                                </Form.Item>
                            </Input.Group>
                        </Form.Item>
                        <Form.Item label='Nom de l’inspecteur / Coordonnateur de marché/ Chargé de projet / Parrain d’unité pour ce travail :'
                            name='Inspector_Name'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidInspectorName')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom' />
                        </Form.Item>
                        <Form.Item label='Édifice'
                            name='Edifice'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidEdifceName')
                                }
                            ]}>
                            <Input placeholder='Numéro édifice' />
                        </Form.Item>
                        <Form.Item label='Numéro de projet / Bon de travail / CF141 ou Numéro de projet CDC / ou téléphone :'
                            name='Project_Number'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidProjectNumber')
                                }
                            ]}>
                            <Input placeholder='Saisissez un numéro' />
                        </Form.Item>
                        <Form.Item label='Pièce'
                            name='Piece'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidPieceName')
                                }
                            ]}>
                            <Input placeholder='Numéro pièce' />
                        </Form.Item>
                        <Form.Item label='Date actuelle'
                            name='Actual_date'>
                            <Space direction='vertical'>
                                <DatePicker onChange={onChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Type de système projeté :'
                            name='System_Type'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidSystemType')
                                }
                            ]}
                        >
                            <Input placeholder='Saisissez un nom de système' />
                        </Form.Item>
                        <Form.Item label='Manufacturier :'
                            name='Manufacturier'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidManufacturierName')
                                }
                            ]}
                        >
                            <Input placeholder='Saisissez un nom de manufacturier' />
                        </Form.Item>
                        <Form.Item label='Type d’halocarbure (réfrigérant) :'>
                            <Input.Group compact={true}>
                                <Form.Item
                                    name={['typehalocarbure']}
                                    noStyle={true}
                                    rules={[{ required: true, message: 'Veuillez selectionner un type d’halocarbure' }]}
                                >
                                    <Select placeholder='Selectionnez un type d’halocarbure'>
                                        <Option value='R-134A'>R-134A</Option>
                                        <Option value='R-22'>R-22</Option>
                                        <Option value='R-407C'>R-407C</Option>
                                        <Option value='R-404A'>R-404A</Option>
                                        <Option value='R-408A'>R-408A</Option>
                                        <Option value='R-409A'>R-409A</Option>
                                        <Option value='R-410A'>R-410A</Option>
                                        <Option value='Inconnu'>Inconnu</Option>
                                    </Select>
                                </Form.Item>
                            </Input.Group>
                        </Form.Item>
                        <Form.Item label='Modèle :'
                            name='Model'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidModelName')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de modèle' />
                        </Form.Item>
                        <Form.Item label='Halocarbon type :'
                            name='Halocarbon_type'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidModelName')
                                }
                            ]}>
                            <Input placeholder='Saisissez le type halocarbure' />
                        </Form.Item>
                        <div>
                            <h3 className={'majuscule'}>aucun document en pdf ne sera accepté aux étapes 1 et 2</h3>
                        </div>
                        <Form.Item label='Information supplémentaire :'
                            name='Infos_sup'
                            rules={[
                                {
                                    required: true,
                                    message: t('AdditionalInformation')
                                }
                            ]}>
                            <Input.TextArea />
                        </Form.Item>
                        <div>
                            <h3>Remarques et autorisation par le GH ((418) 844-5000 poste 3867 </h3>
                        </div>
                        <Form.Item label='Nom du GH ou du remplaçant :'
                            name='GH_Name'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidSubstituteName')
                                }
                            ]}
                        >
                            <Input placeholder='Saisissez le nom du GH ou du remplaçant' />
                        </Form.Item>
                        <Form.Item label='Signature du GH (alphanumérique) :'
                            name='GH_Signature'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidSignature')
                                }
                            ]}>
                            <Input placeholder='Saisissez votre signature' />
                        </Form.Item>
                        <div>
                            <h3>N.B : Doit être approuvé par le GH, sinon vous n'avez pas le droit de procéder à l'étape 2.</h3>
                        </div>
                        <Form.Item label=' ' colon={false}>
                            <Button type='primary' htmlType='submit'>
                                Enregistrer
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
}
