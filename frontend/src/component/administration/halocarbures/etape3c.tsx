import { Button, Checkbox, DatePicker, Form, Input, Modal, Space } from 'antd';
import { Formstep3CModel } from 'common/src';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';



function oneChange(date: any, dateString: any) {
    console.log(date, dateString);
}

function onChange(checkedValues: any) {
    console.log('checked = ', checkedValues);
}
const plaineOptions = ['OUI', 'NON'];


function checkedChange(checkedValues: any) {
    console.log('checked = ', checkedValues);
}
const plainOptions = ['Détection électronique ', 'Eau savonneuse', 'Détection Électronique /eau savonneuse', 'Nitrogène', 'Sous vide', 'Méthode alternative (expliquer à droite)', 'Mèche pour l’ammoniac seulement'];


export default function Etape3C() {

    const { t } = useTranslation();

    const [step3c, setStep3c] = useState(undefined as Formstep3CModel | undefined);

    const [form] = Form.useForm();

    const onFinish = async (values: Formstep3CModel) => {
        setStep3c(values);
        const step1Json = JSON.stringify(values);
        const result = await (await fetch('/api/stepthreec', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: step1Json,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            Modal.success({
                content: 'Confirmation entegistrement étape3c',
            });
        } else {
            Modal.error({
                content: 'Échec entegistrement étape3c',
            });
        }
    };

    return (
        <div id='etape-halocarbures'>
            <div className='entete-complexe-form'>
                <div className={'entete'}>
                    <h1 className={'majuscule'}>étape 3C</h1>
                    <h3><strong>Détection et essai d’étanchéité USS Valcartier</strong></h3>
                    <h3><strong>Avertissement d’essais d’étanchéité</strong></h3>
                </div>
                <div>
                    <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={step3c} onFinish={onFinish}>
                        <Form.Item label='Cocher la et/ou les méthodes utilisées'
                            name='Electronique_detection'
                            rules={[
                                {
                                    required: true,
                                    message: t('ValidPieceName')
                                }
                            ]}>
                            <Checkbox.Group options={plainOptions} defaultValue={['Détection électronique']} onChange={checkedChange} />
                        </Form.Item>
                        <Form.Item label='Fuite(s) trouvée(s)'>
                            <Checkbox.Group options={plaineOptions} defaultValue={['OUI']} onChange={onChange} />
                        </Form.Item>
                        <Form.Item label='Date'>
                            <Space direction='vertical'>
                                <DatePicker onChange={oneChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Fuite(s) réparée(s)'>
                            <Checkbox.Group options={plaineOptions} defaultValue={['OUI']} onChange={onChange} />
                        </Form.Item>
                        <Form.Item label='Date'>
                            <Space direction='vertical'>
                                <DatePicker onChange={oneChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Essai étanchéité final'>
                            <Checkbox.Group options={plaineOptions} defaultValue={['OUI']} onChange={onChange} />
                        </Form.Item>
                        <Form.Item label='Date'>
                            <Space direction='vertical'>
                                <DatePicker onChange={oneChange} />
                            </Space>
                        </Form.Item>
                        <div>
                            <h3>
                                <strong>N.B Si, après avoir effectué une détection, vous découvrez une nouvelle fuite sur le système,
                                    vous devez refaire l’étape 3 au complet.
                                    Cette étape doit être complétée par le technicien dans les 48 heures suivant la fin des travaux. Voir Note 1
                                </strong>
                            </h3>
                        </div>
                        <div>
                            <h3>
                                <strong>
                                    L’inspecteur / coordonnateur de marché/ chargé de projet ou parrain doit remettre l’étape 3, 3A, 3B et 3C
                                    si applicable dûment remplie au GH ((418) 844-5000 poste 3867 ou son remplaçant.
                                </strong>
                            </h3>
                        </div>
                        <Form.Item label='Nom de la Compagnie  :'>
                            <Input placeholder='Saisissez le nom de la compagnie' />
                        </Form.Item>
                        <Form.Item label='Numéro de téléphone de la Compagnie  :'>
                            <Input placeholder='Saisissez le numéro de téléphone de la compagnie' />
                        </Form.Item>
                        <Form.Item label='Nom du technicien 1 :'>
                            <Input placeholder='Saisissez le nom du technicien 1' />
                        </Form.Item>
                        <Form.Item label='Signature du technicien 1 :'>
                            <Input placeholder='Saisissez la signature du technicien 1' />
                        </Form.Item>
                        <Form.Item label='Nom du technicien 2 :'>
                            <Input placeholder='Saisissez le nom du technicien 2' />
                        </Form.Item>
                        <Form.Item label='Signature du technicien 2 :'>
                            <Input placeholder='Saisissez la signature du technicien 2' />
                        </Form.Item>
                        <div>
                            <h3>
                                <strong>
                                    CHAMP OBLIGATOIRE : Avez-vous laissé une copie de L’Étape 3C - Détection et essai d’étanchéité sur l’unité comme mentionné dans la Note 1.
                                </strong>
                            </h3>
                        </div>
                        <Form.Item label='Copie détection et essai d’étanchéité ?'>
                            <Checkbox.Group options={plaineOptions} defaultValue={['OUI']} onChange={onChange} />
                        </Form.Item>
                        <Form.Item label=' ' colon={false}>
                            <Button type='primary' htmlType='submit'>
                                Enregistrer
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
};
