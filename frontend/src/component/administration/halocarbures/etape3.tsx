import { Button, DatePicker, Form, Input, Modal, Space } from 'antd';
import { Formstep3Model } from 'common/src';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';


function onChange(date: any, dateString: any) {
    console.log(date, dateString);
}

export default function Etape3() {
    const { t } = useTranslation();

    const [step3, setStep3] = useState(undefined as Formstep3Model | undefined);

    const [form] = Form.useForm();

    const onFinish = async (values: Formstep3Model) => {
        setStep3(values);
        const step1Json = JSON.stringify(values);
        const result = await (await fetch('/api/stepthree', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: step1Json,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            Modal.success({
                content: 'Confirmation entegistrement étape3',
            });
        } else {
            Modal.error({
                content: 'Échec entegistrement étape3',
            });
        }
    };

    return (
        <div id='etape-halocarbures'>
            <div className='entete-complexe-form'>
                <div className={'entete'}>
                    <h1 className={'majuscule'}>étape 3</h1>
                    <h3> <span className={'majuscule'}><strong>Exécution des travaux</strong></span></h3>
                </div>
                <div>
                    <Form form={form} name='complex-form' labelCol={{ span: 16 }} wrapperCol={{ span: 8 }} initialValues={step3} onFinish={onFinish}>
                        <Form.Item label='Date actuelle :'
                            name='Actual_date'
                            rules={[
                                {
                                    required: true,
                                    message: t('ActualDate')
                                }
                            ]}>
                            <Space direction='vertical'>
                                <DatePicker onChange={onChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Édifice'
                            name='Edifice'
                            rules={[
                                {
                                    required: true,
                                    message: t('EdificeName')
                                }
                            ]}>
                            <Input placeholder='Saissez le numéro édifice' />
                        </Form.Item>
                        <Form.Item label='Pièce'
                            name='Piece'
                            rules={[
                                {
                                    required: true,
                                    message: t('PieceName')
                                }
                            ]}>
                            <Input placeholder='Saissez le numéro de la pièce' />
                        </Form.Item>
                        <Form.Item label='Nom inspecteur :'
                            name='Inspector_Name'
                            rules={[
                                {
                                    required: true,
                                    message: t('InspectorName')
                                }
                            ]}>
                            <Input placeholder='Saisissez le nom inspecteur' />
                        </Form.Item>
                        <Form.Item label='Numéro de téléphone inspecteur:'
                            name='Inspector_Phone_Number'
                            rules={[
                                {
                                    required: true,
                                    message: t('InspectorPhoneNumber')
                                }
                            ]}>
                            <Input placeholder='Saisissez un numéro inspecteur' />
                        </Form.Item>
                        <Form.Item label='Type de système :'
                            name='System_Type'
                            rules={[
                                {
                                    required: true,
                                    message: t('SystemType')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de système' />
                        </Form.Item>
                        <Form.Item label='Numéro de projet / Bon de travail / CF141 :'
                            name='Project_Number'
                            rules={[
                                {
                                    required: true,
                                    message: t('ProjectNumber')
                                }
                            ]}>
                            <Input placeholder='Saisissez un numéro' />
                        </Form.Item>
                        <Form.Item label='Information obligatoire : Numéro de la plaquette bleue (GMAO) :'
                            name='Blue_Plakette'
                            rules={[
                                {
                                    required: true,
                                    message: t('BluePlakette')
                                }
                            ]}>
                            <Input placeholder='Saisissez le numéro de la plaquette bleue' />
                        </Form.Item>
                        <Form.Item label='sinon vous devez fournir les informations suivantes : Manufacturier :'
                            name='Manufacturier'
                            rules={[
                                {
                                    required: true,
                                    message: t('Manufacturier')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de manufacturier' />
                        </Form.Item>
                        <Form.Item label='Modèle :'
                            name='Model'
                            rules={[
                                {
                                    required: true,
                                    message: t('ModelName')
                                }
                            ]}>
                            <Input placeholder='Saisissez un nom de modèle' />
                        </Form.Item>
                        <Form.Item label='Numéro de série :'
                            name='Nunber_serie'
                            rules={[
                                {
                                    required: true,
                                    message: t('SeriealNumber')
                                }
                            ]}>
                            <Input placeholder='Saisissez le numéro de série' />
                        </Form.Item>

                        <Form.Item label='Description :'
                            name='Description'
                            rules={[
                                {
                                    required: true,
                                    message: t('Description')
                                }
                            ]}>
                            <Input placeholder='Saisissez une description' />
                        </Form.Item>
                        <div>
                            <h3>Les techniciens doivent détenir leur carte HRAI/MOPI ainsi que celle de frigoriste compagnon.
                                Aucun technicien non qualifié n’a le droit de toucher ou manipuler des systèmes contenant des halocarbures
                            </h3>
                        </div>
                        <Form.Item label='Nom du technicien :'>
                            <Input placeholder='Saissez le nom du technicien' />
                        </Form.Item>
                        <Form.Item label='Numéro de carte HRAI/MOPI :'>
                            <Input placeholder='Saissez le numéro de carte' />
                        </Form.Item>
                        <Form.Item label='Inscrire date d’expiration si applicable :'>
                            <Space direction='vertical'>
                                <DatePicker onChange={onChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Numéro de carte de compétence en réfrigération :'>
                            <Input placeholder='Saissez le numéro de carte' />
                        </Form.Item>
                        <Form.Item label='Inscrire la date d’expiration :'>
                            <Space direction='vertical'>
                                <DatePicker onChange={onChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Type d’halocarbure :'>
                            <Input placeholder='Saisissez le type halocabure' />
                        </Form.Item>
                        <Form.Item label='Quantité d’halocarbure récupéré (lb/oz, kg/g) :'>
                            <Input placeholder='Saissez la quantité récupérée' />
                        </Form.Item>
                        <Form.Item label='Quantité d’halocarbure ajouté (lb/oz, kg/g) :'>
                            <Input placeholder='Saissez la quantité ajoutée' />
                        </Form.Item>
                        <Form.Item label='Quantité d’halocarbure perdu (lb/oz, kg/g) :'>
                            <Input placeholder='Saissez la quantité perdue' />
                        </Form.Item>
                        <Form.Item label='Description des travaux obligatoires, si vous n’avez pas assez de place utilisez l’endos de la feuille :'>
                            <Input.TextArea />
                        </Form.Item>
                        <Form.Item label='Date du rejet :'
                            name='Date_Rejet'
                            rules={[
                                {
                                    required: true,
                                    message: t('DateRejet')
                                }
                            ]}>
                            <Space direction='vertical'>
                                <DatePicker onChange={onChange} />
                            </Space>
                        </Form.Item>
                        <Form.Item label='Date finale de récuparation :'
                            name='Final_reparation'
                            rules={[
                                {
                                    required: true,
                                    message: t('FinalReparation')
                                }
                            ]}>
                            <Space direction='vertical'>
                                <DatePicker onChange={onChange} />
                            </Space>
                        </Form.Item>
                        <div>
                            <h3>L’inspecteur / coordonnateur de marché/ chargé de projet ou parrain est responsable de soumettre
                                l’étape 3 dûment remplie au GH ou son remplaçant lorsque complété dès la fin des travaux.</h3>
                        </div>

                        <Form.Item label='Nom du technicien :'
                            name='Technicien_Lastname'
                            rules={[
                                {
                                    required: true,
                                    message: t('TechnicienLastname')
                                }
                            ]}>
                            <Input placeholder='Saisissez le nom du technicien' />
                        </Form.Item>
                        <Form.Item label='Signature du technicien :'
                            name='Signature'
                            rules={[
                                {
                                    required: true,
                                    message: t('TechnicienSignature')
                                }
                            ]}>
                            <Input placeholder='Saisissez vos initiales' />
                        </Form.Item>
                        <div>
                            <h3 className={'majuscule'}>fin de l'étape 3</h3>
                        </div>
                        <Form.Item label=' ' colon={false}>
                            <Button type='primary' htmlType='submit'>
                                Enregistrer
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
};
