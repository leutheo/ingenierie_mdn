import { DownOutlined, LogoutOutlined, SettingFilled, SmileOutlined } from '@ant-design/icons';
import { Col, Divider, Dropdown, Menu, Row } from 'antd';
import { Header } from 'antd/lib/layout/layout';
import { logOutAction } from 'common/src/redux/slices';
import { GlobalReducer } from 'common/src/redux/type';
import { MenuInfo } from 'rc-menu/lib/interface';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Profile from '../formulaires/toutelistes/profile/profile';


type MenuheaderProps = {
    onclick: (menu: MenuInfo) => void;
};

export default function Headerpage(props: MenuheaderProps) {

    const dispatch = useDispatch();
    const history = useHistory();
    const userContext = useSelector((state: GlobalReducer) => state.stateReducer.userContext);

    const onFinish = () => {
        dispatch(logOutAction());
        history.push('/');
    };

    const menu = (
        <Menu>
            <Menu.Item key='0'>
                <Row>
                    <Col span={1}>
                        <SettingFilled />
                    </Col>
                    <Col>
                        <Divider type='vertical' />
                    </Col>
                    <Col span={6}>
                        Parametre
                    </Col>
                </Row>
            </Menu.Item>
            <Menu.Item key='1'>
                <Row>
                    <Col span={1}>
                        <SmileOutlined />
                    </Col>
                    <Col>
                        <Divider type='vertical' />
                    </Col>
                    <Col span={6}>
                        <Profile />
                    </Col>
                </Row>
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item key='2'>
                <span onClick={onFinish}>
                    <Row>
                        <Col className='text-center' span={1}>
                            <LogoutOutlined />
                        </Col>
                        <Col>
                            <Divider type='vertical' />
                        </Col>
                        <Col className='text-center' span={6}>
                            Deconnection
                        </Col>
                    </Row>
                </span>
            </Menu.Item>
        </Menu>
    );

    console.log(props);

    return (
        <div>
            <Header className='site-layout-background' style={{ padding: 0 }} >
                <Row className='px-2'>
                    <Col span={6} xs={{ order: 1 }} sm={{ order: 1 }} md={{ order: 1 }} lg={{ order: 1 }} />
                    <Col span={6} xs={{ order: 2 }} sm={{ order: 2 }} md={{ order: 2 }} lg={{ order: 2 }} />
                    <Col span={10} xs={{ order: 3 }} sm={{ order: 3 }} md={{ order: 3 }} lg={{ order: 3 }} />
                    <Col span={2} xs={{ order: 4 }} sm={{ order: 4 }} md={{ order: 4 }} lg={{ order: 4 }}>
                        <Dropdown overlay={menu} overlayClassName={'profile-dropdown-nemu'}>
                            <a className='ant-dropdown-link' onClick={e => e.preventDefault()}>
                                {userContext.user?.FirstName + ' ' + userContext.user?.LastName} <DownOutlined />
                            </a>
                        </Dropdown>
                    </Col>
                </Row>
            </Header>
        </div>
    );
}
