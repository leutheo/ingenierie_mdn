import { Button, Form, Input, message, Modal, Popconfirm, Space, Table } from 'antd';
import { UserModel } from 'common';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';



export default function Listeutilisateur() {
    //Création des objets
    const { t } = useTranslation();

    const [users, setUsers] = useState(undefined as UserModel[] | undefined);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [user, setUser] = useState(undefined as UserModel | undefined);

    const [form] = Form.useForm();


    //Obtention de la liste des utilisateur
    useEffect(() => {
        GetListUsers();
    }, []);

    const GetListUsers = async () => {
        const result = await fetch('/api/user', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(UserModel.fromJSON);
            setUsers(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('UserListsEmpty'),
            });
        }

    };

    //Gestion affichage de l'utilisateur sélectioné en modal

    const showModal = (userId: number) => {

        if (users !== undefined) {
            users.forEach((value) => {
                if (value.UserId === userId) {
                    setUser(value);
                }
            });

            setIsModalVisible(true);

        }
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //Gestion modification de l'utilisateur
    const onFinish = async (value: UserModel) => {
        setUser(value);
        value.RoleId = 1;
        if (user !== undefined) {
            value.UserId = user.UserId;
        }
        const object = JSON.stringify(value);
        const result = await (await fetch('/api/user', {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: object,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setIsModalVisible(false);
            Modal.success({
                content: t('ConfirmModificationUser'),
            });
            GetListUsers();
        } else {
            Modal.error({
                content: t('ConfirmModificationEchecUser'),
            });
        }
    };

    //Gestion de la supression de l'utilisateur
    const confirm = async (userId: number) => {
        await fetch('/api/user/' + userId, {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        GetListUsers();
    };

    function cancel() {
        message.error(t('CancelDeleteList'));
    }


    //Affichage de la liste des utilisateurs
    const { Column } = Table;


    return (
        <div id='contenaireliste'>
            <div>
                <h1>{t('UserListsTitle')}</h1>
            </div>

            {users !== undefined ? <Table dataSource={users} rowKey={user => user.UserId} >
                <Column title={t('UsernameList')} dataIndex='Email' key='Email' />
                <Column title={t('UserFirstNameList')} dataIndex='FirstName' key='FirstName' />
                <Column title={t('UserLastNameList')} dataIndex='LastName' key='LastName' />
                <Column title={t('UserCompanyList')} dataIndex='CompanyName' key='CompanyName' />
                <Column
                    title={t('UserActionList')}
                    dataIndex='UserId'
                    key='Action'
                    render={(UserId: number) => (
                        <Space size='middle'>
                            <a onClick={() => showModal(UserId)}>{t('UserEditList')}</a>

                            <Popconfirm
                                title={t('ModalDeleteList')}
                                onConfirm={() => confirm(UserId)}
                                onCancel={cancel}
                                okText={t('OkText')}
                                cancelText={t('CancelText')}
                            >
                                <a>{t('UserRemoveList')}</a>
                            </Popconfirm>
                            <span>{t('UserStatusList')}</span>
                        </Space>
                    )}
                />
            </Table> : t('StateListUser')}

            <Modal title={t('ModalEditList')}
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}>

                <Form form={form} name='complex-form' labelCol={{ span: 12 }} wrapperCol={{ span: 8 }} initialValues={user} onFinish={onFinish}>
                    <Form.Item label={t('LabelAccountUserNameCreate')} rules={[
                        {
                            required: true,
                            type: 'email',
                            message: t('ValidEmailAccountUserNameCreate')
                        }
                    ]} name='Email'>
                        <Input required={true} placeholder={t('PlaceHolderAccountUserNameCreate')} />
                    </Form.Item>
                    <Form.Item label={t('LabelAccountLastNameCreate')} name='LastName' rules={[
                        {
                            required: true,
                            message: t('PlaceHolderAccountLastNameCreate')
                        }
                    ]}>
                        <Input type='text' placeholder={t('PlaceHolderAccountLastNameCreate')} />
                    </Form.Item>
                    <Form.Item label={t('LabelAccountFirstNameCreate')} name='FirstName' rules={[
                        {
                            required: true,
                            message: t('PlaceHolderAccountFirstNameCreate')
                        }
                    ]}>
                        <Input type='text' placeholder={t('PlaceHolderAccountFirstNameCreate')} />
                    </Form.Item>
                    <Form.Item label={t('LabelUserCreateRole')} name='Role' rules={[
                        {
                            required: true,
                            message: t('PlaceHolderUserCreateRole')
                        }
                    ]}>
                        <Input type='text' placeholder={t('PlaceHolderUserCreateRole')} />
                    </Form.Item>
                    <Form.Item label={t('LabelAccountCompanyNameCreate')} name='CompanyName' rules={[
                        {
                            required: true,
                            message: t('PlaceHolderAccountCompanyNameCreate')
                        }
                    ]}
                    >
                        <Input type='text' placeholder={t('PlaceHolderAccountCompanyNameCreate')} />
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit' >
                            {t('SubmitrequestCreate')}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>

        </div>
    );

}
