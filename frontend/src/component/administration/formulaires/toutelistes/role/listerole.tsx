import { Button, Form, Input, Modal, Popconfirm, Space, Table } from 'antd';
import { RoleModel } from 'common';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

export default function Listerole() {
    const { t } = useTranslation();

    const [roles, setRoles] = useState(undefined as RoleModel[] | undefined);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [role, setRole] = useState(undefined as RoleModel | undefined);

    const [form] = Form.useForm();

    //Obtention de la liste des roles
    useEffect(() => {
        GetListRoles();
    }, []);

    const GetListRoles = async () => {
        const result = await fetch('/api/role', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(RoleModel.fromJSON);
            setRoles(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('RoleListsEmpty'),
            });
        }
    };

    //Gestion affichage de la role sélectionée en modal

    const showModal = (roleId: number) => {

        if (roles !== undefined) {
            roles.forEach((value) => {
                if (value.RoleId === roleId) {
                    setRole(value);
                }
            });

            setIsModalVisible(true);

        }
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //Gestion modification de la role
    const onFinish = async (value: RoleModel) => {
        setRole(value);
        value.RoleId = 1;
        if (role !== undefined) {
            value.RoleId = role.RoleId;
        }
        const object = JSON.stringify(value);
        const result = await (await fetch('/api/role', {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: object,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setIsModalVisible(false);
            Modal.success({
                content: t('ConfirmModificationRole'),
            });
            GetListRoles();
        } else {
            Modal.error({
                content: t('ConfirmModificationEchecRole'),
            });
        }
    };

    //Gestion de la supression d'une role
    const confirm = async (roleId: number) => {
        await fetch('/api/role/' + roleId, {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        GetListRoles();
    };

    //Affichage de la liste des roles militaire
    const { Column } = Table;

    return (
        <div id='contenaireliste'>
            <div>
                <h1>{t('ListRole')}</h1>
            </div>

            {roles !== undefined ? <Table dataSource={roles} rowKey={role => role.RoleId}>
                <Column title={t('RoleNameList')} dataIndex='Name' key='Name' />
                <Column
                    title={t('RoleActionList')}
                    dataIndex='RoleId'
                    key='Action'
                    render={(RoleId: number) => (
                        <Space size='middle'>
                            <a onClick={() => showModal(RoleId)}>{t('RoleEditList')}</a>

                            <Popconfirm
                                title={t('ModalDeleteRoleList')}
                                onConfirm={() => confirm(RoleId)}
                                okText={t('OkText')}
                                cancelText={t('CancelText')}
                            >
                                <a>{t('RoleRemoveList')}</a>
                            </Popconfirm>
                        </Space>
                    )}
                />
            </Table> : t('StateListRole')}

            <Modal title={t('ModalEditListRole')}
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}>

                <Form form={form} name='complex-form' labelCol={{ span: 12 }} wrapperCol={{ span: 8 }} initialValues={role} onFinish={onFinish}>
                    <Form.Item label={t('LabelNameRole')} rules={[
                        {
                            required: true,
                            message: t('ValidRoleName')
                        }
                    ]} name='Name'>
                        <Input required={true} placeholder={t('PlaceHolderRoleName')} />
                    </Form.Item>

                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit' >
                            {t('SubmitrequestCreate')}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
