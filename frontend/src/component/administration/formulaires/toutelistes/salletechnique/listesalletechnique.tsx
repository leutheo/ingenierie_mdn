import { UploadOutlined } from '@ant-design/icons';
import { Button, Form, Input, message, Modal, Popconfirm, Select, Space, Table, Upload } from 'antd';
import { BaseModel, SalleTechniqueModel } from 'common';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { Option } = Select;

export default function Listesalletechnique() {

    const { t } = useTranslation();

    //Gestion chargement fichier
    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    const { Column } = Table;

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [salletechniques, setSalleTechniques] = useState(undefined as SalleTechniqueModel[] | undefined);

    const [salletechnique, setSalleTechnique] = useState(undefined as SalleTechniqueModel | undefined);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [form] = Form.useForm();

    //Obtention de la liste des salles techniques
    useEffect(() => {
        GetListSalleTechniques();
    }, []);

    const GetListSalleTechniques = async () => {
        const result = await fetch('/api/salletechnique', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(SalleTechniqueModel.fromJSON);
            setSalleTechniques(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('TechnicalRoomListEmpty'),
            });
        }
    };

    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    //Gestion affichage de la salle technique sélectionée en modal

    const showModal = (salleTechniqueId: number) => {

        if (salletechniques !== undefined) {
            salletechniques.forEach((value) => {
                if (value.Salle_TechniqueId === salleTechniqueId) {
                    setSalleTechnique(value);
                }
            });

            setIsModalVisible(true);

        }
    };

    //Gestion de la supression d'une salle technique
    const confirm = async (Salle_TechniqueId: number) => {
        await fetch('/api/salletechnique/' + Salle_TechniqueId, {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        GetListSalleTechniques();
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //Gestion modification de la salle technique

    const onFinish = async (values: SalleTechniqueModel) => {
        debugger;
        setSalleTechnique(values);
        const docJson = JSON.stringify(values);
        const result = await (await fetch('/api/salletechnique', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: docJson,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            Modal.success({
                content: t('ConfirmCreateSalletechnique'),
            });

        } else {
            Modal.error({
                content: t('ConfirmCreateEchecSalletechnique'),
            });
        }
    };

    return (
        <div id='contenaireliste'>
            <div>
                <h1>{t('ListTechnicalRoomTitle')}</h1>
            </div>
            {salletechniques !== undefined ? <Table dataSource={salletechniques} rowKey={salletechnique => salletechnique.Salle_TechniqueId}>
                <Column title={t('TechnicalRoomNameList')} dataIndex='Name' key='Name' />
                <Column title={t('TechnicalRoomList')} dataIndex='Name' key='Name' />
                <Column
                    title={t('TechnicalRoomActionList')}
                    dataIndex='Salle_TechniqueId'
                    key='Actions'
                    render={(Salle_TechniqueId: number) => (
                        <Space size='middle'>
                            <a onClick={() => showModal(Salle_TechniqueId)}>{t('TechnicalRoomEditList')}</a>
                            <Popconfirm
                                title={t('ModalDeleteList')}
                                onConfirm={() => confirm(Salle_TechniqueId)}
                                okText={t('OkText')}
                                cancelText={t('CancelText')}
                            >
                                <a>{t('TechnicalRoomRemoveList')}</a>
                            </Popconfirm>

                        </Space>
                    )}
                />
            </Table> : t('StateListSalleTechnique')}
            <Modal title={t('ModalEditListDevis')}
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={salletechnique} onFinish={onFinish}>
                    <Form.Item label={t('LabelNameNewDocumentTechnicalRoom')} name='Name'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidTitleNewDocumentTechnicalRoomNameCreate')
                            }
                        ]}>
                        <Upload {...props}>
                            <Button icon={<UploadOutlined />}>{t('ClickToUpload')}</Button>
                        </Upload>
                    </Form.Item>

                    <h2>{t('CreateNewLinkTechnicalRoomTitle')}</h2>
                    <Form.Item label={t('LabelNameNewLinkTechnicalRoom')} name='Name'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidTitleNewLinkTechnicalRoomNameCreate')
                            }
                        ]}>
                        <Upload {...props}>
                            <Button icon={<UploadOutlined />}>{t('ClickToUpload')}</Button>
                        </Upload>
                    </Form.Item>

                    <Form.Item label={t('LabelBaseMilitaireTechnicalRoom')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['BaseId']}
                                noStyle={true}
                                rules={[{ required: true, message: t('ValidMilitaryBaseTechnicalRoom') }]}
                            >
                                <Select
                                    placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                    {bases?.map(base => {
                                        return (
                                            <Option key={base.BaseId} value={base.BaseId}>
                                                {base.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
