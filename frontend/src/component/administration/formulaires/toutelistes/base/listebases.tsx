import { Button, Form, Input, message, Modal, Popconfirm, Space, Table } from 'antd';
import { BaseModel } from 'common';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

export default function Listebases() {
    const { t } = useTranslation();

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [base, setBase] = useState(undefined as BaseModel | undefined);

    const [form] = Form.useForm();

    //Obtention de la liste des bases
    useEffect(() => {
        GetListBases();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('ContactListsEmpty'),
            });
        }
    };

    //Gestion affichage de la base sélectionée en modal

    const showModal = (baseId: number) => {

        if (bases !== undefined) {
            bases.forEach((value) => {
                if (value.BaseId === baseId) {
                    setBase(value);
                }
            });

            setIsModalVisible(true);

        }
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //Gestion modification de la base
    const onFinish = async (value: BaseModel) => {
        setBase(value);
        value.BaseId = 1;
        if (base !== undefined) {
            value.BaseId = base.BaseId;
        }
        const object = JSON.stringify(value);
        const result = await (await fetch('/api/base', {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: object,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setIsModalVisible(false);
            Modal.success({
                content: t('ConfirmModificationBase'),
            });
            GetListBases();
        } else {
            Modal.error({
                content: t('ConfirmModificationEchecBase'),
            });
        }
    };

    //Gestion de la supression d'une base
    const confirm = async (baseId: number) => {
        await fetch('/api/base/' + baseId, {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        GetListBases();
    };

    function cancel() {
        message.error(t('CancelDeleteList'));
    }

    //Affichage de la liste des bases militaire
    const { Column } = Table;

    return (
        <div id='contenaireliste'>
            <div>
                <h1>{t('ListBaseMilitaire')}</h1>
            </div>

            {bases !== undefined ? <Table dataSource={bases} rowKey={base => base.BaseId}>
                <Column title={t('BaseNameList')} dataIndex='Name' key='Name' />
                <Column
                    title={t('BaseActionList')}
                    dataIndex='BaseId'
                    key='Action'
                    render={(BaseId: number) => (
                        <Space size='middle'>
                            <a onClick={() => showModal(BaseId)}>{t('BaseEditList')}</a>

                            <Popconfirm
                                title={t('ModalDeleteList')}
                                onConfirm={() => confirm(BaseId)}
                                onCancel={cancel}
                                okText={t('OkText')}
                                cancelText={t('CancelText')}
                            >
                                <a>{t('BaseRemoveList')}</a>
                            </Popconfirm>
                        </Space>
                    )}
                />
            </Table> : t('StateListBaseMilitaire')}

            <Modal title={t('ModalEditListBase')}
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}>

                <Form form={form} name='complex-form' labelCol={{ span: 12 }} wrapperCol={{ span: 8 }} initialValues={base} onFinish={onFinish}>
                    <Form.Item label={t('LabelBaseNameCreate')} rules={[
                        {
                            required: true,
                            message: t('ValidBaseNameCreate')
                        }
                    ]} name='Name'>
                        <Input required={true} placeholder={t('PlaceHolderBaseNameCreate')} />
                    </Form.Item>

                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit' >
                            {t('SubmitrequestCreate')}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
