import { Button, Form, Input, Modal, Popconfirm, Select, Space, Table } from 'antd';
import { ContactModel } from 'common';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { Option } = Select;

export default function Listecontact() {
    //Création des objets
    const { t } = useTranslation();

    const [contacts, setContacts] = useState(undefined as ContactModel[] | undefined);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [contact, setContact] = useState(undefined as ContactModel | undefined);

    const [form] = Form.useForm();

    //Obtention de la liste des contacts
    useEffect(() => {
        GetListContacts();
    }, []);

    const GetListContacts = async () => {
        const result = await fetch('/api/contact', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any).map(ContactModel.fromJSON);
            setContacts(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('ContactListsEmpty'),
            });
        }
    };

    //Gestion affichage de l'utilisateur sélectioné en modal

    const showModal = (contactId: number) => {

        if (contacts !== undefined) {
            contacts.forEach((value) => {
                if (value.ContactId === contactId) {
                    setContact(value);
                }
            });

            setIsModalVisible(true);

        }
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //Gestion modification du contact
    const onFinish = async (value: ContactModel) => {
        setContact(value);
        if (contact !== undefined) {
            value.ContactId = contact.ContactId;
        }
        const object = JSON.stringify(value);
        const result = await (await fetch('/api/contact', {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: object,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setIsModalVisible(false);
            Modal.success({
                content: t('ConfirmModificationUser'),
            });
            GetListContacts();

        } else {
            Modal.error({
                content: t('ConfirmModificationEchecUser'),
            });
        }
    };

    //Gestion de la supression du contact
    const confirm = async (contactId: number) => {
        await fetch('/api/contact/' + contactId, {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        GetListContacts();
    };
    //Affichage de la liste des utilisateurs
    const { Column } = Table;

    return (
        <div id='contenaireliste'>
            <div>
                <h1>{t('ContactListsTitle')}</h1>
            </div>

            {contacts !== undefined ? <Table dataSource={contacts} rowKey={contact => contact.ContactId}>
                <Column title={t('ContactFisrtNameList')} dataIndex='FirstName' key='FirstName' />
                <Column title={t('ContactLastNameList')} dataIndex='lastName' key='lastName' />
                <Column title={t('ContactEmailList')} dataIndex='Email' key='Email' />
                <Column title={t('ContactPhoneNumberList')} dataIndex='Phone_Number' key='Phone_Number' />
                <Column
                    title={t('ContactActionList')}
                    dataIndex='ContactId'
                    key='Action'
                    render={(ContactId: number) => (
                        <Space size='middle'>
                            <a onClick={() => showModal(ContactId)}>{t('ContactEditList')}</a>

                            <Popconfirm
                                title={t('ContactModalDeleteList')}
                                onConfirm={() => confirm(ContactId)}
                                okText={t('OkText')}
                                cancelText={t('CancelText')}
                            >
                                <a>{t('ContactRemoveList')}</a>
                            </Popconfirm>
                        </Space>
                    )}
                />
            </Table> : t('StateListContact')}

            <Modal title={t('ModalEditListContact')}
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={contact} onFinish={onFinish}>
                    <Form.Item label={t('LabelLastNameContact')} name='LastName'
                        rules={[
                            {
                                required: true,
                                message: t('ValidLastNameContactCreate')
                            }
                        ]} >
                        <Input type='text' placeholder={t('PlaceholderLastNameContact')} />
                    </Form.Item>
                    <Form.Item label={t('LabelFirstnameContact')} name='Firstname'
                        rules={[
                            {
                                required: true,
                                message: t('ValidFirstNameContactCreate')
                            }
                        ]}>
                        <Input placeholder={t('PlaceholderFirstnameContact')} />
                    </Form.Item>
                    <Form.Item label={t('LabelEmailContact')} name='Email'
                        rules={[
                            {
                                required: true,
                                message: t('ValidEmailContactCreate')
                            }
                        ]}>
                        <Input placeholder={t('PlaceholderEmailContact')} />
                    </Form.Item>
                    <Form.Item label={t('LabelPhoneContact')} name='Phone_Number'
                        rules={[
                            {
                                required: true,
                                message: t('ValidPhonenumberContactCreate')
                            }
                        ]}>
                        <Input placeholder={t('PlaceholderPhoneContact')} />
                    </Form.Item>
                    <Form.Item label={t('LabelBaseMilitaireContact')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['BaseId']}
                                noStyle={true}
                                rules={[{ required: true, message: t('RequireMessageContact') }]}
                            >
                                <Select placeholder={t('PlaceholderBaseMilitaireContact')}>
                                    <Option value='Bagotville'>Bagotville</Option>
                                    <Option value='Montréal'>Montréal</Option>
                                    <Option value='Saint-Jean'>Saint-Jean</Option>
                                    <Option value='Valcartier'>Valcartier</Option>
                                    <Option value='RDDC-Valcartier'>RDDC Valcartier</Option>
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('SubmitrequestCreate')}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
