import { Modal, Popconfirm, Space, Table } from 'antd';
import { DocumentModel } from 'common';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Formulairedocument from '../../tousformulaires/document/formulairedocument';

// const { Option } = Select;

export default function Listedocument() {
    const { t } = useTranslation();

    const { Column } = Table;

    // const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [documents, setDocuments] = useState(undefined as DocumentModel[] | undefined);

    const [document, setDocument] = useState(undefined as DocumentModel | undefined);

    const [isModalVisible, setIsModalVisible] = useState(false);

    //Obtention de la liste des documents
    useEffect(() => {
        GetListDocuments();
    }, []);

    const GetListDocuments = async () => {
        const result = await fetch('/api/document', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(DocumentModel.fromJSON);
            setDocuments(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('DocumentListEmpty'),
            });
        }
    };

    const showModal = (documentId: number) => {

        if (documents !== undefined) {
            documents.forEach((value) => {
                if (value.DocumentId === documentId) {
                    setDocument(value);
                }
            });

            setIsModalVisible(true);

        }
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };


    //Gestion modification du document
    // const onFinish = async (value: DocumentModel) => {
    //     setDocument(value);
    //     value.DocumentId = 1;
    //     if (document !== undefined) {
    //         value.DocumentId = document.DocumentId;
    //     }
    //     const object = JSON.stringify(value);
    //     const result = await (await fetch('/api/document', {
    //         method: 'PUT',
    //         headers: {
    //             'content-type': 'application/json'
    //         },
    //         body: object,
    //         credentials: 'include'
    //     })).json();

    //     if (result !== undefined) {
    //         setIsModalVisible(false);
    //         Modal.success({
    //             content: t('ConfirmModificationBase'),
    //         });
    //         GetListDocuments();
    //     } else {
    //         Modal.error({
    //             content: t('ConfirmModificationEchecBase'),
    //         });
    //     }
    // };

    //Gestion de la supression d'un document
    const confirm = async (documentId: number) => {
        await fetch('/api/document/' + documentId, {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        GetListDocuments();
    };

    return (
        <div id='contenaireliste'>
            <div>
                <h1>{t('ListDocumentTitle')}</h1>
            </div>
            {documents !== undefined ? <Table dataSource={documents} rowKey={document => document.DocumentId}>
                <Column title={t('DocumentNameList')} dataIndex='Titre' key='Titre' />
                <Column title={t('DocumentDescriptionList')} dataIndex='Description' key='Description' />
                <Column
                    title={t('DocumentActionList')}
                    dataIndex='DocumentId'
                    key='Actions'
                    render={(DocumentId: number) => (
                        <Space size='middle'>
                            <a onClick={() => showModal(DocumentId)}>{t('DocumentEditList')}</a>
                            <Popconfirm
                                title={t('ModalDeleteList')}
                                onConfirm={() => confirm(DocumentId)}
                                okText={t('OkText')}
                                cancelText={t('CancelText')}
                            >
                                <a>{t('DocumentRemoveList')}</a>
                            </Popconfirm>
                        </Space>
                    )}
                />
            </Table> : t('StateListDocument')}

            <Modal title={t('ModalEditList')}
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}
            >
                <Formulairedocument document={document} />
            </Modal>
        </div>
    );
}
