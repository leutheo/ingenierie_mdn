import { Button, Form, Input, Modal, Popconfirm, Select, Space, Table } from 'antd';
import { BaseModel, SectionDevisModel } from 'common';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { Option } = Select;

export default function Listesectiondevis() {
    const { t } = useTranslation();

    const { Column } = Table;

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [sectiondeviss, setSectiondeviss] = useState(undefined as SectionDevisModel[] | undefined);

    const [sectiondevis, setSectiondevis] = useState(undefined as SectionDevisModel | undefined);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [form] = Form.useForm();

    //Obtention de la liste des devis
    useEffect(() => {
        GetListSectionDeviss();
    }, []);

    const GetListSectionDeviss = async () => {
        const result = await fetch('/api/sectiondevis', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(SectionDevisModel.fromJSON);
            setSectiondeviss(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('DevisListEmpty'),
            });
        }
    };

    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    //Gestion affichage du devis sélectioné en modal

    const showModal = (sectiondevisId: number) => {

        if (sectiondeviss !== undefined) {
            sectiondeviss.forEach((value) => {
                if (value.Section_DevisId === sectiondevisId) {
                    setSectiondevis(value);
                }
            });

            setIsModalVisible(true);

        }
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //Gestion modification du devis
    const onFinish = async (value: SectionDevisModel) => {
        debugger;
        setSectiondevis(value);
        value.Section_DevisId = 1;
        if (sectiondevis !== undefined) {
            value.Section_DevisId = sectiondevis.Section_DevisId;
        }
        const docJson = JSON.stringify(value);
        const result = await (await fetch('/api/sectiondevis', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: docJson,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setIsModalVisible(false);
            Modal.success({
                content: t('ConfirmCreateSectiondevis'),
            });
            GetListSectionDeviss();
        } else {
            Modal.error({
                content: t('ConfirmCreateEchecSectiondevis'),
            });
        }
    };

    //Gestion de la supression d'un devis
    const confirm = async (Section_DevisId: number) => {
        await fetch('/api/sectiondevis/' + Section_DevisId, {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        GetListSectionDeviss();
    };

    return (
        <div id='contenaireliste'>
            <div>
                <h1>{t('ListDevisTitle')}</h1>
            </div>
            {sectiondeviss !== undefined ? <Table dataSource={sectiondeviss} rowKey={sectiondevis => sectiondevis.Section_DevisId}>
                <Column title={t('DevisNumberList')} dataIndex='Numero' key='Numero' />
                <Column title={t('DevisNameList')} dataIndex='Titre' key='Titre' />
                <Column
                    title={t('DevisActionList')}
                    dataIndex='Section_DevisId'
                    key='Actions'
                    render={(Section_DevisId: number) => (
                        <Space size='middle'>
                            <a onClick={() => showModal(Section_DevisId)}>{t('DevisEditList')}</a>

                            <Popconfirm
                                title={t('ModalDeleteList')}
                                onConfirm={() => confirm(Section_DevisId)}
                                okText={t('OkText')}
                                cancelText={t('CancelText')}
                            >
                                <a>{t('DevisRemoveList')}</a>
                            </Popconfirm>
                        </Space>
                    )}
                />
            </Table> : t('StateListSectionDevis')}
            <Modal title={t('ModalEditListDevis')}
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={sectiondevis} onFinish={onFinish}>
                    <Form.Item label={t('LabelNumeroNewDevis')} name='Numero'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidDevisNumeroCreate')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderNumeroDevis')} />
                    </Form.Item>
                    <Form.Item label={t('LabelNameNewDevis')} name='Titre'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidTitleDevisNameCreate')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderDevis')} />
                    </Form.Item>
                    <Form.Item label={t('LabelBaseMilitaireDevis')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['basemilitaire']}
                                noStyle={true}
                                rules={[{ required: true, message: t('ValidMilitaryBaseDevis') }]}
                            >
                                <Select
                                    placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                    {bases?.map(base => {
                                        return (
                                            <Option key={base.BaseId} value={base.BaseId}>
                                                {base.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );

}
