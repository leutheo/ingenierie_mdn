import { UploadOutlined } from '@ant-design/icons';
import { Button, Form, Input, message, Modal, Popconfirm, Select, Space, Table, Upload } from 'antd';
import { BaseModel, HalocarburesModel } from 'common/src/model';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { Option } = Select;

export default function Listehalocarbures() {
    const { t } = useTranslation();

    //Gestion chargement fichier
    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    const { Column } = Table;

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [halocarbures, setHalocarbures] = useState(undefined as HalocarburesModel[] | undefined);

    const [halocarbure, setHalocarbure] = useState(undefined as HalocarburesModel | undefined);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [form] = Form.useForm();

    //Obtention de la liste des halocarbures
    useEffect(() => {
        GetListHalocarbures();
    }, []);

    const GetListHalocarbures = async () => {
        const result = await fetch('/api/halocarbures', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(HalocarburesModel.fromJSON);
            setHalocarbures(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('FormListEmpty'),
            });
        }
    };

    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    //Gestion affichage du halocarbure sélectioné en modal

    const showModal = (formulaireId: number) => {

        if (halocarbures !== undefined) {
            halocarbures.forEach((value) => {
                if (value.FormulaireId === formulaireId) {
                    setHalocarbure(value);
                }
            });

            setIsModalVisible(true);

        }
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };


    //Gestion modification du halocarbure
    const onFinish = async (value: HalocarburesModel) => {
        setHalocarbure(value);
        value.FormulaireId = 1;
        if (halocarbure !== undefined) {
            value.FormulaireId = halocarbure.FormulaireId;
        }
        const object = JSON.stringify(value);
        const result = await (await fetch('/api/halocarbures', {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: object,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setIsModalVisible(false);
            Modal.success({
                content: t('ConfirmModificationBase'),
            });
            GetListHalocarbures();
        } else {
            Modal.error({
                content: t('ConfirmModificationEchecBase'),
            });
        }
    };

    //Gestion de la supression d'un halocarbure
    const confirm = async (formulaireId: number) => {
        await fetch('/api/halocarbures/' + formulaireId, {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        GetListHalocarbures();
    };

    return (
        <div id='contenaireliste'>
            <div>
                <h1>{t('ListHalocarburesTitle')}</h1>
            </div>
            {halocarbures !== undefined ? <Table dataSource={halocarbures} rowKey={halocarbures => halocarbures.FormulaireId}>
                <Column title={t('HalocarburesNameList')} dataIndex='Name' key='Name' />
                <Column title={t('HalocarburesList')} dataIndex='Name' key='Name' />
                <Column
                    title={t('HalocarburesActionList')}
                    dataIndex='FormulaireId'
                    key='Actions'
                    render={(FormulaireId: number) => (
                        <Space size='middle'>
                            <a onClick={() => showModal(FormulaireId)}>{t('HalocarburesEditList')}</a>
                            <Popconfirm
                                title={t('ModalDeleteList')}
                                onConfirm={() => confirm(FormulaireId)}
                                okText={t('OkText')}
                                cancelText={t('CancelText')}
                            >
                                <a>{t('HalocarburesRemoveList')}</a>
                            </Popconfirm>

                        </Space>
                    )}
                />
            </Table> : t('StateListHalocarbures')}
            <Modal title={t('ModalEditList')}
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}
            >
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={halocarbure} onFinish={onFinish}>
                    <Form.Item label={t('LabelNameNewhalocarbures')} name='Principal_form'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidTitleFormNameCreate')
                            }
                        ]}>
                        <Upload {...props}>
                            <Button icon={<UploadOutlined />}>{t('ClickToUpload')}</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item label={t('LabelDescriptionForm')} name='Description'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidDescriptionForm')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderDescriptionForm')} />
                    </Form.Item>
                    <Form.Item label={t('LabelBaseMilitaire')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['BaseId']}
                                noStyle={true}
                                rules={[{ required: true, message: t('ValidMilitaryBase') }]}
                            >
                                <Select
                                    placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                    {bases?.map(base => {
                                        return (
                                            <Option key={base.BaseId} value={base.BaseId}>
                                                {base.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
