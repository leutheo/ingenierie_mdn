import { Button, Form, Input, message, Modal, Popconfirm, Space, Table } from 'antd';
import { SectionDevisGroupeModel } from 'common';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';


export default function Listesectiondevisgroupe() {
    const { t } = useTranslation();

    const [sectiondevisgroupes, setsectiondevisgroupes] = useState(undefined as SectionDevisGroupeModel[] | undefined);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [sectiondevisgroupe, setsectiondevisgroupe] = useState(undefined as SectionDevisGroupeModel | undefined);

    const [form] = Form.useForm();

    //Obtention de la liste de section devis
    useEffect(() => {
        GetListSectionDevisGroupes();
    }, []);

    const GetListSectionDevisGroupes = async () => {
        const result = await fetch('/api/sectiondevisgroupe', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(SectionDevisGroupeModel.fromJSON);
            setsectiondevisgroupes(values);
        } else {
            Modal.warning({
                title: t('WarningMessage'),
                content: t('ContactListsEmpty'),
            });
        }
    };

    //Gestion affichage de la section devis sélectionée en modal

    const showModal = (sectiondevisgroupeId: number) => {

        if (sectiondevisgroupes !== undefined) {
            sectiondevisgroupes.forEach((value) => {
                if (value.Section_Devis_GroupeId === sectiondevisgroupeId) {
                    setsectiondevisgroupe(value);
                }
            });

            setIsModalVisible(true);

        }
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //Gestion modification du groupe devis
    const onFinish = async (value: SectionDevisGroupeModel) => {
        setsectiondevisgroupe(value);
        value.Section_Devis_GroupeId = 1;
        if (sectiondevisgroupe !== undefined) {
            value.Section_Devis_GroupeId = sectiondevisgroupe.Section_Devis_GroupeId;
        }
        const object = JSON.stringify(value);
        const result = await (await fetch('/api/sectiondevisgroupe', {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: object,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setIsModalVisible(false);
            Modal.success({
                content: t('ConfirmModificationSectionDevisGroupe'),
            });
            GetListSectionDevisGroupes();
        } else {
            Modal.error({
                content: t('ConfirmModificationEchecSectionDevisGroupe'),
            });
        }
    };

    //Gestion de la supression d'une section devis
    const confirm = async (sectiondevisgroupeId: number) => {
        await fetch('/api/sectiondevisgroupe/' + sectiondevisgroupeId, {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        GetListSectionDevisGroupes();
    };

    function cancel() {
        message.error(t('CancelDeleteList'));
    }

    //Affichage de la liste des section de devis groupe
    const { Column } = Table;
    return (
        <div id='contenaireliste'>
            <div>
                <h1>{t('ListSectionDevisGroup')}</h1>
            </div>

            {sectiondevisgroupes !== undefined ? <Table dataSource={sectiondevisgroupes} rowKey={sectiondevisgroupe => sectiondevisgroupe.Section_Devis_GroupeId}>
                <Column title={t('SectionDevisGrouNameList')} dataIndex='Name' key='Name' />
                <Column
                    title={t('SectionDevisGrouActionList')}
                    dataIndex='SectionDevisGroupeId'
                    key='Action'
                    render={(SectionDevisGroupeId: number) => (
                        <Space size='middle'>
                            <a onClick={() => showModal(SectionDevisGroupeId)}>{t('SectionDevisGroupEditList')}</a>

                            <Popconfirm
                                title={t('ModalDeleteList')}
                                onConfirm={() => confirm(SectionDevisGroupeId)}
                                onCancel={cancel}
                                okText={t('OkText')}
                                cancelText={t('CancelText')}
                            >
                                <a>{t('SectionDevisGroupRemoveList')}</a>
                            </Popconfirm>
                        </Space>
                    )}
                />
            </Table> : t('StateListSectionDevisGrou')}

            <Modal title={t('ModalEditListSectionDevisGroup')}
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}>

                <Form form={form} name='complex-form' labelCol={{ span: 12 }} wrapperCol={{ span: 8 }} initialValues={sectiondevisgroupe} onFinish={onFinish}>
                    <Form.Item label={t('LabelSectionDevisGroupNameCreate')} rules={[
                        {
                            required: true,
                            message: t('ValidSectionDevisGrouNameCreate')
                        }
                    ]} name='Name'>
                        <Input required={true} placeholder={t('PlaceHolderSectionDevisGroupNameCreate')} />
                    </Form.Item>

                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit' >
                            {t('SubmitrequestCreate')}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
