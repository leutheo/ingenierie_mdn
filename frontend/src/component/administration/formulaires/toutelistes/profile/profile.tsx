import { Col, Modal, Row } from 'antd';
import { GlobalReducer } from 'common/src/redux/type';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';


export default function Profile() {
    //Création des objets

    const userContext = useSelector((state: GlobalReducer) => state.stateReducer.userContext);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    // const { t } = useTranslation();

    // const [user, setUsers] = useState(undefined as UserModel[] | undefined);

    // const [user, setUser] = useState(undefined as UserModel | undefined);

    // //Obtention de la liste des utilisateur
    // useEffect(() => {
    //     GetListUser();
    // }, []);

    // const GetListUser = async () => {
    //     const result = await fetch('/api/user/' + props.userId, {
    //         method: 'GET',
    //         headers: {
    //             'content-type': 'application/json'
    //         },
    //         credentials: 'include'
    //     });
    //     //valider que result n'est pas null
    //     if (result.ok) {
    //         const value = (await result.json() as any[]).map(UserModel.fromJSON);
    //         setUser(value);
    //         console.log(user);
    //     } else {
    //         //alert(t('Validresult'));
    //         Modal.warning({
    //             title: t('WarningMessage'),
    //             content: t('UserListsEmpty'),
    //         });
    //     }

    // };

    return (
        <div id='contenaireliste'>
            <span onClick={showModal}>
                Profil
            </span>

            <Modal title='Profil'
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                centered={true}>

                <Row>
                    <Col>
                        <h5>Prénom :</h5>
                    </Col>
                    <Col>
                        <h5>{userContext.user?.FirstName}</h5>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h5>Nom :</h5>
                    </Col>
                    <Col>
                        <h5>{userContext.user?.LastName}</h5>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h5>Nom Utilisateur :</h5>
                    </Col>
                    <Col>
                        <h5>{userContext.user?.Email}</h5>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h5>Compagnie :</h5>
                    </Col>
                    <Col>
                        <h5>{userContext.user?.CompanyName}</h5>
                    </Col>
                </Row>
            </Modal>
        </div>
    );
}
