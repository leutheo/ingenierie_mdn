import { Button, Form } from 'antd';
import React from 'react';


const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

export default function Enregistrer() {
    return (
        <Form {...layout} name='enregistrer'>
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button type='primary' htmlType='submit'>
                    Enregistrer
                </Button>
            </Form.Item>
        </Form>
    );
}
