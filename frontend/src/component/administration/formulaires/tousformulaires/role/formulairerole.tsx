import { Button, Form, Input, Modal } from 'antd';
import { RoleModel } from 'common';
import { default as React, useState } from 'react';
import { useTranslation } from 'react-i18next';


export default function Formulairerole() {
    const { t } = useTranslation();

    const [role, setRole] = useState(undefined as RoleModel | undefined);

    const [form] = Form.useForm();

    const onFinish = async (values: RoleModel) => {
        setRole(values);
        const va = JSON.stringify(values);
        const result = await (await fetch('/api/role', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: va,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setRole(new RoleModel());
            form.resetFields();
            Modal.success({
                content: t('ConfirmCreateRole'),
            });

        } else {
            Modal.error({
                content: t('ConfirmCreateEchecRole'),
            });
        }
    };

    return (
        <div id='contenaireformulaire'>
            <div>
                <h1>{t('CreateRoleTitle')}</h1>
            </div>
            <div>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={role} onFinish={onFinish}>
                    <Form.Item label={t('LabelNameRole')} name='Name'
                        rules={[{
                            required: true,
                            type: 'string',
                            message: t('RuleNameRole')
                        }]}>
                        <Input placeholder={t('PlaceHolderNameRole')} />
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
