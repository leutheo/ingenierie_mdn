import { UploadOutlined } from '@ant-design/icons';
import { Button, Form, Input, message, Modal, Select, Upload } from 'antd';
import { BaseModel, CONSTANT, DocumentModel } from 'common/src';
import { dirname, extname } from 'path';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { Option } = Select;

interface DocumentProps {
    document: DocumentModel | undefined;
}

export default function Formulairedocument(documentProps: DocumentProps) {

    const [form] = Form.useForm();

    const { t } = useTranslation();

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [document, setDocument] = useState(documentProps.document as DocumentModel | undefined);

    const [fileInfo, setFileInfo] = useState(undefined as File | undefined);

    const [pageTitle, setPageTitle] = useState(t('CreateNewDocumentTitle') as string);

    let _defaultFileList: any = [];
    if (documentProps.document !== undefined) {
        _defaultFileList.push({
            uid: 1,
            name: documentProps.document.DocDesitnationPath,
            status: 'done',
            url: 'http://www.baidu.com/xxx.png',
        });
    }

    //Gestion chargement fichier
    const props = {
        name: 'file',
        // action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        action: '/api/document/add',
        multiple: false,
        accept: '.jpeg,.jpg,.png,.pdf,.ofd,image/jpeg,image/jpg,image/png,application/pdf,',
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
                setFileInfo(info.file);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
        defaultFileList: _defaultFileList

    };

    useEffect(() => {
        GetListBases();
        form.resetFields();
        if (documentProps.document !== undefined) {
            setDocument(documentProps.document);
            setPageTitle("");
            form.setFieldsValue(documentProps.document);
        }

    }, [documentProps.document, form]);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    // const loadDocument = async () => {
    //     const result = await fetch('/api/document' + documentId, {
    //         method: 'GET',
    //         headers: {
    //             'content-type': 'application/json'
    //         },
    //         credentials: 'include'
    //     });
    //     //valider que result n'est pas null
    //     if (result.ok) {
    //         setDocument(await result.json());
    //     } else {
    //         //alert(t('Validresult'));
    //         Modal.warning({
    //             title: t('WarningMessage'),
    //             content: t('BaseListsEmpty'),
    //         });
    //     }
    // };

    const onFinish = async (values: DocumentModel) => {
        let file: any = fileInfo;
        console.log(dirname(file.name));
        let ext = extname(file.name);
        let filename = file.name.split(ext)[0];
        values.DocDesitnationPath = filename + CONSTANT.DOCUMENT_IMG_SEPARATOR + (new Date().getTime()).toString() + ext;

        setDocument(values);
        saveDocument(values);

    };

    const saveDocument = async (values: DocumentModel) => {
        let documentModel = {
            Titre: values.Titre,
            Description: values.Description,
            DocDesitnationPath: values.DocDesitnationPath,
            IsDelete: false,
            BaseId: values.BaseId,
            DocumentId: values.DocumentId
        };

        const docJson = JSON.stringify(documentModel);
        let formData = new FormData();
        let file: any = fileInfo;
        formData.append('file', file?.originFileObj);
        formData.append('document', docJson);

        const result = await (await fetch('/api/document', {
            method: 'POST',
            body: formData,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setDocument(new DocumentModel());
            form.resetFields();
            Modal.success({
                content: t('ConfirmCreateDocument'),
            });
        } else {
            Modal.error({
                content: t('EchecCreateDocument'),
            });
        }
    };

    return (
        <div id='contenaireformulaire'>
            <div>
                <h1>{pageTitle}</h1>
            </div>
            <div>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={document} onFinish={onFinish}>
                    <Form.Item label={t('LabelTitleDocument')} className={'d-none'} name='DocumentId'
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item label={t('LabelNameNewDocument')} name='File'
                        rules={[
                            {
                                required: true,
                                message: t('ValidTitleDocumentNameCreate')
                            }
                        ]}>
                        <Upload {...props}>
                            <Button icon={<UploadOutlined />}>{t('ClickToUpload')}</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item label={t('LabelTitleDocument')} name='Titre'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidTitleDocument')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderTitleDocument')} />
                    </Form.Item>
                    <Form.Item label={t('LabelDescriptionDocument')} name='Description'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidDescriptionDocument')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderDescriptionDocument')} />
                    </Form.Item>
                    <Form.Item label={t('LabelBaseMilitaireDocument')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['BaseId']}
                                noStyle={true}
                                rules={[{ required: true, message: t('ValidMilitaryBase') }]}
                            >
                                <Select
                                    placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                    {bases?.map(base => {
                                        return (
                                            <Option key={base.BaseId} value={base.BaseId}>
                                                {base.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
