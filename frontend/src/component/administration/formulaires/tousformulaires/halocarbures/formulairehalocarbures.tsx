import { UploadOutlined } from '@ant-design/icons';
import { Button, Form, Input, message, Modal, Select, Upload } from 'antd';
import { BaseModel, HalocarburesModel } from 'common/src/model';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';



const { Option } = Select;

export default function Formulairehalocarbures() {
    const { t } = useTranslation();

    //Gestion chargement fichier
    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [document, setDocument] = useState(undefined as HalocarburesModel | undefined);

    const [form] = Form.useForm();

    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    const onFinish = async (values: HalocarburesModel) => {
        debugger;
        setDocument(values);
        const docJson = JSON.stringify(values);
        const result = await (await fetch('/api/halocarbures', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: docJson,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            Modal.success({
                content: t('ConfirmCreateDocument'),
            });

        } else {
            Modal.error({
                content: t('ConfirmCreateEchecDocument'),
            });
        }
    };

    return (
        <div id='contenaireformulaire'>
            <div>
                <h1>{t('CreateNewFormTitle')}</h1>
            </div>
            <div>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={document} onFinish={onFinish}>
                    <Form.Item label={t('LabelNameNewhalocarbures')} name='Formulaire'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidTitleFormNameCreate')
                            }
                        ]}>
                        <Upload {...props}>
                            <Button icon={<UploadOutlined />}>{t('ClickToUpload')}</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item label={t('LabelDescriptionForm')} name='Description'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidDescriptionForm')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderDescriptionFormulaire')} />
                    </Form.Item>
                    <Form.Item label={t('LabelBaseMilitaire')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['BaseId']}
                                noStyle={true}
                                rules={[{ required: true, message: t('ValidMilitaryBase') }]}
                            >
                                <Select
                                    placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                    {bases?.map(base => {
                                        return (
                                            <Option key={base.BaseId} value={base.BaseId}>
                                                {base.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
