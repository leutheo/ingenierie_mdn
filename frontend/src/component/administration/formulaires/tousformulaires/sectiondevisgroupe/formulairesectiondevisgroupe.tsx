import { Button, Form, Input, Modal } from 'antd';
import { SectionDevisGroupeModel } from 'common';
import { default as React, useState } from 'react';
import { useTranslation } from 'react-i18next';

export default function Formulairesectiondevisgroupe() {
    const { t } = useTranslation();

    const [sectiondevisgroupe, setSectiondevisgroupe] = useState(undefined as SectionDevisGroupeModel | undefined);

    const [form] = Form.useForm();


    const onFinish = async (values: SectionDevisGroupeModel) => {
        setSectiondevisgroupe(values);
        const va = JSON.stringify(values);
        const result = await (await fetch('/api/sectiondevisgroupe', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: va,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setSectiondevisgroupe(new SectionDevisGroupeModel());
            form.resetFields();
            Modal.success({
                content: t('ConfirmCreateSectiondevisgroupe'),
            });

        } else {
            Modal.error({
                content: t('EchecCreateSectiondevisgroupe'),
            });
        }
    };


    return (
        <div id='contenaireformulaire'>
            <div>
                <h1>{t('CreateMilitarySectiondevisgroupeTitle')}</h1>
            </div>
            <div>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={sectiondevisgroupe} onFinish={onFinish}>
                    <Form.Item label={t('LabelNameSectiondevisgroupe')} name='Name'
                        rules={[{
                            required: true,
                            type: 'string',
                            message: t('RuleNameSectiondevisgroupe')
                        }]}>
                        <Input style={{}} placeholder={t('PlaceHolderNameSectiondevisgroupe')} />
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
