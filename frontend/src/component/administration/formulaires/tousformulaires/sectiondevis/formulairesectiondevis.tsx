import { UploadOutlined } from '@ant-design/icons';
import { Button, Form, Input, message, Modal, Select, Upload } from 'antd';
import { BaseModel, SectionDevisGroupeModel, SectionDevisModel } from 'common';
import { CONSTANT } from 'common/src';
import { extname } from 'path';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { Option } = Select;

export default function Formulairesectiondevis() {
    const { t } = useTranslation();
    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [groups, setGroups] = useState(undefined as SectionDevisGroupeModel[] | undefined);

    const [sectiondevis, setSectiondevis] = useState(undefined as SectionDevisModel | undefined);

    const [fileInfo, setFileInfo] = useState(undefined as File | undefined);

    const [form] = Form.useForm();

    //Gestion chargement fichier
    const props = {
        name: 'file',
        //action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        action: '/api/sectiondevis/add',
        multiple: false,
        accept: '.jpeg,.jpg,.png,.pdf,.ofd,image/jpeg,image/jpg,image/png,application/pdf,',
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
                setFileInfo(info.file);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };


    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
        GetListGroup();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    const GetListGroup = async () => {
        const result = await fetch('/api/sectiondevisgroupe', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(SectionDevisGroupeModel.fromJSON);
            setGroups(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    const onFinish = async (values: SectionDevisModel) => {

        let file: any = fileInfo;
        let ext = extname(file.name);
        let filename = file.name.split(ext)[0];
        values.DocDestinationPath = filename + CONSTANT.DOCUMENT_IMG_SEPARATOR + (new Date().getTime()).toString() + ext;

        setSectiondevis(values);
        saveSectiondevis(values);
    };


    const saveSectiondevis = async (values: SectionDevisModel) => {

        const docJson = JSON.stringify(values);
        let formData = new FormData();
        let file: any = fileInfo;
        formData.append('file', file?.originFileObj);
        formData.append('document', docJson);

        const result = await (await fetch('/api/sectiondevis', {
            method: 'POST',
            body: formData,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setSectiondevis(new SectionDevisModel());
            form.resetFields();
            Modal.success({
                content: t('ConfirmCreateSectiondevis'),
            });

        } else {
            Modal.error({
                content: t('EchecCreateSectiondevis'),
            });
        }
    };
    return (
        <div id='contenaireformulaire'>
            <div>
                <h1>{t('CreateNewQuoteTitle')}</h1>
            </div>
            <div>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={sectiondevis} onFinish={onFinish}>
                    <Form.Item label={t('LabelNumeroNewDevis')} name='Numero'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidDevisNumeroCreate')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderNumeroDevis')} />
                    </Form.Item>
                    <Form.Item label={t('LabelNameNewDevis')} name='file'
                        rules={[
                            {
                                required: true,
                                message: t('ValidTitleDevisNameCreate')
                            }
                        ]}>
                        <Upload {...props}>
                            <Button icon={<UploadOutlined />}>{t('ClickToUpload')}</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item label={t('LabelTitleDocument')} name='Titre'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidTitleDocument')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderTitleDocument')} />
                    </Form.Item>
                    <Form.Item label={t('LabelBaseMilitaireDevis')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['BaseId']}
                                noStyle={true}
                                rules={[{ required: true, message: t('ValidMilitaryBaseDevis') }]}
                            >
                                <Select
                                    placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                    {bases?.map(base => {
                                        return (
                                            <Option key={base.BaseId} value={base.BaseId}>
                                                {base.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label={t('LabelSectionDevisGroup')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['GroupId']}
                                noStyle={true}
                                rules={[{ required: true, message: t('ValidSectionDevisGroup') }]}
                            >
                                <Select
                                    placeholder={t('PlaceHolderSelectDevisGroup')}>
                                    {groups?.map(group => {
                                        return (
                                            <Option key={group.Section_Devis_GroupeId} value={group.Section_Devis_GroupeId}>
                                                {group.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
