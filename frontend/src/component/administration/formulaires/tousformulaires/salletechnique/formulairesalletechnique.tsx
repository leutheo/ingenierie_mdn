import { UploadOutlined } from '@ant-design/icons';
import { Button, Form, Input, message, Modal, Select, Upload } from 'antd';
import { BaseModel, SalleTechniqueModel } from 'common';
import { CONSTANT } from 'common/src/constant';
import { extname } from 'path';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { Option } = Select;

export default function Formulairesalletechnique() {
    const { t } = useTranslation();

    const [form] = Form.useForm();

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [salletechnique, setSalletechnique] = useState(undefined as SalleTechniqueModel | undefined);

    const [fileDocInfo, setFileDocInfo] = useState(undefined as File | undefined);

    const [fileLinkInfo, setFileLinkInfo] = useState(undefined as File | undefined);


    //Gestion chargement fichier
    const docProps = {
        name: 'file',
        action: '/api/salletechnique/add',
        multiple: false,
        accept: '.jpeg,.jpg,.png,.pdf,.ofd,image/jpeg,image/jpg,image/png,application/pdf,',
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                setFileDocInfo(info.file);
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    //Gestion chargement fichier
    const linkProps = {
        name: 'fileList',
        action: '/api/salletechnique/add',
        multiple: false,
        accept: '.jpeg,.jpg,.png,.pdf,.ofd,image/jpeg,image/jpg,image/png,application/pdf,',
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                setFileLinkInfo(info.file);
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    const onFinish = async (salleTechniqueModel: SalleTechniqueModel) => {
        let linkFile: any = fileLinkInfo;
        let docFile: any = fileDocInfo;

        let linkFileExt = extname(linkFile.name);
        let docFileExt = extname(docFile.name);

        let linkFilename = linkFile.name.split(linkFileExt)[0];
        let docFilename = docFile.name.split(docFileExt)[0];

        // let linkModel = new LinksalleTechniqueModel;
        // linkModel.DocDestinationPath = linkFilename + CONSTANT.DOCUMENT_IMG_SEPARATOR + (new Date().getTime()).toString() + linkFileExt;
        // linkModel.Name = "";

        salleTechniqueModel.DocDestinationPath = docFilename + CONSTANT.DOCUMENT_IMG_SEPARATOR + (new Date().getTime()).toString() + docFileExt;
        salleTechniqueModel.linkDocDestinationPath = linkFilename + CONSTANT.DOCUMENT_IMG_SEPARATOR + (new Date().getTime()).toString() + linkFileExt;
        setSalletechnique(salleTechniqueModel);
        const docJson = JSON.stringify(salleTechniqueModel);
        let formData = new FormData();
        formData.append('files', linkFile?.originFileObj);
        formData.append('files', docFile?.originFileObj);
        formData.append('document', docJson);

        const result = await (await fetch('/api/salletechnique', {
            method: 'POST',
            body: formData,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setSalletechnique(new SalleTechniqueModel());
            form.resetFields();
            Modal.success({
                content: t('ConfirmCreateSectiondevis'),
            });

        } else {
            Modal.error({
                content: t('ConfirmCreateEchecSectiondevis'),
            });
        }
    };

    return (
        <div id='contenaireformulaire'>
            <div>
                <h1>{t('CreateNewTechnicalRoomTitle')}</h1>
            </div>
            <div>
                <h2>{t('CreateNewDocumentTechnicalRoomTitle')}</h2>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={salletechnique} onFinish={onFinish}>
                    <Form.Item label={t('LabelNameNewDocumentTechnicalRoom')}
                        name='LinkFile'
                        rules={[
                            {
                                required: true,
                                message: t('ValidTitleNewDocumentTechnicalRoomNameCreate')
                            }
                        ]}>
                        <Upload {...docProps}>
                            <Button icon={<UploadOutlined />}>{t('ClickToUpload')}</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item label={t('LabelTitleDocument')} name='documentTitre'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidTitleDocument')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderTitleDocument')} />
                    </Form.Item>

                    <h2>{t('CreateNewLinkTechnicalRoomTitle')}</h2>
                    <Form.Item label={t('LabelNameNewLinkTechnicalRoom')}
                        name='DocFile'
                        rules={[
                            {
                                required: true,
                                message: t('ValidTitleNewLinkTechnicalRoomNameCreate')
                            }
                        ]}>
                        <Upload {...linkProps}>
                            <Button icon={<UploadOutlined />}>{t('ClickToUpload')}</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item label={t('LabelTitleDocument')} name='linkTitre'
                        rules={[
                            {
                                required: true,
                                type: 'string',
                                message: t('ValidTitleDocument')
                            }
                        ]}>
                        <Input placeholder={t('PlaceHolderTitleDocument')} />
                    </Form.Item>

                    <Form.Item label={t('LabelBaseMilitaireTechnicalRoom')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['BaseId']}
                                noStyle={true}
                                rules={[{ required: true, message: t('ValidMilitaryBaseTechnicalRoom') }]}
                            >
                                <Select
                                    placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                    {bases?.map(base => {
                                        return (
                                            <Option key={base.BaseId} value={base.BaseId}>
                                                {base.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
