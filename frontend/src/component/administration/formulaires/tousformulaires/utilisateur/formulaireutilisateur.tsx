import { Button, Form, Input, Modal, Select } from 'antd';
import { BaseModel, RoleModel, UserModel } from 'common/src';
import { default as React, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { Option } = Select;

export default function Formulaireutilisateur() {
    const { t } = useTranslation();

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [roles, setRoles] = useState(undefined as RoleModel[] | undefined);

    const [user, setUser] = useState(undefined as UserModel | undefined);

    const [form] = Form.useForm();


    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
        GetRoles();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    const GetRoles = async () => {
        const result = await fetch('/api/role', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any).map(RoleModel.fromJSON);
            setRoles(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('RoleListsEmpty'),
            });
        }

    };

    const onFinish = async (values: UserModel) => {
        setUser(values);
        values.RoleId = 1;
        values.IsActif = true;
        const va = JSON.stringify(values);
        const result = await (await fetch('/api/user', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: va,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setUser(new UserModel());
            form.resetFields();
            Modal.success({
                content: t('ConfirmCreateSuccesUser'),
            });

        } else {
            Modal.error({
                content: t('ConfirmCreateEchecUser'),
            });
        }
    };

    return (
        <div id='contenaireformulaire'>
            <div>
                <h1>{t('CreateNewUserTitle')}</h1>
            </div>
            <div>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={user} onFinish={onFinish}>
                    <Form.Item label={t('LabelNameNewUser')}
                        rules={[
                            {
                                required: true,
                                type: 'email',
                                message: t('ValidEmailAccountUserNameCreate')
                            }
                        ]} name='Email'>
                        <Input required={true} placeholder={t('PlaceHolderNameNewUser')} />
                    </Form.Item>
                    <Form.Item label={t('LabelAccountLastNameCreate')} name='LastName' rules={[
                        {
                            required: true,
                            message: t('PlaceHolderAccountLastNameCreate')
                        }
                    ]}>
                        <Input type='text' placeholder={t('PlaceHolderAccountLastNameCreate')} />
                    </Form.Item>
                    <Form.Item label={t('LabelAccountFirstNameCreate')} name='FirstName' rules={[
                        {
                            required: true,
                            message: t('PlaceHolderAccountFirstNameCreate')
                        }
                    ]}>
                        <Input type='text' placeholder={t('PlaceHolderAccountFirstNameCreate')} />
                    </Form.Item>
                    <Form.Item label={t('LabelPasswordNewUser')}
                        name='Password'
                        rules={[
                            {
                                required: true,
                                message: t('MessageRequireAccountPasswordCreate'),
                                min: 8,
                            },
                        ]}
                        hasFeedback={true}>
                        <Input type='password' placeholder={t('PlaceHolderPasswordNewUser')} />
                    </Form.Item>
                    <Form.Item label={t('LabelConfirmPasswordNewUser')}
                        name='confirm'
                        dependencies={['Password']}
                        hasFeedback={true}
                        rules={[
                            {
                                required: true,
                                message: t('ConfirmMessageRequireAccountPasswordCreate'),
                                min: 8
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('Password') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error(t('MatchRequireAccountPasswordCreate')));
                                },
                            }),
                        ]}>
                        <Input type='password' placeholder={t('PlaceHolderConfirmPasswordNewUser')} />
                    </Form.Item>
                    <Form.Item label={t('LabelRoleNewUser')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name='RoleId'
                                noStyle={true}
                                rules={[{ required: true, message: t('RuleSectionRoleNewUser') }]}
                            >
                                <Select placeholder={t('PlaceHolderSelectionRoleNewUser')}>
                                    {roles?.map(role => {
                                        return (
                                            <Option key={role.RoleId} value={role.Name}>
                                                {role.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label={t('LabelCompagnieNewUser')}
                        name='CompanyName' rules={[
                            {
                                required: true,
                                message: t('PlaceHolderAccountCompanyNameCreate')
                            }
                        ]}>
                        <Input type='text' placeholder={t('PlaceHolderCompagnieNewUser')} />
                    </Form.Item>
                    <Form.Item label={t('PlaceHolderMilitarybaseNewUser')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name='BaseId'
                                noStyle={true}
                                rules={[{ required: true, message: t('RuleSelectMilitarybaseNewUser') }]}>
                                <Select
                                    placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                    {bases?.map(base => {
                                        return (
                                            <Option key={base.BaseId} value={base.BaseId}>
                                                {base.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );

}
