import { Button, Form, Input, Modal, Select } from 'antd';
import { BaseModel, ContactModel } from 'common';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const { Option } = Select;

export default function Formulairecontact() {
    const { t } = useTranslation();

    const [contact, setContact] = useState(undefined as ContactModel | undefined);

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const [form] = Form.useForm();

    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    const onFinish = async (values: ContactModel) => {
        setContact(values);
        const va = JSON.stringify(values);
        const result = await (await fetch('/api/contact', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: va,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setContact(new ContactModel());
            form.resetFields();
            Modal.success({
                content: t('ConfirmCreateBase'),
            });

        } else {
            Modal.error({
                content: t('ConfirmCreateEchecBase'),
            });
        }
    };
    return (

        <div id='contenaireformulaire'>
            <div>
                <h1>{t('CreatNewContact')}</h1>
            </div>
            <div>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={contact} onFinish={onFinish}>
                    <Form.Item label={t('LabelLastNameContact')} name='LastName'
                        rules={[
                            {
                                required: true,
                                message: t('ValidLastNameContactCreate')
                            }
                        ]} >
                        <Input type='text' placeholder={t('PlaceholderLastNameContact')} />
                    </Form.Item>
                    <Form.Item label={t('LabelFirstnameContact')} name='Firstname'
                        rules={[
                            {
                                required: true,
                                message: t('ValidFirstNameContactCreate')
                            }
                        ]}>
                        <Input placeholder={t('PlaceholderFirstnameContact')} />
                    </Form.Item>
                    <Form.Item label={t('LabelEmailContact')} name='Email'
                        rules={[
                            {
                                required: true,
                                message: t('ValidEmailContactCreate')
                            }
                        ]}>
                        <Input placeholder={t('PlaceholderEmailContact')} />
                    </Form.Item>
                    <Form.Item label={t('LabelPhoneContact')} name='Phone_Number'
                        rules={[
                            {
                                required: true,
                                message: t('ValidPhonenumberContactCreate')
                            }
                        ]}>
                        <Input placeholder={t('PlaceholderPhoneContact')} />
                    </Form.Item>
                    <Form.Item label={t('LabelBaseMilitaireContact')}>
                        <Input.Group compact={true}>
                            <Form.Item
                                name={['BaseId']}
                                noStyle={true}
                                rules={[{ required: true, message: t('RequireMessageContact') }]}
                            >
                                <Select
                                    placeholder={t('PlaceHolderSelectMilitarybaseDocument')}>
                                    {bases?.map(base => {
                                        return (
                                            <Option key={base.BaseId} value={base.BaseId}>
                                                {base.Name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Input.Group>
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
