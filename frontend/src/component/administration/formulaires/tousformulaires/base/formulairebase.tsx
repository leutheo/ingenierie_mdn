import { Button, Form, Input, Modal } from 'antd';
import { BaseModel } from 'common';
import { default as React, useState } from 'react';
import { useTranslation } from 'react-i18next';


export default function Formulairebase() {
    const { t } = useTranslation();

    const [base, setBase] = useState(undefined as BaseModel | undefined);

    const [form] = Form.useForm();

    const onFinish = async (values: BaseModel) => {
        setBase(values);
        const va = JSON.stringify(values);
        const result = await (await fetch('/api/base', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: va,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            setBase(new BaseModel());
            form.resetFields();
            Modal.success({
                content: t('ConfirmCreateBase'),
            });

        } else {
            Modal.error({
                content: t('ConfirmCreateEchecBase'),
            });
        }
    };

    return (
        <div id='contenaireformulaire'>
            <div>
                <h1>{t('CreateMilitaryBaseTitle')}</h1>
            </div>
            <div>
                <Form form={form} name='complex-form' labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} initialValues={base} onFinish={onFinish}>
                    <Form.Item label={t('LabelNameBase')} name='Name'
                        rules={[{
                            required: true,
                            type: 'string',
                            message: t('RuleNameBase')
                        }]}>
                        <Input style={{}} placeholder={t('PlaceHolderNameBase')} />
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit'>
                            {t('Enregistrer')}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
