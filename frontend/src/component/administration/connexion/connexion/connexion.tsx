import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Divider, Form, Input } from 'antd';
import { isUserLoggedAction, setUserDetailActions } from 'common/src/redux/slices';
import { APP_URLS } from 'constants/urls';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { Link, useHistory, useLocation } from 'react-router-dom';



export default function Connexion() {
    //Création des objets
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();

    const onFinish = async (values: any) => {
        const locationState: any = location.state;
        console.log('Received values of form: ', values);
        // setuser(values);
        values.RoleId = 1;

        const params = JSON.stringify(values);


        // console.log(window.location.href);
        // console.log(location);
        // return (
        //     <Redirect to={'/admin'} />
        // );

        console.log(params);
        const result = await (await fetch('/api/user/connexion', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: params,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            // Modal.success({
            //     content: t('ConfirmCreateUser'),
            // });
            dispatch(isUserLoggedAction(true));
            const user = result[0];

            dispatch(setUserDetailActions(user));

            history.push(locationState.from, { from: APP_URLS.CONNEXION });
            console.log(result);

        } else {
            // Modal.error({
            //     content: t('ConfirmCreateEchecUser'),
            // });
            console.log('result empty');
        }
    };

    return (

        <div id='contenaireconexion'>
            <div className={'connexion'}>
                <img src='../../../img/uoi.png' alt='Logo_UOI' className={'logo'} />
                <Divider />
            </div>
            <div>
                <Form name='normal_login'
                    className='login-form'
                    labelCol={{ span: 12 }}
                    wrapperCol={{ span: 8 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}>
                    <Form.Item
                        label={t('LabelUserConnect')}
                        name='Email'
                        rules={[{ required: true, message: t('RuleUserConnect') }]}
                    >
                        <Input prefix={<UserOutlined className='site-form-item-icon' />} placeholder={t('PlaceholderUsernameConnect')} />
                    </Form.Item>
                    <Form.Item
                        label={t('LabelPasswordConnect')}
                        name='Password'
                        rules={[{ required: true, message: t('RulePasswordConnect') }]}
                    >
                        <Input
                            prefix={<LockOutlined className='site-form-item-icon' />}
                            type='password'
                            placeholder={t('PlaceholderPasswordConnect')}
                        />
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Link className='login-form-forgot' to='/reinitialisationcompte'>{t('ForgottenUsername')}</Link><br /><br />
                    </Form.Item>

                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit' className='login-form-button'>{t('LoginConnect')}</Button> Or
                        <Link to='/creationcompte'>{t('CreateAccountConnect')}</Link><br />
                    </Form.Item>
                </Form>

                <div className={'connexionpied'}>
                    <Divider />
                    <p> {t('Copyright')}</p>
                </div>
            </div>
        </div>

    );

}
