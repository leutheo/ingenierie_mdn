import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Form, Input, Modal } from 'antd';
import { ConnexionModel } from 'common/src';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

export default function Reinitialisationcompte() {
    //Création des objets
    const { t } = useTranslation();
    const history = useHistory();

    // const [connexion, setConnexion] = useState(undefined as ConnexionModel | undefined);



    const onFinish = async (values: ConnexionModel) => {
        // setConnexion(values);
        const va = JSON.stringify(values);
        const result = await fetch('/api/user/connexion', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: va,
            credentials: 'include'
        });
        //valider que result est valide et rediriger vers la page admin
        if (result.ok) {
            history.push('../admin');
        } else {
            //Prevoir un message d'erreur
            //alert(t('Validresult'));
            Modal.error({
                content: t('Validresult'),
            });
        }
    };


    return (

        <div id='contenaireconexion'>
            <div className={'connexion'}>
                <h1>{t('ConnectAccountUser')}</h1>
            </div>
            <div>
                <Form name='normal_login' className='login-form' labelCol={{ span: 12 }} wrapperCol={{ span: 8 }} initialValues={{ remember: true }} onFinish={onFinish} >
                    <Form.Item
                        label={t('LabelUserConnect')}
                        name='Email'
                        rules={[{ required: true, message: t('RuleUserConnect') }]}
                    >
                        <Input prefix={<UserOutlined className='site-form-item-icon' />} placeholder={t('PlaceholderUsernameConnect')} />
                    </Form.Item>
                    <Form.Item
                        label={t('LabelPasswordConnect')}
                        name='Password'
                        rules={[{ required: true, message: t('RulePasswordConnect') }]}
                    >
                        <Input
                            prefix={<LockOutlined className='site-form-item-icon' />}
                            type='password'
                            placeholder={t('PlaceholderPasswordConnect')}
                        />
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Link className='login-form-forgot' to='/reinitialisationcompte'>{t('ForgottenUsername')}</Link><br /><br />
                    </Form.Item>

                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit' className='login-form-button'>{t('LoginConnect')}</Button> Or
                        <Link to='/creationcompte'>{t('CreateAccountConnect')}</Link><br />
                    </Form.Item>
                </Form>
                <div className={'connexionpied'}>
                    <p> {t('Copyright')}</p>
                </div>
            </div>
        </div>
    );
}
