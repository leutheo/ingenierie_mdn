import { Button, Form, Input, Modal } from 'antd';
import { UserModel } from 'common';
import { default as React, useState } from 'react';
import { useTranslation } from 'react-i18next';



export default function Creationcompte() {
    const { t } = useTranslation();

    const [user, setuser] = useState(undefined as UserModel | undefined);

    const [form] = Form.useForm();

    const onFinish = async (values: UserModel) => {
        setuser(values);
        values.RoleId = 1;
        const va = JSON.stringify(values);
        const result = await (await fetch('/api/user', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: va,
            credentials: 'include'
        })).json();

        if (result !== undefined) {
            Modal.success({
                content: t('ConfirmCreateUser'),
            });

        } else {
            Modal.error({
                content: t('ConfirmCreateEchecUser'),
            });
        }
    };

    return (

        <div id='contenaireconexion'>
            <div className={'connexion'}>
                <h1>{t('CreateAccountUserTitle')}</h1>
            </div>
            <div>
                <Form form={form} name='complex-form' labelCol={{ span: 12 }} wrapperCol={{ span: 8 }} initialValues={user} onFinish={onFinish}>
                    <Form.Item label={t('LabelAccountUserNameCreate')} rules={[
                        {
                            required: true,
                            type: 'email',
                            message: t('ValidEmailAccountUserNameCreate')
                        }
                    ]} name='Email'>
                        <Input required={true} placeholder={t('PlaceHolderAccountUserNameCreate')} />
                    </Form.Item>
                    <Form.Item
                        label={t('LabelAccountPasswordCreate')}
                        name='Password'
                        rules={[
                            {
                                required: true,
                                message: t('MessageRequireAccountPasswordCreate'),
                                min: 8,
                            },
                        ]}
                        hasFeedback={true}
                    >
                        <Input type='password' placeholder={t('PlaceAccountPasswordCreate')} />
                    </Form.Item>
                    <Form.Item
                        label={t('LabelAccountPasswordConfirmCreate')}
                        name='confirm'
                        dependencies={['Password']}
                        hasFeedback={true}
                        rules={[
                            {
                                required: true,
                                message: t('ConfirmMessageRequireAccountPasswordCreate'),
                                min: 8
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('Password') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error(t('MatchRequireAccountPasswordCreate')));
                                },
                            }),
                        ]}
                    >
                        <Input type='password' placeholder={t('PlaceHolderAccountPasswordConfirmCreate')} />
                    </Form.Item>
                    <Form.Item label={t('LabelAccountLastNameCreate')} name='LastName' rules={[
                        {
                            required: true,
                            message: t('PlaceHolderAccountLastNameCreate')
                        }
                    ]}>
                        <Input type='text' placeholder={t('PlaceHolderAccountLastNameCreate')} />
                    </Form.Item>
                    <Form.Item label={t('LabelAccountFirstNameCreate')} name='FirstName' rules={[
                        {
                            required: true,
                            message: t('PlaceHolderAccountFirstNameCreate')
                        }
                    ]}>
                        <Input type='text' placeholder={t('PlaceHolderAccountFirstNameCreate')} />
                    </Form.Item>
                    <Form.Item label={t('LabelAccountCompanyNameCreate')} name='CompanyName' rules={[
                        {
                            required: true,
                            message: t('PlaceHolderAccountCompanyNameCreate')
                        }
                    ]}
                    >
                        <Input type='text' placeholder={t('PlaceHolderAccountCompanyNameCreate')} />
                    </Form.Item>
                    <Form.Item label=' ' colon={false}>
                        <Button type='primary' htmlType='submit' >
                            {t('SubmitrequestCreate')}
                        </Button>
                    </Form.Item>
                </Form>

                <div className={'connexionpied'}>
                    <p>{t('Copyright')}</p>
                </div>
            </div>
        </div>
    );

}
