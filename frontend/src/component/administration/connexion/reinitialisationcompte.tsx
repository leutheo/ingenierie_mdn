import { Button, Form, Input } from 'antd';
import { APP_URLS } from 'constants/urls';
import React from 'react';
import { Link } from 'react-router-dom';


export class Reinitialisationcompte extends React.Component {
    public render() {
        return (
            <div id='contenaireconexion'>
                <div className={'connexion'}>
                    <h1>Demandez vos identifiants</h1>
                </div>
                <div>
                    <Form name='complex-form' labelCol={{ span: 12 }} wrapperCol={{ span: 8 }}>
                        <Form.Item label='Entrez votre nom d’utilisateur ou votre e-mail'>
                            <Input placeholder='Nom d’utilisateur' />
                        </Form.Item>
                        <Form.Item label=' ' colon={false}>
                            <Button type='primary' htmlType='submit'>
                                Renvoyer
                                </Button>
                        </Form.Item >
                        <Form.Item label=' ' className={'connexionpied'} colon={false}>
                            <Link to={APP_URLS.CONNEXION}>Allez vous connecter</Link><br /><br />
                        </Form.Item>
                    </Form>
                    <div className={'connexionpied'}>
                        <p> ©  2021, Unité des Opérations Immobilières, Québec </p>
                    </div>
                </div>
            </div>
        );
    };
}
