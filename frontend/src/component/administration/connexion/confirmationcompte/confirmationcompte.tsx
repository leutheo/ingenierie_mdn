import { Button, Checkbox, Form, Input, Select } from 'antd';
import React from 'react';

const { Option } = Select;

function onChange(e: { target: { checked: any; }; }) {
    console.log(`checked = ${e.target.checked}`);
}

export class Confirmationcompte extends React.Component {
    public render() {
        return (
            <>
                <div id='contenaireconexion'>
                    <div className={'connexion'}>
                        <h1>Confirmer un compte utilisateur</h1>
                    </div>
                    <div>
                        <Form name='complex-form' labelCol={{ span: 12 }} wrapperCol={{ span: 8 }}>
                            <Form.Item label='Entrez votre nom d’utilisateur ou votre e-mail'>
                                <Input placeholder='Nom d’utilisateur' />
                            </Form.Item>
                            <Form.Item label='Entrez votre mot de passe'>
                                <Input type='password' placeholder='Mot de passe' />
                            </Form.Item>
                            <Form.Item label='Confirmez votre mot de passe'>
                                <Input type='password' placeholder='Confirmez mot de passe' />
                            </Form.Item>
                            <Form.Item label='Nom et prénom'>
                                <Input placeholder='Nom et prénom' />
                            </Form.Item>
                            <Form.Item label='Nom de la compagnie'>
                                <Input placeholder='Nom de la compagnie' />
                            </Form.Item>
                            <Form.Item label='Rôle'>
                                <Input.Group compact={true}>
                                    <Form.Item
                                        name={['basemilitaire']}
                                        noStyle={true}
                                        rules={[{ required: true, message: 'Veuillez selectionner un rôle' }]}
                                    >
                                        <Select placeholder='Selectionnez une base militaire'>
                                            <Option value='Employé'>Employé</Option>
                                            <Option value='Expert-conseil'>Expert-conseil</Option>
                                            <Option value='Administrateur'>Administrateur</Option>
                                        </Select>
                                    </Form.Item>
                                </Input.Group>
                            </Form.Item>
                            <Form.Item label='statut'>
                                <Checkbox onChange={onChange}>Actif</Checkbox>
                            </Form.Item>
                            <Form.Item label=' ' colon={false}>
                                <Button type='primary' htmlType='submit'>
                                    Connexion
                                </Button>
                            </Form.Item>
                            <div className={'connexionpied'}>
                                <a href='#'>Créer un compte</a><br /><br />
                                <a href='#'>Nom d’utilisateur ou mot de passe oublié</a>
                            </div>
                        </Form>
                        <div className={'connexionpied'}>
                            <p> ©  2021, Unité des Opérations Immobilières, Québec </p>
                        </div>
                    </div>
                </div>
            </>
        );
    };
}
