import { Layout } from 'antd';
import { Content, Footer, Header } from 'antd/lib/layout/layout';
import { MenuInfo } from 'rc-menu/lib/interface';
import React, { useState } from 'react';
import Footer2 from './bases/frooter2';
import Menufooter from './bases/menufooter';
import Menuheader from './bases/menuheader';
import DocumentPage from './pages/documentpage';
import FormulairePage from './pages/formulairepage';
import HomePage from './pages/homepage';
import NousJoindrePage from './pages/nousjoindrepage';
import SalleTechniquePage from './pages/salletechniquepage';
import SectionDeDevisPage from './pages/sectiondedevispage';

export default function Accueil() {
    const [domContent, setdomContent] = useState('homepage');

    function changeContent(value: MenuInfo) {
        const text = value.key;
        setdomContent(text.toString());
    }

    function renderSwitch(param: string) {
        let values = param.split(':');
        let page = values[0];
        let _baseId = values[1];
        //This is available for sectiondevis
        // -1 means no group selected
        let groupId = "-1";
        if (values.length > 2) {
            groupId = values[2];
        }

        switch (page) {
            case 'homepage': return <HomePage />;
            case 'documents': return <DocumentPage baseId={parseInt(_baseId)} />;
            case 'sectiondedevis': return <SectionDeDevisPage baseId={parseInt(_baseId)} groupId={parseInt(groupId)} />;
            case 'formulaires': return <FormulairePage />;
            case 'salletechnique': return <SalleTechniquePage baseId={parseInt(_baseId)} />;
            case 'nousjoindre': return <NousJoindrePage />;
            default: return <h1>No Component match</h1>;
        }
    }

    return (
        <div id='contenaire'>
            <Layout>
                <Header className={'header-contianer'}>
                    <Menuheader onclick={changeContent} />
                </Header>
                <Content>
                    <div className="body-content">
                        {renderSwitch(domContent)}
                    </div>
                    <Footer2 />
                </Content>
                <Footer id={'footer'}>
                    <Menufooter />
                </Footer>
            </Layout>
        </div>
    );
}
