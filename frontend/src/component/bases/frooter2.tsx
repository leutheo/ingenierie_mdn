import { Anchor, Col, Image, Row } from 'antd';
import React from 'react';


export default function Footer2() {
    const { Link } = Anchor;
    return (
        <Row className={'footer2page p-3'}>
            <Col span={6} className={'footerlocal'}>
                {newFunction()}
            </Col>
            <Col span={18} className={'p-0'} >
                <Anchor className={'quick-link-anchor m-0 p-0'}>

                    <Row className="p-2">
                        <Col span="24">
                            <h6>Liens utiles</h6>
                        </Col>
                        <Col span="6">
                            <Link href="#components-anchor-demo-basic" title="Construction Défense Canada (CDC)" />
                            <Link href="#components-anchor-demo-static" title="Codes Nationaux du Bâtiment" />
                        </Col>
                        <Col span="6">
                            <Link href="#Anchor-Props" title="Défense Nationale" />
                            <Link href="#Link-Props" title="Normes CAO/BIM du MDN" />
                        </Col>
                    </Row>
                </Anchor>
            </Col>
        </Row>

    );

    function newFunction() {
        return (
            <Image src='../../../img/logo-canada.png' alt='canada-logo_noir' className={'footer-logo'} />
        );
    }
}
