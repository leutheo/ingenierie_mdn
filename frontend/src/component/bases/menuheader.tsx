import { DownOutlined, LogoutOutlined } from '@ant-design/icons';
import { Col, Divider, Dropdown, Image, Menu, Modal, Row } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import { logOutAction } from 'common/src/redux/slices';
import { APP_URLS } from 'constants/urls';
import { MenuInfo } from 'rc-menu/lib/interface';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { BaseModel, SectionDevisGroupeModel } from '../../../../common';
import { GlobalReducer } from '../../../../common/src/redux/type';

type MenugaucheProps = {
    onclick: (menu: MenuInfo) => void;
};

function Menuheader(props: MenugaucheProps) {

    const dispatch = useDispatch();
    const history = useHistory();
    const isLoggedIn = useSelector((state: GlobalReducer) => state.stateReducer.isLoggedIn);
    const userContext = useSelector((state: GlobalReducer) => state.stateReducer.userContext);
    console.log(isLoggedIn);

    const { t } = useTranslation();

    const [groups, setGroups] = useState(undefined as SectionDevisGroupeModel[] | undefined);

    const [bases, setBases] = useState(undefined as BaseModel[] | undefined);

    const onFinish = () => {
        dispatch(logOutAction());
        history.push('/');
    };

    const menu = (
        <Menu>
            <Menu.Divider />
            <Menu.Item key='2'>
                <span onClick={onFinish}>
                    <Row>
                        <Col className='text-center' span={1}>
                            <LogoutOutlined />
                        </Col>
                        <Col>
                            <Divider type='vertical' />
                        </Col>
                        <Col className='text-center' span={6}>
                            Deconnection
                        </Col>
                    </Row>
                </span>
            </Menu.Item>
        </Menu>
    );

    //obtension de la liste des bases
    useEffect(() => {
        GetListBases();
        GetListGroup();
    }, []);

    const GetListBases = async () => {
        const result = await fetch('/api/base', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(BaseModel.fromJSON);
            setBases(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };

    const GetListGroup = async () => {
        const result = await fetch('/api/sectiondevisgroupe', {
            method: 'GET',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        //valider que result n'est pas null
        if (result.ok) {
            const values = (await result.json() as any[]).map(SectionDevisGroupeModel.fromJSON);
            setGroups(values);
        } else {
            //alert(t('Validresult'));
            Modal.warning({
                title: t('WarningMessage'),
                content: t('BaseListsEmpty'),
            });
        }
    };
    const listItem = bases?.map(base => ({
        baseid: base.BaseId, label: base.Name
    }));

    const groupItem = groups?.map(group => ({
        groupid: group.Section_Devis_GroupeId, label: group.Name
    }));

    const handleClick = (e: any) => {
        console.log(e);
        let menuInfo: MenuInfo = e;

        props.onclick(menuInfo);
    };


    return (
        <Row>
            <Col span={4}>
                {newFunction()}
            </Col>
            <Col span={16} >
                <Menu onClick={handleClick} mode='horizontal'>
                    <Menu.Item key={'homepage'} className={"top-menu-item"}>HOME</Menu.Item>
                    {listItem?.map(({ label, baseid }) => (
                        <SubMenu title={label.toUpperCase()} className={"top-menu-ul-item"} key={"menuheadersubmenu" + baseid}>
                            <Menu.Item key={'documents:' + baseid} className={"top-menu-item"}>DOCUMENTS</Menu.Item>

                            <SubMenu title="SECTION DE DEVIS" key={'sectiondedevis:' + baseid} className={"top-menu-item"}>
                                {groupItem?.map(({ label, groupid }) => (
                                    <Menu.Item key={'sectiondedevis:' + baseid + ':' + groupid}>{label}</Menu.Item>
                                ))}
                                {/* <Menu.Item key={'Sections_generales_devis:'}>SECTIONS GÉNÉRALE DU DEVIS</Menu.Item>
                                <Menu.Item key={'Sections_architecture_devis:'}>SECTIONS ARCHITECTURE DU DEVIS</Menu.Item>
                                <Menu.Item key={'Sections_mecanique_devis:'}>SECTIONS MÉCANIQUE DU DEVIS</Menu.Item>
                                <Menu.Item key={'Sections_controle_devis:'}>SECTIONS CONTRÔLE DU DEVIS</Menu.Item>
                                <Menu.Item key={'Sections_speciales_devis:'}>SECTIONS SPÉCIALES DU DEVIS</Menu.Item>
                                <Menu.Item key={'Sections_electricite_devis:'}>SECTIONS ÉLECTRIICITÉ DU DEVIS</Menu.Item> */}
                            </SubMenu>
                            {/* <Menu.Item key={'sectiondedevis:' + id} className={"top-menu-item"}>
                                <SubMenu>
                                    <Menu.Item key={'Sections_generales_devis:'}>SECTIONS GÉNÉRALE DU DEVIS</Menu.Item>
                                    <Menu.Item key={'Sections_architecture_devis:'}>SECTIONS ARCHITECTURE DU DEVIS</Menu.Item>
                                    <Menu.Item key={'Sections_mecanique_devis:'}>SECTIONS MÉCANIQUE DU DEVIS</Menu.Item>
                                    <Menu.Item key={'Sections_controle_devis:'}>SECTIONS CONTRÔLE DU DEVIS</Menu.Item>
                                    <Menu.Item key={'Sections_speciales_devis:'}>SECTIONS SPÉCIALES DU DEVIS</Menu.Item>
                                    <Menu.Item key={'Sections_electricite_devis:'}>SECTIONS ÉLECTRIICITÉ DU DEVIS</Menu.Item>
                                </SubMenu>
                            </Menu.Item> */}
                            <Menu.Item key={'formulaires:' + baseid} className={"top-menu-item"}>FORMULAIRES</Menu.Item>
                            <Menu.Item key={'salletechnique:' + baseid} className={"top-menu-item"}>SALLE TECHNIQUE</Menu.Item>
                            <Menu.Item key={'nousjoindre:' + baseid} className={"top-menu-item"}>NOUS JOINDRE</Menu.Item>
                        </SubMenu>
                    ))}
                </Menu>
            </Col>

            <Col span={4}>

                <div className={isLoggedIn ? 'd-block login-action' : 'd-none'}>
                    <Dropdown overlay={menu} overlayClassName={'profile-dropdown-nemu'}>
                        <a className='ant-dropdown-link' onClick={e => e.preventDefault()}>
                            {userContext.user?.FirstName + ' ' + userContext.user?.LastName}<DownOutlined />
                        </a>
                    </Dropdown>
                </div>
                <div className={isLoggedIn ? 'd-none' : 'd-block login-action'}>
                    <Link to={{ pathname: APP_URLS.CONNEXION, state: { from: APP_URLS.BASE } }}>Connexion</Link>
                </div>

            </Col>

        </Row >
    );

    function newFunction() {
        return (
            <Image
                src='../../../img/uoi.png' alt='Logo_UOI' className={'header-logo'}
            />
        );
    }


}

export default Menuheader;
