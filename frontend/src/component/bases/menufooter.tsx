import LogoutOutlined from '@ant-design/icons/lib/icons/LogoutOutlined';
import React from 'react';



export default function Menufooter() {
    return (
        <div className={'footerpage'}>
            <div className={'footerlocal'}>
                <p>Ministère de la Défense Nationale</p>
                <a href='https://www.canada.ca/fr/ministere-defense-nationale.html' target='_blank'>
                    <LogoutOutlined />
                </a>

            </div>
            <div >
                <p>© 2021, Unité des Opérations Immobilières, Québec</p>
            </div>
        </div>
    );
}
