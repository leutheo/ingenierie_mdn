import { Carousel } from 'antd';
import React from 'react';
import imgComplexValcartier from '../../../img/complex-valcartier.jpg';
import doordash from '../../../img/slide1.jpg';
import valcartier from '../../../img/valcartier.jpg';
export default function HomeCarousel() {

    return (
        <>
            <Carousel autoplay effect="fade" className="carousel-container">
                <div>
                    <img src={imgComplexValcartier} alt='imgComplexValcartier' className="carousel-slide" />
                </div>
                <div>
                    <img src={doordash} alt='doordash' className="carousel-slide" />
                </div>
                <div>
                    <img src={valcartier} alt='valcartier' className="carousel-slide" />
                </div>
            </Carousel >
            <div className='titre-base'>
                <h1 className='majuscule'>bienvenue sur le site web de l'unité des Opérations Immobilières</h1>
            </div>
        </>
    );
}
