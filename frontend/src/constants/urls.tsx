export const APP_URLS = {
    CONNEXION: '/connexion',
    CREATIONCOMPTE: '/creationcompte',
    CONFIRMATIONCOMPTE: '/confirmationcompte',
    REINITIALISECOMPTE: '/reinitialisationcompte',
    ETAPEONE: '/etape1',
    ETAPETWO: '/etape2',
    ETAPETHREE: '/etape3',
    ETAPETHREEA: '/etape3a',
    ETAPETHREEB: '/etape3b',
    ETAPEONETHREEC: '/etape3c',
    ADMIN: '/admin',
    DOCUMENT: '/document',
    HALOCARBURES: '/halocarbures',
    BASE: '/'
};
