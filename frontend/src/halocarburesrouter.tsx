import Admin from 'component/administration/admin/admin';
import { Confirmationcompte } from 'component/administration/connexion/confirmationcompte/confirmationcompte';
import Connexion from 'component/administration/connexion/connexion/connexion';
import Creationcompte from 'component/administration/connexion/creationcompte/creationcompte';
import Reinitialisationcompte from 'component/administration/connexion/reinitialisationcompte/reinitialisationcompte';
import Etape1 from 'component/administration/halocarbures/etape1';
import Etape2 from 'component/administration/halocarbures/etape2';
import Etape3 from 'component/administration/halocarbures/etape3';
import Etape3A from 'component/administration/halocarbures/etape3a';
import Etape3B from 'component/administration/halocarbures/etape3b';
import Etape3C from 'component/administration/halocarbures/etape3c';
import { APP_URLS } from 'constants/urls';
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Accueil from './component/accueil';
import Document from './component/bases/bagotville/document';
import Halocarbures from './component/halocarbures';


export class HalocarburesRouter extends React.Component<{}>{
    public render() {

        return <BrowserRouter>
            <Switch>
                <Route exact={true} path={APP_URLS.CONNEXION} component={Connexion} />
                <Route exact={true} path={APP_URLS.CREATIONCOMPTE} component={Creationcompte} />
                <Route exact={true} path={APP_URLS.CONFIRMATIONCOMPTE} component={Confirmationcompte} />
                <Route exact={true} path={APP_URLS.REINITIALISECOMPTE} component={Reinitialisationcompte} />
                <Route exact={true} path={APP_URLS.ETAPEONE} component={Etape1} />
                <Route exact={true} path={APP_URLS.ETAPETWO} component={Etape2} />
                <Route exact={true} path={APP_URLS.ETAPETHREE} component={Etape3} />
                <Route exact={true} path={APP_URLS.ETAPETHREEA} component={Etape3A} />
                <Route exact={true} path={APP_URLS.ETAPETHREEB} component={Etape3B} />
                <Route exact={true} path={APP_URLS.ETAPEONETHREEC} component={Etape3C} />
                <Route exact={true} path={APP_URLS.ADMIN} component={Admin} />
                <Route exact={true} path={APP_URLS.DOCUMENT} component={Document} />
                <Route exact={true} path={APP_URLS.HALOCARBURES} component={Halocarbures} />
                <Route exact={true} path={APP_URLS.BASE} component={Accueil} />
            </Switch>
        </BrowserRouter>;
    }
}
