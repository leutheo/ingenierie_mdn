import 'bootstrap/dist/css/bootstrap.min.css';
import store from 'common/src/redux/configureStore';
import { HalocarburesRouter } from 'halocarburesrouter';
import 'main.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { persistStore } from 'reduxjs-toolkit-persist';
import { PersistGate } from 'reduxjs-toolkit-persist/integration/react';
import './i18n';

let persistor = persistStore(store);

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={<div>Loading .....</div>} persistor={persistor}>
            <HalocarburesRouter />
        </PersistGate>
    </Provider>,
    document.getElementById('ingenierieMdn'));
