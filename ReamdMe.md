How to run
----------
0. package installatiomn
To install all needed package accross all modules(backend, frontend, common), you have to:
$ cd     in the root folder
$ yarn install

1. Build modules
Build module is required to get this project to run as expeted. 
Once you build, all needed folders such as model, enum, index.js.tsx, etc will be generated under dist forlder for each modules.
To build, you need to run the build command for each module following the bellow order:
- common
$ cd ./common
$  yarn build
- backend
$ cd ./backend
$ rename ./backend/src/config.ts.template to ./backend/src/config.ts
$ yarn build
- frontend
$ cd ./frontend
$ yarn build
if you face error, related to webpack, run following command
$ cd ./frontend
$ npm install -g webpack
$ npm i -g webpack-cli

2. to run all module at once
This command will run both frontend and backend at once
$ npm run-script localSetup

If for some raison, you need to run modules one by one, we just need to processed as following:
2. run in debug mode
- frontend
$ yarn serve
- backend
$ yarn debug


3. run in dev env
- frontend
$ yarn serve
- backend
$ yarn serve

4. run in prod env
TODO: complete this 

5. arrêter serveur 

Recherche des fonctionnalités en cours d'exécution
- netstat -ano | findstr 1280
- lsof -Pi:1280

Arrêt de la fonctionalité en cours d'exécution
- TASKKILL /PID 8464 /F
- kill -9 PID

6. supprimer fichier ou dossier
- rm -rf node-modules
- rm -rf backend/node-modules
- rm -rf ../backend/dist/*
- MacBook-Pro-de-LEUWAT:backend leuwatngogangtheodorebienvenu$ rm -rf ../common/dist/* && rm -rf ../backend/dist/* && cd ../common/ && yarn build && cd ../backend/ && yarn build

7. Fix mysql engine date restriction
Some mysql engine have restiction on data format. To fix by removing those restriction, run the following commande
SELECT @@GLOBAL.sql_mode global, @@SESSION.sql_mode session;
SET sql_mode = '';
SET GLOBAL sql_mode = '';

8. Pour arrêter processus sur Mac
- rechercher le processus en cours 
    Lsof -Pi:8080

- arrêter le processus
    Kill -9 PID
