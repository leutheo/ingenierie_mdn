import { UserModel } from "common";

declare global {
    module Express {
        interface Request {
            user: UserModel;
        }
    }
}
