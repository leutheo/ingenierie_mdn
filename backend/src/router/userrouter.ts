import { Router } from 'express';
import nodemailer from 'nodemailer';
import { ConnexionModel, UserModel } from '../../../common';
import { UserDAO } from '../dao/userdao';
import { wrap } from '../util';

const userRouter = Router();
const userDAO = new UserDAO;


userRouter.post('/', wrap(async (req, res) => {
    const user = UserModel.fromJSON(req.body);
    const userId = await userDAO.createUser(user);
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'garneaubienvenu@gmail.com',
            pass: 'monaec@2019'
        }
    });
    const mailOptions = {
        from: 'garneaubienvenu@gmail.com',
        to: 'leutheo@yahoo.fr',
        subject: 'Sending Email using Node.js',
        text: 'Un utilisateur est en attente de votre approbation pour la création de son compte'
    };
    transporter.sendMail(mailOptions, function (error: any, info: any) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
    return res.send(await userDAO.getUser(userId));
}));

userRouter.get('/', wrap(async (_req, res) => {

    const users = await userDAO.getUsers();
    return res.send(users);
}));

userRouter.get('/:userId', wrap(async (req, res) => {
    return res.send(req.user);
}));

userRouter.get('/:baseId', wrap(async (req, res) => {
    return res.send(req.user);
}));

userRouter.put('/', wrap(async (req, res) => {
    const updated: UserModel = req.body;
    await userDAO.updateUser(updated);

    return res.send(await userDAO.getUser(updated.UserId));
}));
userRouter.delete('/:userId', wrap(async (req, res) => {
    const userId = req.params.userId;
    await userDAO.deleteUser(parseInt(userId, 10));
    return res.sendStatus(204);
}));

userRouter.post('/connexion', wrap(async (req, res) => {
    console.log(req.body);
    const model = ConnexionModel.fromJSON(req.body);
    const user = await userDAO.ValidateUserConexion(model.Email, model.Password);
    return res.send(user);
}));
export { userRouter };
