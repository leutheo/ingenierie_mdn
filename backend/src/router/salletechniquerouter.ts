import { Router } from 'express';
import { createWriteStream } from 'fs';
import multer from 'multer';
import { CONSTANT, DocumentTechniqueModel, LinksalleTechniqueModel, SalleTechniqueModel } from '../../../common';
import { DocumentSalleTechniqueDao } from '../dao/documentsalleTechniquedao';
import { LinkSalleTechniqueDao } from '../dao/linksalleTechniquedao';
import { SalleTechniqueDAO } from '../dao/salletechniquedao';
import { wrap } from '../util';

const salletechniqueRouter = Router();
const salletechniqueDAO = new SalleTechniqueDAO;
const docDao = new DocumentSalleTechniqueDao;
const linkDao = new LinkSalleTechniqueDao;
const upload = multer();

salletechniqueRouter.post('/', upload.any(), wrap(async (req, res) => {
    let datajson: any = JSON.parse(req.body.document);
    let model: any = {
        documentTitre: datajson.documentTitre,
        linkTitre: datajson.linkTitre,
        BaseId: datajson.BaseId,
        DocDestinationPath: datajson.DocDestinationPath,
        linkDocDestinationPath: datajson.linkDocDestinationPath
    };
    let salletechnique = SalleTechniqueModel.fromJSON(model);
    let linkModel = LinksalleTechniqueModel.fromJSON(model);
    let docModel = DocumentTechniqueModel.fromJSON(model);

    linkModel.Name = model.linkTitre;
    linkModel.DocDestinationPath = model.linkDocDestinationPath;
    docModel.Name = model.documentTitre;
    docModel.DocDestinationPath = model.DocDestinationPath;

    console.log(linkModel);
    console.log("docModel");
    console.log(docModel);

    let linkId = await linkDao.create(linkModel);
    let docId = await docDao.create(docModel);

    salletechnique.LinkId = linkId;
    salletechnique.Document_salle_techniqueId = docId;
    // salletechnique.BaseId = 
    const salletechniqueId = await salletechniqueDAO.createSalle_Technique(salletechnique);

    console.log(req.files);
    //create file
    let _files: any = req.files;
    console.log(_files);

    let writeFileStream = createWriteStream(CONSTANT.DOC_PATH_DESTINATION + '/' + linkModel.DocDestinationPath);
    writeFileStream.write(_files[0].buffer, 'base64');

    //let docFile: any = req.file[1];
    writeFileStream = createWriteStream(CONSTANT.DOC_PATH_DESTINATION + '/' + docModel.DocDestinationPath);
    writeFileStream.write(_files[1].buffer, 'base64');


    writeFileStream.on('close', () => {
        console.log('FileStream was closed!!!');
    });

    return res.send(await salletechniqueDAO.getSalle_Technique(salletechniqueId));

}));

salletechniqueRouter.get('/', wrap(async (_req, res) => {
    const salletechniques = await salletechniqueDAO.getSalle_Techniques();
    return res.send(salletechniques);
}));

salletechniqueRouter.post('/add', wrap(async (req, res) => {
    return res.send(req.params);

}));

salletechniqueRouter.get('/:salletechniqueId', wrap(async (req, res) => {
    const salletechniqueId = req.params.salletechniqueId;
    return res.send((await salletechniqueDAO.getSalle_Technique(salletechniqueId)));
}));

salletechniqueRouter.get('/base/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    let salleTechniques: any = await salletechniqueDAO.getSalle_TechniqueBaseId(baseId);
    if (salleTechniques == undefined || salleTechniques == null) {
        return salleTechniques;
    }
    for (const _salleTechnique of salleTechniques) {
        let link = await linkDao.getLink(_salleTechnique.LinkId);
        _salleTechnique.linkSalleTechniques.push(link);
        let document = await docDao.getDocumentSalleTechnique(_salleTechnique.Document_salle_techniqueId);
        _salleTechnique.documentSalleTechniques.push(document);
    }


    return res.send(salleTechniques);
}));

salletechniqueRouter.put('/', wrap(async (req, res) => {
    const updated: SalleTechniqueModel = req.body;
    await salletechniqueDAO.updateSalle_Technique(updated);

    return res.send(await salletechniqueDAO.getSalle_Technique(updated.Salle_TechniqueId));
}));

salletechniqueRouter.delete('/:salletechniqueId', wrap(async (req, res) => {
    const salletechniqueId = req.params.salletechniqueId;
    await salletechniqueDAO.deleteSalle_Technique(parseInt(salletechniqueId, 10));
    return res.sendStatus(204);
}));

export { salletechniqueRouter };
