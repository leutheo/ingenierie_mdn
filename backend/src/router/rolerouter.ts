import { RoleModel } from 'common';
import { Router } from 'express';
import { RoleDAO } from '../dao/roledao';
import { wrap } from '../util';

const roleRouter = Router();
const roleDAO = new RoleDAO;

roleRouter.post('/', wrap(async (req, res) => {
    const role = RoleModel.fromJSON(req.body);
    const roleId = await roleDAO.createRole(role);
    return res.send(await roleDAO.getRole(roleId));
}));

roleRouter.get('/', wrap(async (_req, res) => {
    const roles = await roleDAO.getRoles();
    return res.send(roles);
}));

roleRouter.get('/:roleId', wrap(async (req, res) => {
    const roleId = req.params.roleId;
    return res.send((await roleDAO.getRole(roleId)));
}));

roleRouter.put('/', wrap(async (req, res) => {
    const updated: RoleModel = req.body;
    await roleDAO.updateRole(updated);
    return res.send(await roleDAO.getRole(updated.RoleId));
}));

roleRouter.delete('/:roleId', wrap(async (req, res) => {
    const roleId = req.params.roleId;
    await roleDAO.deleteRole(parseInt(roleId, 10));
    return res.sendStatus(204);
}));

export { roleRouter };
