import { CompanyModel } from 'common';
import { Router } from 'express';
import { CompanyDAO } from '../dao/companydao';
import { wrap } from '../util';

const companyRouter = Router();
const companyDAO = new CompanyDAO;

companyRouter.post('/', wrap(async (req, res) => {
    const company = CompanyModel.fromJSON(req.body);
    const companyId = await companyDAO.createCompany(company);
    return res.send(await companyDAO.getCompany(companyId));
}));

companyRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('company');
}));

companyRouter.get('/', wrap(async (_req, res) => {
    const companys = await companyDAO.getCompanys();
    return res.send(companys);
}));

companyRouter.get('/:companyId', wrap(async (req, res) => {
    const companyId = req.params.companyId;
    return res.send((await companyDAO.getCompany(companyId)));
}));

companyRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await companyDAO.getCompany(baseId)));
}));

companyRouter.put('/', wrap(async (req, res) => {
    const updated: CompanyModel = req.body;
    await companyDAO.updateCompany(updated);

    return res.send(await companyDAO.getCompany(updated.CompanyId));
}));

companyRouter.delete('/:companyId', wrap(async (req, res) => {
    const companyId = req.params.FormstepId;
    await companyDAO.deleteCompany(parseInt(companyId, 30));
    return res.sendStatus(304);
}));


export { companyRouter };
