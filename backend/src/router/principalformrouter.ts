import { Principal_formModel } from 'common';
import { Router } from 'express';
import { Principal_formDAO } from '../dao/principalformdao';
import { wrap } from '../util';

const principal_formRouter = Router();
const principal_formDAO = new Principal_formDAO;

principal_formRouter.post('/', wrap(async (req, res) => {
    const principal_form = Principal_formModel.fromJSON(req.body);
    const principal_formId = await principal_formDAO.createPrincipal_form(principal_form);
    return res.send(await principal_formDAO.getPrincipal_form(principal_formId));

}));

principal_formRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('etape 1');
}));

principal_formRouter.get('/', wrap(async (_req, res) => {
    const principal_forms = await principal_formDAO.getPrincipal_forms();
    return res.send(principal_forms);
}));

principal_formRouter.get('/:principal_formId', wrap(async (req, res) => {
    const principal_formId = req.params.principal_formId;
    return res.send((await principal_formDAO.getPrincipal_form(principal_formId)));
}));

principal_formRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await principal_formDAO.getPrincipal_form(baseId)));
}));

principal_formRouter.put('/', wrap(async (req, res) => {
    const updated: Principal_formModel = req.body;
    await principal_formDAO.updatePrincipal_form(updated);

    return res.send(await principal_formDAO.getPrincipal_form(updated.Principal_formId));
}));

principal_formRouter.delete('/:principal_formId', wrap(async (req, res) => {
    const principal_formId = req.params.Principal_formId;
    await principal_formDAO.deletePrincipal_form(parseInt(principal_formId, 10));
    return res.sendStatus(204);
}));

export { principal_formRouter };
