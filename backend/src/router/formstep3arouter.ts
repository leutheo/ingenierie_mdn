import { Formstep3AModel, StepModel } from 'common';
import { Router } from 'express';
import { StepDAO } from '../dao/etapesdao';
import { Formstep3aDAO } from '../dao/formstep3adao';
import { wrap } from '../util';

const formstepThreeaRouter = Router();
const formstep3aDAO = new Formstep3aDAO;
const stepDAO = new StepDAO;

formstepThreeaRouter.post('/', wrap(async (req, res) => {

    const principal_formId = req.body.Principal_formId;
    console.log("principal_formId");
    console.log(principal_formId);

    const formstep3a = Formstep3AModel.fromJSON(req.body);
    const formstep3aId = await formstep3aDAO.createFormstep3a(formstep3a);

    let modelStep: any = {
        Principal_formId: principal_formId,
        Form_StepId: formstep3aId,
        Form_Step_IsValid: false,
        Form_step_typeId: 4
    };

    const stepModel = StepModel.fromJSON(modelStep);
    let stepModelId = await stepDAO.createStep(stepModel);
    console.log(stepModelId);
    return res.send(await formstep3aDAO.getFormstep3a(formstep3aId));
}));

formstepThreeaRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('etape3a');
}));

formstepThreeaRouter.get('/', wrap(async (_req, res) => {
    const formstep3as = await formstep3aDAO.getFormstep3as();
    return res.send(formstep3as);
}));

formstepThreeaRouter.get('/:formstep3aId', wrap(async (req, res) => {
    const formstep3aId = req.params.formstep3aId;
    return res.send((await formstep3aDAO.getFormstep3a(formstep3aId)));
}));

formstepThreeaRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await formstep3aDAO.getFormstep3a(baseId)));
}));

formstepThreeaRouter.put('/', wrap(async (req, res) => {
    const updated: Formstep3AModel = req.body;
    await formstep3aDAO.updateFormstep3a(updated);

    return res.send(await formstep3aDAO.getFormstep3a(updated.Form_step3AId));
}));

formstepThreeaRouter.delete('/:formstep3aId', wrap(async (req, res) => {
    const formstep3aId = req.params.FormstepId;
    await formstep3aDAO.deleteFormstep3a(parseInt(formstep3aId, 30));
    return res.sendStatus(304);
}));

export { formstepThreeaRouter };
