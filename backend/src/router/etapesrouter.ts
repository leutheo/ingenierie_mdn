import { StepModel } from 'common';
import { Router } from 'express';
import { StepDAO } from '../dao/etapesdao';
import { wrap } from '../util';

const stepRouter = Router();
const stepDAO = new StepDAO;

stepRouter.post('/', wrap(async (req, res) => {
    const step = StepModel.fromJSON(req.body);
    const stepId = await stepDAO.createStep(step);
    return res.send(await stepDAO.getStep(stepId));

}));

stepRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('etapes');
}));

stepRouter.get('/', wrap(async (_req, res) => {
    const steps = await stepDAO.getSteps();
    return res.send(steps);
}));

stepRouter.get('/:stepId', wrap(async (req, res) => {
    const stepId = req.params.stepId;
    return res.send((await stepDAO.getStep(stepId)));
}));

stepRouter.put('/', wrap(async (req, res) => {
    const updated: StepModel = req.body;
    await stepDAO.updateStep(updated);

    return res.send(await stepDAO.getStep(updated.StepId));
}));

stepRouter.delete('/:stepId', wrap(async (req, res) => {
    const stepId = req.params.stepId;
    await stepDAO.deleteStep(parseInt(stepId, 10));
    return res.sendStatus(204);
}));

export { stepRouter };
