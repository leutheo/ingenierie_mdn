import { Formstep3BModel, StepModel } from 'common';
import { Router } from 'express';
import { StepDAO } from '../dao/etapesdao';
import { Formstep3BDAO } from '../dao/formstep3bdao';
import { wrap } from '../util';

const formstepThreebRouter = Router();
const formstep3bDAO = new Formstep3BDAO;
const stepDAO = new StepDAO;

formstepThreebRouter.post('/', wrap(async (req, res) => {

    const principal_formId = req.body.Principal_formId;
    console.log("principal_formId");
    console.log(principal_formId);

    const formstep3b = Formstep3BModel.fromJSON(req.body);
    const formstep3bId = await formstep3bDAO.createFormstep3b(formstep3b);

    let modelStep: any = {
        Principal_formId: principal_formId,
        Form_StepId: formstep3bId,
        Form_Step_IsValid: false,
        Form_step_typeId: 5
    };
    const stepModel = StepModel.fromJSON(modelStep);
    let stepModelId = await stepDAO.createStep(stepModel);
    console.log(stepModelId);
    return res.send(await formstep3bDAO.getFormstep3b(formstep3bId));
}));

formstepThreebRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('etape3b');
}));

formstepThreebRouter.get('/', wrap(async (_req, res) => {
    const formstep3bs = await formstep3bDAO.getFormstep3bs();
    return res.send(formstep3bs);
}));

formstepThreebRouter.get('/:formstep3bId', wrap(async (req, res) => {
    const formstep3bId = req.params.formstep3bId;
    return res.send((await formstep3bDAO.getFormstep3b(formstep3bId)));
}));

formstepThreebRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await formstep3bDAO.getFormstep3b(baseId)));
}));

formstepThreebRouter.put('/', wrap(async (req, res) => {
    const updated: Formstep3BModel = req.body;
    await formstep3bDAO.updateFormstep3b(updated);

    return res.send(await formstep3bDAO.getFormstep3b(updated.Form_step3BId));
}));

formstepThreebRouter.delete('/:formstep3bId', wrap(async (req, res) => {
    const formstep3bId = req.params.FormstepId;
    await formstep3bDAO.deleteFormstep3b(parseInt(formstep3bId, 30));
    return res.sendStatus(304);
}));

export { formstepThreebRouter };
