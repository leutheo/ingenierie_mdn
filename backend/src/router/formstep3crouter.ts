import { Formstep3CModel, FuitesModel, StepModel } from 'common';
import { Router } from 'express';
import { StepDAO } from '../dao/etapesdao';
import { Formstep3CDAO } from '../dao/formstep3cdao';
import { FuitesDAO } from '../dao/fuitesdao';
import { wrap } from '../util';

const formstepThreecRouter = Router();
const formstep3cDAO = new Formstep3CDAO;
const fuitesDAO = new FuitesDAO;
const stepDAO = new StepDAO;

formstepThreecRouter.post('/', wrap(async (req, res) => {

    const principal_formId = req.body.Principal_formId;
    console.log("principal_formId1111111");
    console.log(principal_formId);


    const fuites = FuitesModel.fromJSON(req.body);
    const fuitesId = await fuitesDAO.createfuites(fuites);
    //let fuitesId = 1;
    console.log("fuitesId =====");
    console.log(fuites);


    const formstep3c = Formstep3CModel.fromJSON(req.body);
    formstep3c.FuitesId = fuitesId;
    console.log("formstep3c");
    console.log(formstep3c);
    const formstep3cId = await formstep3cDAO.createFormstep3c(formstep3c);

    let modelStep: any = {
        Principal_formId: principal_formId,
        Form_StepId: formstep3cId,
        Form_Step_IsValid: false,
        Form_step_typeId: 6
    };

    const stepModel = StepModel.fromJSON(modelStep);
    let stepModelId = await stepDAO.createStep(stepModel);
    console.log(stepModelId);
    return res.send(await formstep3cDAO.getFormstep3c(formstep3cId));
}));

formstepThreecRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('etape3c');
}));

formstepThreecRouter.get('/', wrap(async (_req, res) => {
    const formstep3cs = await formstep3cDAO.getFormstep3cs();
    return res.send(formstep3cs);
}));

formstepThreecRouter.get('/:formstep3cId', wrap(async (req, res) => {
    const formstep3cId = req.params.formstep3cId;
    return res.send((await formstep3cDAO.getFormstep3c(formstep3cId)));
}));

formstepThreecRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await formstep3cDAO.getFormstep3c(baseId)));
}));

formstepThreecRouter.put('/', wrap(async (req, res) => {
    const updated: Formstep3CModel = req.body;
    await formstep3cDAO.updateFormstep3c(updated);

    return res.send(await formstep3cDAO.getFormstep3c(updated.Form_step3CId));
}));

formstepThreecRouter.delete('/:formstep3cId', wrap(async (req, res) => {
    const formstep3cId = req.params.FormstepId;
    await formstep3cDAO.deleteFormstep3c(parseInt(formstep3cId, 30));
    return res.sendStatus(304);
}));

export { formstepThreecRouter };
