import { Formstep2Model, StepModel } from 'common';
import { Router } from 'express';
import { StepDAO } from '../dao/etapesdao';
import { Formstep2DAO } from '../dao/formstep2dao';
import { wrap } from '../util';

const formstepSecondRouter = Router();
const formstep2DAO = new Formstep2DAO;
const stepDao = new StepDAO;

formstepSecondRouter.post('/', wrap(async (req, res) => {

    console.log('pincipal_formId');
    console.log(req.body);
    const principal_formId = req.body.Principal_formId;
    const formstep2 = Formstep2Model.fromJSON(req.body);
    const formstep2Id = await formstep2DAO.createFormstep2(formstep2);

    let modelStep: any = {
        Principal_formId: principal_formId,
        Form_StepId: formstep2Id,
        Form_Step_IsValid: false,
        Form_step_typeId: 2
    };
    const stepModel = StepModel.fromJSON(modelStep);
    let stepModelId = await stepDao.createStep(stepModel);
    console.log(stepModelId);
    return res.send(await formstep2DAO.getFormstep2(formstep2Id));
}));

formstepSecondRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('etape2');
}));

formstepSecondRouter.get('/', wrap(async (_req, res) => {
    const formstep2s = await formstep2DAO.getFormstep2s();
    return res.send(formstep2s);
}));

formstepSecondRouter.get('/:formstep2Id', wrap(async (req, res) => {
    const formstep2Id = req.params.formstep2Id;
    return res.send((await formstep2DAO.getFormstep2(formstep2Id)));
}));

formstepSecondRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await formstep2DAO.getFormstep2(baseId)));
}));

formstepSecondRouter.put('/', wrap(async (req, res) => {
    const updated: Formstep2Model = req.body;
    await formstep2DAO.updateFormstep2(updated);

    return res.send(await formstep2DAO.getFormstep2(updated.Form_step2Id));
}));

formstepSecondRouter.delete('/:formstep2Id', wrap(async (req, res) => {
    const formstep2Id = req.params.FormstepId;
    await formstep2DAO.deleteFormstep2(parseInt(formstep2Id, 20));
    return res.sendStatus(204);
}));

export { formstepSecondRouter };
