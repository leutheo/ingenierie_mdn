import { BaseModel } from 'common';
import { Router } from 'express';
import { BaseDAO } from '../dao/basedao';
import { wrap } from '../util';

const baseRouter = Router();
const baseDAO = new BaseDAO;

baseRouter.post('/', wrap(async (req, res) => {
    const base = BaseModel.fromJSON(req.body);
    const baseId = await baseDAO.createBase(base);
    return res.send(await baseDAO.getBase(baseId));
}));

baseRouter.get('/', wrap(async (_req, res) => {
    const bases = await baseDAO.getBases();
    return res.send(bases);
}));

baseRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await baseDAO.getBase(baseId)));
}));
baseRouter.put('/', wrap(async (req, res) => {
    const updated: BaseModel = req.body;
    await baseDAO.updateBase(updated);

    return res.send(await baseDAO.getBase(updated.BaseId));
}));
baseRouter.delete('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    await baseDAO.deleteBase(parseInt(baseId, 10));
    return res.sendStatus(204);
}));

export { baseRouter };
