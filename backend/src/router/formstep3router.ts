import { Formstep3Model, StepModel } from 'common';
import { Router } from 'express';
import { StepDAO } from '../dao/etapesdao';
import { Formstep3DAO } from '../dao/formstep3dao';
import { wrap } from '../util';

const formstepThreeRouter = Router();
const formstep3DAO = new Formstep3DAO;
const stepDAO = new StepDAO;

formstepThreeRouter.post('/', wrap(async (req, res) => {

    const principal_formId = req.body.Principal_formId;
    console.log("principal_formId");
    console.log(principal_formId);

    const formstep3 = Formstep3Model.fromJSON(req.body);
    const formstep3Id = await formstep3DAO.createFormstep3(formstep3);

    let modelStep: any = {
        Principal_formId: principal_formId,
        Form_StepId: formstep3Id,
        Form_Step_IsValid: false,
        Form_step_typeId: 3
    };
    console.log("---------------- -----------   ----------------");
    const stepModel = StepModel.fromJSON(modelStep);
    let stepModelId = await stepDAO.createStep(stepModel);
    console.log(stepModelId);
    return res.send(await formstep3DAO.getFormstep3(formstep3Id));

}));

formstepThreeRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('etape3');
}));

formstepThreeRouter.get('/', wrap(async (_req, res) => {
    const formstep3s = await formstep3DAO.getFormstep3s();
    return res.send(formstep3s);
}));

formstepThreeRouter.get('/:formstep3Id', wrap(async (req, res) => {
    const formstep3Id = req.params.formstep3Id;
    return res.send((await formstep3DAO.getFormstep3(formstep3Id)));
}));

formstepThreeRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await formstep3DAO.getFormstep3(baseId)));
}));

formstepThreeRouter.put('/', wrap(async (req, res) => {
    const updated: Formstep3Model = req.body;
    await formstep3DAO.updateFormstep3(updated);

    return res.send(await formstep3DAO.getFormstep3(updated.Form_step3Id));
}));

formstepThreeRouter.delete('/:formstep3Id', wrap(async (req, res) => {
    const formstep3Id = req.params.FormstepId;
    await formstep3DAO.deleteFormstep3(parseInt(formstep3Id, 30));
    return res.sendStatus(304);
}));

export { formstepThreeRouter };
