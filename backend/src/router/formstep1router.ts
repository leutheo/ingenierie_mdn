import { Formstep1Model, Principal_formModel, StepModel } from 'common';
import { Router } from 'express';
import { StepDAO } from '../dao/etapesdao';
import { Formstep1DAO } from '../dao/formstep1dao';
import { Principal_formDAO } from '../dao/principalformdao';
import { wrap } from '../util';

const formstepFirstRouter = Router();
const formstep1DAO = new Formstep1DAO;
const principal_formDAO = new Principal_formDAO;
const stepDAO = new StepDAO;

formstepFirstRouter.post('/', wrap(async (req, res) => {

    const principal_form = Principal_formModel.fromJSON(req.body);
    const principal_formId = await principal_formDAO.createPrincipal_form(principal_form);

    console.log("principal_formId");
    console.log(principal_formId);

    const formstep1 = Formstep1Model.fromJSON(req.body);
    const formstep1Id = await formstep1DAO.createFormstep1(formstep1);

    let modelStep: any = {
        Principal_formId: principal_formId,
        Form_StepId: formstep1Id,
        Form_Step_IsValid: false,
        Form_step_typeId: 1
    };
    const stepModel = StepModel.fromJSON(modelStep);
    let stepModelId = await stepDAO.createStep(stepModel);
    console.log(stepModelId);
    return res.send(await formstep1DAO.getFormstep1(formstep1Id));
}));

formstepFirstRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('etape1');
}));

formstepFirstRouter.get('/', wrap(async (_req, res) => {
    const formstep1s = await formstep1DAO.getFormstep1s();
    return res.send(formstep1s);
}));

formstepFirstRouter.get('/:formstep1Id', wrap(async (req, res) => {
    const formstep1Id = req.params.formstep1Id;
    return res.send((await formstep1DAO.getFormstep1(formstep1Id)));
}));

formstepFirstRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await formstep1DAO.getFormstep1(baseId)));
}));

formstepFirstRouter.put('/', wrap(async (req, res) => {
    const updated: Formstep1Model = req.body;
    await formstep1DAO.updateFormstep1(updated);

    return res.send(await formstep1DAO.getFormstep1(updated.Form_step1Id));
}));

formstepFirstRouter.delete('/:formstep1Id', wrap(async (req, res) => {
    const formstep1Id = req.params.FormstepId;
    await formstep1DAO.deleteFormstep1(parseInt(formstep1Id, 10));
    return res.sendStatus(204);
}));

export { formstepFirstRouter };
