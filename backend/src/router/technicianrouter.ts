import { TechnicianModel } from 'common';
import { Router } from 'express';
import { TechnicianDAO } from '../dao/techniciandao';
import { wrap } from '../util';

const technicianRouter = Router();
const technicianDAO = new TechnicianDAO;

technicianRouter.post('/', wrap(async (req, res) => {
    const technician = TechnicianModel.fromJSON(req.body);
    const technicianId = await technicianDAO.createTechnician(technician);
    return res.send(await technicianDAO.getTechnician(technicianId));
}));

technicianRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('technician');
}));

technicianRouter.get('/', wrap(async (_req, res) => {
    const technicians = await technicianDAO.getTechnicians();
    return res.send(technicians);
}));

technicianRouter.get('/:technicianId', wrap(async (req, res) => {
    const technicianId = req.params.technicianId;
    return res.send((await technicianDAO.getTechnician(technicianId)));
}));

technicianRouter.get('/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await technicianDAO.getTechnician(baseId)));
}));

technicianRouter.put('/', wrap(async (req, res) => {
    const updated: TechnicianModel = req.body;
    await technicianDAO.updateTechnician(updated);

    return res.send(await technicianDAO.getTechnician(updated.TechnicianId));
}));

technicianRouter.delete('/:technicianId', wrap(async (req, res) => {
    const technicianId = req.params.FormstepId;
    await technicianDAO.deleteTechnician(parseInt(technicianId, 30));
    return res.sendStatus(304);
}));


export { technicianRouter };
