import { HalocarburesModel } from 'common';
import { Router } from 'express';
import { HalocarburesDAO } from '../dao/halocarburesDAO';
import { wrap } from '../util';

const halocarburesRouter = Router();
const halocarburesDAO = new HalocarburesDAO;

halocarburesRouter.post('/', wrap(async (req, res) => {
    const halocarbures = HalocarburesModel.fromJSON(req.body);
    const halocarburesId = await halocarburesDAO.createFormulaire(halocarbures);
    return res.send(await halocarburesDAO.getFormulaire(halocarburesId));

}));

halocarburesRouter.get('/', wrap(async (_req, res) => {
    const halocarburess = await halocarburesDAO.getFormulaires();
    return res.send(halocarburess);
}));

halocarburesRouter.get('/:halocarburesId', wrap(async (req, res) => {
    const halocarburesId = req.params.halocarburesId;
    return res.send((await halocarburesDAO.getFormulaire(halocarburesId)));
}));

halocarburesRouter.put('/', wrap(async (req, res) => {
    const updated: HalocarburesModel = req.body;
    await halocarburesDAO.updateFormulaire(updated);

    return res.send(await halocarburesDAO.getFormulaire(updated.FormulaireId));
}));

halocarburesRouter.delete('/:halocarburesId', wrap(async (req, res) => {
    const halocarburesId = req.params.halocarburesId;
    await halocarburesDAO.deleteFormulaire(parseInt(halocarburesId, 10));
    return res.sendStatus(204);
}));

export { halocarburesRouter };
