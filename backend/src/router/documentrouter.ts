import { CONSTANT, DocumentModel } from 'common';
import { Router } from 'express';
import { createWriteStream } from 'fs';
import multer from 'multer';
import { DocumentDAO } from '../dao/documentdao';
import { wrap } from '../util';

const documentRouter = Router();
const documentDAO = new DocumentDAO;
const upload = multer();

documentRouter.post('/', upload.single('file'), wrap(async (req, res) => {
    const document = DocumentModel.fromJSON(JSON.parse(req.body.document));
    let documentId = document.DocumentId;
    if (documentId == undefined) {
        documentId = await documentDAO.createDocument(document);
    } else {
        await documentDAO.updateDocument(document);
    }
    //create file
    let file: any = req.file;
    let writeFileStream = createWriteStream(CONSTANT.DOC_PATH_DESTINATION + '/' + document.DocDesitnationPath);
    writeFileStream.write(file.buffer, 'base64');
    writeFileStream.on('close', () => {
        console.log('FileStream was closed!!!');
    });

    writeFileStream.end();

    return res.send(await documentDAO.getDocument(documentId));

}));

documentRouter.post('/add', wrap(async (req, res) => {
    console.log(req);
    return res.send('hello word');

}));

documentRouter.get('/', wrap(async (_req, res) => {
    const documents = await documentDAO.getDocuments();
    return res.send(documents);
}));

documentRouter.get('/:documentId', wrap(async (req, res) => {
    const documentId = req.params.documentId;
    return res.send((await documentDAO.getDocument(documentId)));
}));

documentRouter.get('/base/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await documentDAO.getDocumentBaseId(baseId)));
}));

documentRouter.put('/', wrap(async (req, res) => {
    const updated: DocumentModel = req.body;
    await documentDAO.updateDocument(updated);

    return res.send(await documentDAO.getDocument(updated.DocumentId));
}));

documentRouter.delete('/:documentId', wrap(async (req, res) => {
    const documentId = req.params.documentId;
    await documentDAO.deleteDocument(parseInt(documentId, 10));
    return res.sendStatus(204);
}));

export { documentRouter };
