import { SectionDevisGroupeModel } from 'common';
import { Router } from 'express';
import { SectionDevisGroupeDAO } from '../dao/sectiondevisgroupedao';
import { wrap } from '../util';

const sectiondevisgroupeRouter = Router();
const sectiondevisgroupeDAO = new SectionDevisGroupeDAO;

sectiondevisgroupeRouter.post('/', wrap(async (req, res) => {
    const sectiondevisgroupe = SectionDevisGroupeModel.fromJSON(req.body);
    const sectiondevisgroupeId = await sectiondevisgroupeDAO.createSectionDevisGroupe(sectiondevisgroupe);
    return res.send(await sectiondevisgroupeDAO.getSectionDevisGroupe(sectiondevisgroupeId));
}));

sectiondevisgroupeRouter.get('/', wrap(async (_req, res) => {
    const sectiondevisgroupes = await sectiondevisgroupeDAO.getSectionDevisGroupes();
    return res.send(sectiondevisgroupes);
}));

sectiondevisgroupeRouter.get('/:sectiondevisgroupeId', wrap(async (req, res) => {
    const sectiondevisgroupeId = req.params.Section_Devis_GroupeId;
    return res.send((await sectiondevisgroupeDAO.getSectionDevisGroupe(sectiondevisgroupeId)));
}));
sectiondevisgroupeRouter.put('/', wrap(async (req, res) => {
    const updated: SectionDevisGroupeModel = req.body;
    await sectiondevisgroupeDAO.updateSectionDevisGroupe(updated);

    return res.send(await sectiondevisgroupeDAO.getSectionDevisGroupe(updated.Section_Devis_GroupeId));
}));
sectiondevisgroupeRouter.delete('/:sectiondevisgroupeId', wrap(async (req, res) => {
    const sectiondevisgroupeId = req.params.Section_Devis_GroupeId;
    await sectiondevisgroupeDAO.deleteSectionDevisGroupe(parseInt(sectiondevisgroupeId, 10));
    return res.sendStatus(204);
}));

export { sectiondevisgroupeRouter };
