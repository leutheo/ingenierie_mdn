import { CONSTANT, SectionDevisModel } from 'common';
import { Router } from 'express';
import { createWriteStream } from 'fs';
import multer from 'multer';
import { SectionDevisDAO } from '../dao/sectiondevisdao';
import { wrap } from '../util';

const sectiondevisRouter = Router();
const sectiondevisDAO = new SectionDevisDAO;
const upload = multer();

sectiondevisRouter.post('/', upload.single('file'), wrap(async (req, res) => {

    const sectiondevis = SectionDevisModel.fromJSON(JSON.parse(req.body.document));
    const sectiondevisId = await sectiondevisDAO.createSection_Devis(sectiondevis);

    //create file
    let file: any = req.file;
    let writeFileStream = createWriteStream(CONSTANT.DOC_PATH_DESTINATION + '/' + sectiondevis.DocDestinationPath);
    writeFileStream.write(file.buffer, 'base64');
    writeFileStream.on('close', () => {
        console.log('FileStream was closed!!!');
    });

    return res.send(await sectiondevisDAO.getSection_Devis(sectiondevisId));

}));

sectiondevisRouter.post('/add', wrap(async (req, res) => {
    return res.send(req.params);

}));

sectiondevisRouter.get('/', wrap(async (_req, res) => {
    const sectiondeviss = await sectiondevisDAO.getSection_Deviss();
    return res.send(sectiondeviss);
}));

sectiondevisRouter.get('/:sectiondevisId', wrap(async (req, res) => {
    const sectiondevisId = req.params.sectiondevisId;
    return res.send((await sectiondevisDAO.getSection_Devis(sectiondevisId)));
}));

sectiondevisRouter.get('/baseid/:baseId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    return res.send((await sectiondevisDAO.getSection_Devis(baseId)));
}));

sectiondevisRouter.get('/base/:baseId/group/:groupId', wrap(async (req, res) => {
    const baseId = req.params.baseId;
    const groupId = req.params.groupId;
    console.log(req.params);
    return res.send((await sectiondevisDAO.getSectionDevisByGroupId(baseId, groupId)));
}));

sectiondevisRouter.put('/', wrap(async (req, res) => {
    const updated: SectionDevisModel = req.body;
    await sectiondevisDAO.updateSection_Devis(updated);

    return res.send(await sectiondevisDAO.getSection_Devis(updated.Section_DevisId));
}));

sectiondevisRouter.delete('/:sectiondevisId', wrap(async (req, res) => {
    const sectiondevisId = req.params.sectiondevisId;
    await sectiondevisDAO.deleteSection_Devis(parseInt(sectiondevisId, 10));
    return res.sendStatus(204);
}));

export { sectiondevisRouter };
