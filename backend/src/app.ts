import bodyParser from 'body-parser';
import errorHandler from 'errorhandler';
import express from 'express';
import { baseRouter } from './router/baserouter';
import { companyRouter } from './router/companyrouter';
import { documentRouter } from './router/documentRouter';
import { stepRouter } from './router/etapesrouter';
import { formstepFirstRouter } from './router/formstep1router';
import { formstepSecondRouter } from './router/formstep2router';
import { formstepThreeaRouter } from './router/formstep3arouter';
import { formstepThreebRouter } from './router/formstep3brouter';
import { formstepThreecRouter } from './router/formstep3crouter';
import { formstepThreeRouter } from './router/formstep3router';
import { halocarburesRouter } from './router/halocarburesRouter';
import { principal_formRouter } from './router/principalformrouter';
import { roleRouter } from './router/rolerouter';
import { salletechniqueRouter } from './router/salletechniquerouter';
import { sectiondevisgroupeRouter } from './router/sectiondevisgrouperouter';
import { sectiondevisRouter } from './router/sectiondevisrouter';
import { technicianRouter } from './router/technicianrouter';
import { userRouter } from './router/userrouter';

const app = express();

app.set('trust proxy', 'loopback');

app.use(errorHandler({ log: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

app.use((_req, res, next) => {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
});

app.use('/user', userRouter);
app.use('/base', baseRouter);
app.use('/role', roleRouter);
app.use('/document', documentRouter);
app.use('/sectiondevisgroupe', sectiondevisgroupeRouter);
app.use('/sectiondevis', sectiondevisRouter);
app.use('/salletechnique', salletechniqueRouter);
app.use('/halocarbures', halocarburesRouter);
app.use('/stepone', formstepFirstRouter);
app.use('/steptwo', formstepSecondRouter);
app.use('/stepthree', formstepThreeRouter);
app.use('/stepthreea', formstepThreeaRouter);
app.use('/stepthreeb', formstepThreebRouter);
app.use('/stepthreec', formstepThreecRouter);
app.use('/company', companyRouter);
app.use('/technician', technicianRouter);
app.use('/step', stepRouter);
app.use('/principalform', principal_formRouter);
export { app };
