import { RoleModel } from 'common';
import { DBProvider } from '../dbprovider';

export class RoleDAO {

    private knex = DBProvider.getKnexConnection();

    public async createRole(user: RoleModel) {
        const [roleId] = await this.knex('role').insert({
            RoleId: user.RoleId,
            Name: user.Name,
            Creation_Date: new Date(),
            update_Date: new Date(),
            IsDelete: user.IsDelete,
        });
        return roleId;
    }

    public async getRole(RoleId: number | string) {
        const role = await this.knex('role').first('*').where({ RoleId });
        if (!role) { return null; }
        return RoleModel.fromJSON(role);
    }

    public async updateRole(role: RoleModel) {
        const roleId = role.RoleId;
        await this.knex('role').update({
            Name: role.Name,
            update_Date: new Date()
        }).where({ roleId });
    }

    public async deleteRole(roleId: number) {
        await this.knex('role').update({
            IsDelete: true,
            update_Date: new Date(),
            Delete_Date: new Date()
        }).where({ roleId });
    }

    public async getRoles() {
        const roles = await this.knex('role').where('IsDelete', '=', false);
        return roles.map(RoleModel.fromJSON);
    }

}
