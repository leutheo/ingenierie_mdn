import { Formstep3BModel } from 'common';
import { DBProvider } from '../dbprovider';

export class Formstep3BDAO {
    private knex = DBProvider.getKnexConnection();

    public async createFormstep3b(formstep3b: Formstep3BModel) {

        console.log(formstep3b);
        let _actu_date = formstep3b.Actual_date.split('/');
        let actu_date = new Date(parseFloat(_actu_date[2]), parseFloat(_actu_date[1]) - 1, parseFloat(_actu_date[0]));
        console.log(actu_date);

        let model = {
            Actual_date: actu_date,
            System_type: formstep3b.System_type,
            Actual_UnityId: formstep3b.Actual_UnityId,
            Retrait_reason: formstep3b.Retrait_reason,
            Unity_adress: formstep3b.Unity_adress,
            Condition: formstep3b.Condition,
            Technicien_Lastname: formstep3b.Technicien_Lastname,
            Technicien_Firstname: formstep3b.Technicien_Firstname,
            Signature: formstep3b.Signature,
            IsActif: true,
            IsDelete: false
        };
        console.log("model");
        console.log(model);
        const [form_step3BId] = await this.knex('Form_step3B').insert(model);
        return form_step3BId;
    }

    public async getFormstep3b(Form_step3bId: number | string) {
        const formstep3b = await this.knex('Form_step3B').first('*').where({ Form_step3bId });
        if (!formstep3b) { return null; }
        return Formstep3BModel.fromJSON(formstep3b);
    }

    public async updateFormstep3b(formstep3b: Formstep3BModel) {
        const form_step3bId = formstep3b.Form_step3BId;
        await this.knex('Form_step3B').update({
            Actual_date: new Date().toLocaleString(),
            System_type: formstep3b.System_type, Actual_UnityId: formstep3b.Actual_UnityId,
            Retrait_reason: formstep3b.Retrait_reason, Unity_adress: formstep3b.Unity_adress,
            Condition: formstep3b.Condition, Technicien_Lastname: formstep3b.Technicien_Lastname,
            Technicien_Firstname: formstep3b.Technicien_Firstname,
            Signature: formstep3b.Signature,
        }).where({ form_step3bId });
    }

    public async deleteFormstep3b(form_step3bId: number) {
        await this.knex('Form_step3B').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ form_step3bId });
    }

    public async getFormstep3bs() {
        const formstep3bs = await this.knex('Form_step3B').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return formstep3bs.map(Formstep3BModel.fromJSON);
    }
}
