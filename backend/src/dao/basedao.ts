import { BaseModel } from 'common';
import { DBProvider } from '../dbprovider';

export class BaseDAO {
    private knex = DBProvider.getKnexConnection();

    public async createBase(user: BaseModel) {

        const [baseId] = await this.knex('base').insert({
            BaseId: user.BaseId, Name: user.Name,
            IsDelete: user.IsDelete,
            Creation_Date: new Date(),
            update_Date: new Date(),
        });
        return baseId;
    }

    public async getBase(BaseId: number | string) {
        const base = await this.knex('base').first('*').where({ BaseId });
        if (!base) { return null; }
        return BaseModel.fromJSON(base);
    }

    public async updateBase(base: BaseModel) {
        const baseId = base.BaseId;
        await this.knex('base').update({
            Name: base.Name, update_Date: new Date()
        }).where({ baseId });
    }

    public async deleteBase(baseId: number) {
        await this.knex('base').update({
            IsDelete: true,
            update_Date: new Date(), Delete_Date: new Date()
        }).where({ baseId });
    }

    public async getBases() {
        const bases = await this.knex('base').where('IsDelete', '=', false);

        return bases.map(BaseModel.fromJSON);
    }
}
