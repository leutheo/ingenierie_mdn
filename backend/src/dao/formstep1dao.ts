import { Formstep1Model } from 'common';
import { DBProvider } from '../dbprovider';

export class Formstep1DAO {
    private knex = DBProvider.getKnexConnection();

    public async createFormstep1(formstep1: Formstep1Model) {
        console.log(formstep1);
        let _actu_date = formstep1.Actual_date.split("/");
        let actu_date = new Date(parseFloat(_actu_date[2]), parseFloat(_actu_date[1]) - 1, parseFloat(_actu_date[0]));
        console.log(actu_date);
        let model = {
            Inspector_Name: formstep1.Inspector_Name,
            Edifice: formstep1.Edifice,
            Project_Number: formstep1.Project_Number,
            Piece: formstep1.Piece,
            Actual_date: actu_date,
            System_Type: formstep1.System_Type,
            Manufacturier: formstep1.Manufacturier,
            Model: formstep1.Model,
            Halocarbon_type: formstep1.Halocarbon_type,
            Infos_sup: formstep1.Infos_sup,
            Typehalocarbure: formstep1.Typehalocarbure,
            GH_Name: formstep1.GH_Name,
            GH_Signature: formstep1.GH_Signature,
            IsActif: true,
            IsDelete: false
        };
        console.log("model");
        console.log(model);
        const [form_step1Id] = await this.knex('Form_step1').insert(model);
        return form_step1Id;
    }

    public async getFormstep1(Form_step1Id: number | string) {
        const formstep1 = await this.knex('Form_step1').first('*').where({ Form_step1Id });
        if (!formstep1) { return null; }
        return Formstep1Model.fromJSON(formstep1);
    }

    public async updateFormstep1(formstep1: Formstep1Model) {
        const form_step1Id = formstep1.Form_step1Id;
        await this.knex('Form_step1').update({
            Form_step1Id: formstep1.Form_step1Id,
            Inspector_Name: formstep1.Inspector_Name,
            Edifice: formstep1.Edifice,
            Project_Number: formstep1.Project_Number,
            Piece: formstep1.Piece,
            Actual_date: new Date().toLocaleString(),
            System_Type: formstep1.System_Type,
            Manufacturier: formstep1.Manufacturier,
            Model: formstep1.Model,
            Halocarbon_type: formstep1.Halocarbon_type,
            Infos_sup: formstep1,
            GH_Name: formstep1.GH_Name,
            GH_Signature: formstep1.GH_Signature,
            IsActif: true,
            IsDelete: false
        }).where({ form_step1Id });
    }

    public async deleteFormstep1(form_step1Id: number) {
        await this.knex('Form_step1').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ form_step1Id });
    }

    public async getFormstep1s() {
        const formstep1s = await this.knex('Form_step1').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return formstep1s.map(Formstep1Model.fromJSON);
    }
}
