import { SalleTechniqueModel } from '../../../common';
import { DBProvider } from '../dbprovider';

export class SalleTechniqueDAO {
    private knex = DBProvider.getKnexConnection();

    public async createSalle_Technique(salletechnique: SalleTechniqueModel) {

        const [salletechniqueId] = await this.knex('salle_technique').insert({
            Document_salle_techniqueId: salletechnique.Document_salle_techniqueId,
            LinkId: salletechnique.LinkId,
            BaseId: salletechnique.BaseId,
            Creation_Date: new Date(),
            update_Date: new Date(),
            IsDelete: false
        });
        return salletechniqueId;
    }

    public async getSalle_Technique(Salle_TechniqueId: number | string) {
        const salletechnique = await this.knex('salle_technique').first('*').where({ Salle_TechniqueId });
        if (!salletechnique) { return null; }
        return SalleTechniqueModel.fromJSON(salletechnique);
    }

    public async getSalle_TechniqueBaseId(BaseId: number | string) {
        const salletechniques = await this.knex('salle_technique')
            .where({ BaseId })
            .innerJoin('link', 'link.linkid', 'salle_technique.linkid')
            .innerJoin('document_salle_technique', 'document_salle_technique.document_salle_techniqueid', 'salle_technique.document_salle_techniqueid');
        if (!salletechniques) { return null; }

        return salletechniques.map(SalleTechniqueModel.fromJSON);
    }

    public async updateSalle_Technique(salletechnique: SalleTechniqueModel) {
        const salletechniqueId = salletechnique.Salle_TechniqueId;
        await this.knex('salle_technique').update({
            Salle_Technique: salletechnique.Salle_TechniqueId, Document_salle_technique: salletechnique.Document_salle_techniqueId,
            Link: salletechnique.LinkId, update_Date: new Date().toLocaleString(),
        }).where({ salletechniqueId });
    }

    public async deleteSalle_Technique(salletechniqueId: number) {
        await this.knex('salle_technique').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ salletechniqueId });
    }

    public async getSalle_Techniques() {
        const salletechniques = await this.knex('salle_technique').where('IsDelete', '=', false);

        return salletechniques.map(SalleTechniqueModel.fromJSON);
    }
}
