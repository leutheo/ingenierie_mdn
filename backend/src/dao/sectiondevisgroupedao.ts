import { SectionDevisGroupeModel } from 'common';
import { DBProvider } from '../dbprovider';

export class SectionDevisGroupeDAO {
    private knex = DBProvider.getKnexConnection();

    public async createSectionDevisGroupe(sectiondevisgroupe: SectionDevisGroupeModel) {

        const [sectiondevisgroupeId] = await this.knex('section_devis_groupe').insert({
            Section_Devis_GroupeId: sectiondevisgroupe.Section_Devis_GroupeId,
            Name: sectiondevisgroupe.Name,
            IsDelete: sectiondevisgroupe.IsDelete,
            Creation_Date: new Date(),
            update_Date: new Date(),
        });
        return sectiondevisgroupeId;
    }

    public async getSectionDevisGroupe(Section_Devis_GroupeId: number | string) {
        const sectiondevisgroupe = await this.knex('section_devis_groupe').first('*').where({ Section_Devis_GroupeId });
        if (!sectiondevisgroupe) { return null; }
        return SectionDevisGroupeModel.fromJSON(sectiondevisgroupe);
    }

    public async updateSectionDevisGroupe(sectiondevisgroupe: SectionDevisGroupeModel) {
        const sectiondevisgroupeId = sectiondevisgroupe.Section_Devis_GroupeId;
        await this.knex('section_devis_groupe').update({
            Name: sectiondevisgroupe.Name, update_Date: new Date()
        }).where({ sectiondevisgroupeId });
    }

    public async deleteSectionDevisGroupe(Section_Devis_GroupeId: number) {
        await this.knex('section_devis_groupe').update({
            IsDelete: true,
            update_Date: new Date(), Delete_Date: new Date()
        }).where({ Section_Devis_GroupeId });
    }

    public async getSectionDevisGroupes() {
        const sectiondevisgroupes = await this.knex('section_devis_groupe').where('IsDelete', '=', false);

        return sectiondevisgroupes.map(SectionDevisGroupeModel.fromJSON);
    }
}
