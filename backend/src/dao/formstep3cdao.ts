import { Formstep3CModel } from 'common';
import { DBProvider } from '../dbprovider';

export class Formstep3CDAO {
    private knex = DBProvider.getKnexConnection();

    public async createFormstep3c(formstep3c: Formstep3CModel) {
        console.log(formstep3c);

        let model = {
            Edifice: formstep3c.Edifice,
            Piece: formstep3c.Piece,
            Bon_Travail: formstep3c.Bon_Travail,
            Unity_id: formstep3c.Unity_id,
            Condition: formstep3c.Condition,
            Technicien_Lastname: formstep3c.Technicien_Lastname,
            Technicien_Firstname: formstep3c.Technicien_Firstname,
            Signature: formstep3c.Signature,
            IsActif: true,
            IsDelete: false,
            FuitesId: formstep3c.FuitesId
        };
        console.log("le model");
        console.log(model);
        const [form_step3CId] = await this.knex('Form_step3C').insert(model);
        return form_step3CId;
    }

    public async getFormstep3c(Form_step3cId: number | string) {
        const formstep3c = await this.knex('Form_step3C').first('*').where({ Form_step3cId });
        if (!formstep3c) { return null; }
        return Formstep3CModel.fromJSON(formstep3c);
    }

    public async updateFormstep3c(formstep3c: Formstep3CModel) {
        const form_step3cId = formstep3c.Form_step3CId;
        await this.knex('Form_step3C').update({
            Edifice: formstep3c.Edifice,
            Piece: formstep3c.Piece,
            Bon_Travail: formstep3c.Bon_Travail,
            Unity_id: formstep3c.Unity_id,
            Condition: formstep3c.Condition,
            Technicien_Lastname: formstep3c.Technicien_Lastname,
            Technicien_Firstname: formstep3c.Technicien_Firstname,
            Signature: formstep3c.Signature,
            IsActif: true,
            IsDelete: false
        }).where({ form_step3cId });
    }

    public async deleteFormstep3c(form_step3cId: number) {
        await this.knex('Form_step3C').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ form_step3cId });
    }

    public async getFormstep3cs() {
        const formstep3cs = await this.knex('Form_step3C').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return formstep3cs.map(Formstep3CModel.fromJSON);
    }
}
