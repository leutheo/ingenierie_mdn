import { TechnicianModel } from 'common';
import { DBProvider } from '../dbprovider';

export class TechnicianDAO {
    private knex = DBProvider.getKnexConnection();

    public async createTechnician(technician: TechnicianModel) {

        const [TechnicianId] = await this.knex('technician').insert({
            Name: technician.Name,
            CarteId: technician.CarteId,
            Expiration_Date: technician.Expiration_Date,
            CarteId_Refrigeration_compet: technician.CarteId_Refrigeration_compet,
            Expiration_Date_CarteId_Refrigeration_compet: technician.Expiration_Date_CarteId_Refrigeration_compet,
            Form_step3Id: technician.Form_step3Id,
        });
        return TechnicianId;
    }

    public async getTechnician(TechnicianId: number | string) {
        const technician = await this.knex('technician').first('*').where({ TechnicianId });
        if (!technician) { return null; }
        return TechnicianModel.fromJSON(technician);
    }

    public async updateTechnician(technician: TechnicianModel) {
        const technicianId = technician.TechnicianId;
        await this.knex('technician').update({
            Name: technician.Name,
            CarteId: technician.CarteId,
            Expiration_Date: technician.Expiration_Date,
            CarteId_Refrigeration_compet: technician.CarteId_Refrigeration_compet,
            Expiration_Date_CarteId_Refrigeration_compet: technician.Expiration_Date_CarteId_Refrigeration_compet,
            Form_step3Id: technician.Form_step3Id,
        }).where({ technicianId });
    }

    public async deleteTechnician(technicianId: number) {
        await this.knex('technician').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ technicianId });
    }

    public async getTechnicians() {
        const technician = await this.knex('technician').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return technician.map(TechnicianModel.fromJSON);
    }
}
