import { Formstep3AModel } from 'common';
import { DBProvider } from '../dbprovider';

export class Formstep3aDAO {
    private knex = DBProvider.getKnexConnection();

    public async createFormstep3a(formstep3a: Formstep3AModel) {
        console.log(formstep3a);
        let _actu_date = formstep3a.Actual_date.split('/');
        let actu_date = new Date(parseFloat(_actu_date[2]), parseFloat(_actu_date[1]) - 1, parseFloat(_actu_date[0]));
        console.log(actu_date);

        let model = {
            Unity_Name: formstep3a.Unity_Name,
            Name: formstep3a.Name,
            Actual_date: actu_date,
            Edifice: formstep3a.Edifice,
            Piece: formstep3a.Piece,
            Unity_adress: formstep3a.Unity_adress,
            Retrait_reason: formstep3a.Retrait_reason,
            Manufacturier: formstep3a.Manufacturier,
            Model: formstep3a.Model,
            Type_refrigerant: formstep3a.Type_refrigerant,
            Number_serie: formstep3a.Number_serie,
            System_type: formstep3a.System_type,
            GH_coments: formstep3a.GH_coments,
            Technicien_Lastname: formstep3a.Technicien_Lastname,
            Technicien_Firstname: formstep3a.Technicien_Firstname,
            Signature: formstep3a.Signature,
            IsActif: true,
            IsDelete: false
        };
        console.log("le model");
        console.log(model);
        const [form_step3AId] = await this.knex('Form_step3A').insert(model);
        return form_step3AId;
    }

    public async getFormstep3a(Form_step3aId: number | string) {
        const formstep3a = await this.knex('Form_step3A').first('*').where({ Form_step3aId });
        if (!formstep3a) { return null; }
        return Formstep3AModel.fromJSON(formstep3a);
    }

    public async updateFormstep3a(formstep3a: Formstep3AModel) {
        const form_step3aId = formstep3a.Form_step3AId;
        await this.knex('Form_step3A').update({
            Unity_Name: formstep3a.Unity_Name,
            Name: formstep3a.Name,
            Actual_date: new Date().toLocaleString(),
            Edifice: formstep3a.Edifice,
            Piece: formstep3a.Piece,
            Unity_adress: formstep3a.Unity_adress,
            Retrait_reason: formstep3a.Retrait_reason,
            Manufacturier: formstep3a.Manufacturier,
            Model: formstep3a.Model,
            Type_refrigerant: formstep3a.Type_refrigerant,
            Number_serie: formstep3a.Number_serie,
            System_type: formstep3a.System_type,
            GH_coments: formstep3a.GH_coments,
            Technicien_Lastname: formstep3a.Technicien_Lastname,
            Technicien_Firstname: formstep3a.Technicien_Firstname,
            Signature: formstep3a.Signature,
            IsActif: true,
            IsDelete: false
        }).where({ form_step3aId });
    }

    public async deleteFormstep3a(form_step3aId: number) {
        await this.knex('Form_step3A').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ form_step3aId });
    }

    public async getFormstep3as() {
        const formstep3as = await this.knex('Form_step3A').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return formstep3as.map(Formstep3AModel.fromJSON);
    }
}
