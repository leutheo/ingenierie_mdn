import { UserModel } from '../../../common';
import { DBProvider } from '../dbprovider';

export class UserDAO {
    private knex = DBProvider.getKnexConnection();

    public async createUser(user: UserModel) {

        const [userId] = await this.knex('user').insert({
            RoleId: user.RoleId, LastName: user.LastName, FirstName: user.FirstName, BaseId: user.BaseId,
            Email: user.Email, Password: user.Password, CompanyName: user.CompanyName, IsActif: user.IsActif, IsDelete: user.IsDelete, Creation_Date: new Date(),
            update_Date: new Date(),
        });
        return userId;
    }

    public async getUser(UserId: number | string) {
        const user = await this.knex('user').first('*').where({ UserId });
        if (!user) { return null; }
        return UserModel.fromJSON(user);
    }

    public async getUserBaseId(BaseId: number | string) {
        const users = await this.knex('user').where({ BaseId });
        if (!users) { return null; }
        return users.map(UserModel.fromJSON);
    }

    public async updateUser(user: UserModel) {
        const userId = user.UserId;
        await this.knex('user').update({
            LastName: user.LastName, FirstName: user.FirstName, BaseId: user.BaseId,
            Email: user.Email, Password: user.Password, CompanyName: user.CompanyName, IsActif: user.IsActif,
            update_Date: new Date()
        }).where({ userId });
    }

    public async deleteUser(userId: number) {
        await this.knex('user').update({
            IsDelete: true,
            update_Date: new Date(), Delete_Date: new Date()
        }).where({ userId });
    }

    public async getUsers() {
        const users = await this.knex('user').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return users.map(UserModel.fromJSON);
    }

    public async ValidateUserConexion(email: string, password: string) {
        const user = await this.knex('user').where('Email', '=', email).andWhere('Password', '=', password);
        if (user !== null || user !== undefined) {
            return user.map(UserModel.fromJSON);
        }
        return null;
    }
}
