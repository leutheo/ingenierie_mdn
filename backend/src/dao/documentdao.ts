import { DocumentModel } from '../../../common';
import { DBProvider } from '../dbprovider';

export class DocumentDAO {
    private knex = DBProvider.getKnexConnection();

    public async createDocument(document: DocumentModel) {

        const [documentId] = await this.knex('document').insert({
            Titre: document.Titre,
            Description: document.Description,
            DocDesitnationPath: document.DocDesitnationPath,
            Creation_Date: new Date(),
            update_Date: new Date(),
            Delete_Date: new Date(),
            IsDelete: document.IsDelete,
            BaseId: document.BaseId
        });
        return documentId;
    }

    public async getDocument(DocumentId: number | string) {
        const document = await this.knex('document').first('*').where({ DocumentId });
        if (!document) { return null; }
        return DocumentModel.fromJSON(document);
    }

    public async getDocumentBaseId(BaseId: number | string) {
        const documents = await this.knex('document').where({ BaseId });
        if (!documents) { return null; }
        return documents.map(DocumentModel.fromJSON);
    }

    public async updateDocument(document: DocumentModel) {
        const documentId = document.DocumentId;
        await this.knex('document').update({
            Titre: document.Titre, Description: document.Description,
            update_Date: new Date().toLocaleString(),
        }).where({ documentId });
    }

    public async deleteDocument(documentId: number) {
        await this.knex('document').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ documentId });
    }

    public async getDocuments() {
        const documents = await this.knex('document').where('IsDelete', '=', false);

        return documents.map(DocumentModel.fromJSON);
    }
}
