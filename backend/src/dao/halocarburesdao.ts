import { HalocarburesModel } from 'common';
import { DBProvider } from '../dbprovider';

export class HalocarburesDAO {
    private knex = DBProvider.getKnexConnection();

    public async createFormulaire(halocarbure: HalocarburesModel) {

        const [halocarbureId] = await this.knex('halocarbure').insert({
            Formulaire: halocarbure.FormulaireId, Name: halocarbure.Name, BaseId: halocarbure.BaseId,
            Description: halocarbure.Description, Creation_Date: new Date().toLocaleString(),
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString(),
            IsDelete: halocarbure.IsDelete
        });
        return halocarbureId;
    }

    public async getFormulaire(FormulaireId: number | string) {
        const halocarbure = await this.knex('halocarbure').first('*').where({ FormulaireId });
        if (!halocarbure) { return null; }
        return HalocarburesModel.fromJSON(halocarbure);
    }

    public async updateFormulaire(halocarbure: HalocarburesModel) {
        const halocarbureId = halocarbure.FormulaireId;
        await this.knex('halocarbure').update({
            Formulaire: halocarbure.FormulaireId, update_Date: new Date().toLocaleString(),
        }).where({ halocarbureId });
    }

    public async deleteFormulaire(halocarbureId: number) {
        await this.knex('halocarbure').update({
            IsDelete: true, update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ halocarbureId });
    }

    public async getFormulaires() {
        const halocarbures = await this.knex('halocarbure').where('IsDelete', '=', false);

        return halocarbures.map(HalocarburesModel.fromJSON);
    }
}
