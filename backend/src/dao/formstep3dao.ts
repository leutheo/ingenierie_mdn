import { Formstep3Model } from 'common';
import { DBProvider } from '../dbprovider';

export class Formstep3DAO {
    private knex = DBProvider.getKnexConnection();

    public async createFormstep3(formstep3: Formstep3Model) {
        console.log(formstep3);
        let _actu_date = formstep3.Actual_date.split('/');
        let actu_date = new Date(parseFloat(_actu_date[2]), parseFloat(_actu_date[1]) - 1, parseFloat(_actu_date[0]));
        console.log(actu_date);

        let model = {
            Actual_date: actu_date,
            Edifice: formstep3.Edifice,
            Piece: formstep3.Piece,
            Inspector_Name: formstep3.Inspector_Name,
            Inspector_Phone_Number: formstep3.Inspector_Phone_Number.toString(),
            System_Type: formstep3.System_Type,
            Project_Number: formstep3.Project_Number,
            Blue_Plakette: formstep3.Blue_Plakette,
            Manufacturier: formstep3.Manufacturier,
            Model: formstep3.Model,
            Number_serie: formstep3.Number_serie,
            Description: formstep3.Description,
            Date_Rejet: actu_date,
            Final_reparation: actu_date,
            Technicien_Lastname: formstep3.Technicien_Lastname,
            Technicien_Firstname: formstep3.Technicien_Firstname,
            Signature: formstep3.Signature,
            IsActif: true,
            IsDelete: false
        };
        console.log('======================model');
        console.log(model);
        const [form_step3Id] = await this.knex('Form_step3').insert(model);
        return form_step3Id;
    }

    public async getFormstep3(Form_step3Id: number | string) {
        const formstep3 = await this.knex('Form_step3').first('*').where({ Form_step3Id });
        if (!formstep3) { return null; }
        return Formstep3Model.fromJSON(formstep3);
    }

    public async updateFormstep3(formstep3: Formstep3Model) {
        const form_step3Id = formstep3.Form_step3Id;

        let data = {
            Actual_date: new Date().toLocaleString(),
            Edifice: formstep3.Edifice,
            Piece: formstep3.Piece,
            Inspector_Name: formstep3.Inspector_Name,
            Inspector_Phone_Number: formstep3.Inspector_Phone_Number,
            Project_Number: formstep3.Project_Number,
            Blue_Plakette: formstep3.Blue_Plakette,
            Manufacturier: formstep3.Manufacturier,
            System_Type: formstep3.System_Type,
            Model: formstep3.Model,
            Number_serie: formstep3.Number_serie,
            Description: formstep3.Description,
            Date_Rejet: formstep3.Date_Rejet,
            Final_reparation: formstep3.Final_reparation,
            Technicien_Lastname: formstep3.Technicien_Lastname,
            Technicien_Firstname: formstep3.Technicien_Firstname,
            Signature: formstep3.Signature,
            IsActif: true,
            IsDelete: false
        };
        console.log('dddddddddddddddddd');
        console.log(data);
        await this.knex('Form_step3').update(data).where({ form_step3Id });
    }

    public async deleteFormstep3(form_step3Id: number) {
        await this.knex('Form_step3').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ form_step3Id });
    }

    public async getFormstep3s() {
        const formstep3s = await this.knex('Form_step3').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return formstep3s.map(Formstep3Model.fromJSON);
    }
}
