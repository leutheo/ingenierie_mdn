import { StepModel } from 'common';
import { DBProvider } from '../dbprovider';

export class StepDAO {
    private knex = DBProvider.getKnexConnection();

    public async createStep(step: StepModel) {

        const [stepId] = await this.knex('step').insert({
            Principal_formId: step.Principal_formId,
            Form_StepId: step.Form_StepId,
            Form_Step_IsValid: step.Form_Step_IsValid,
            Form_step_typeId: step.Form_step_typeId,
            Creation_Date: new Date(),
            update_Date: new Date(),
            IsDelete: false,
        });
        return stepId;
    }

    public async getStep(StepId: number | string) {
        const step = await this.knex('step').first('*').where({ StepId });
        if (!step) { return null; }
        return StepModel.fromJSON(step);
    }

    public async updateStep(step: StepModel) {
        const stepId = step.StepId;
        await this.knex('step').update({
            Principal_formId: step.Principal_formId, Form_StepId: step.Form_StepId,
            Form_Step_IsValid: step.Form_Step_IsValid, Form_step_typeId: step.Form_step_typeId,
            Creation_Date: new Date().toLocaleString(),
            Update_Date: new Date().toLocaleString(),
            Delete_Date: new Date().toLocaleString(),
            IsDelete: new Date().toLocaleString(),

        }).where({ stepId });
    }

    public async deleteStep(stepId: number) {
        await this.knex('step').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ stepId });
    }

    public async getSteps() {
        const steps = await this.knex('step').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return steps.map(StepModel.fromJSON);
    }

}
