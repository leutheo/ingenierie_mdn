import { Formstep2Model } from 'common';
import { DBProvider } from '../dbprovider';

export class Formstep2DAO {
    private knex = DBProvider.getKnexConnection();

    public async createFormstep2(formstep2: Formstep2Model) {
        console.log(formstep2);
        let _actu_date = formstep2.Actual_date.split('/');
        let actu_date = new Date(parseFloat(_actu_date[2]), parseFloat(_actu_date[1]) - 1, parseFloat(_actu_date[0]));
        console.log(actu_date);
        let model = {
            Actual_date: actu_date,
            Edifice: formstep2.Edifice,
            Piece: formstep2.Piece,
            Company_Name: formstep2.Company_Name,
            Phone_Number: formstep2.Phone_Number,
            Adress: formstep2.Adress,
            Manufacturier: formstep2.Manufacturier,
            System_Type: formstep2.System_Type,
            Model: formstep2.Model,
            Nunber_serie: formstep2.Nunber_serie,
            Quantity_refrigerant: formstep2.Quantity_refrigerant,
            Capacity: formstep2.Capacity,
            Infos_sup: formstep2.Infos_sup,
            GH_Name: formstep2.GH_Name,
            GH_Signature: formstep2.GH_Signature,
            IsActif: true,
            IsDelete: false
        };
        console.log('model');
        console.log(model);
        const [form_step2Id] = await this.knex('Form_step2').insert(model);
        return form_step2Id;
    }

    public async getFormstep2(Form_step2Id: number | string) {
        const formstep2 = await this.knex('Form_step2').first('*').where({ Form_step2Id });
        if (!formstep2) { return null; }
        return Formstep2Model.fromJSON(formstep2);
    }

    public async updateFormstep2(formstep2: Formstep2Model) {
        const form_step2Id = formstep2.Form_step2Id;
        await this.knex('Form_step2').update({
            Actual_date: new Date().toLocaleString(),
            Edifice: formstep2.Edifice,
            Piece: formstep2.Piece,
            Company_Name: formstep2.Company_Name,
            Phone_Number: formstep2.Phone_Number,
            Adress: formstep2.Adress,
            Manufacturier: formstep2.Manufacturier,
            System_Type: formstep2.System_Type,
            Model: formstep2.Model,
            Nunber_serie: formstep2.Nunber_serie,
            Quantity_refrigerant: formstep2.Quantity_refrigerant,
            Capacity: formstep2.Capacity,
            Infos_sup: formstep2.Infos_sup,
            GH_Name: formstep2.GH_Name,
            GH_Signature: formstep2.GH_Signature,
            IsActif: true,
            IsDelete: false
        }).where({ form_step2Id });
    }

    public async deleteFormstep2(form_step2Id: number) {
        await this.knex('Form_step2').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ form_step2Id });
    }

    public async getFormstep2s() {
        const formstep2s = await this.knex('Form_step2').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return formstep2s.map(Formstep2Model.fromJSON);
    }
}
