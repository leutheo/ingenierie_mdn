import { FuitesModel } from 'common';
import { DBProvider } from '../dbprovider';

export class FuitesDAO {
    private knex = DBProvider.getKnexConnection();

    public async createfuites(fuites: FuitesModel) {
        console.log(fuites);
        let _date_founds = fuites.Date_founds.split('/');
        let date_founds = new Date(parseFloat(_date_founds[2]), parseFloat(_date_founds[1]) - 1, parseFloat(_date_founds[0]));
        console.log(date_founds);

        let _date_repairs = fuites.Date_repairs.split('/');
        let date_repairs = new Date(parseFloat(_date_repairs[2]), parseFloat(_date_repairs[1]) - 1, parseFloat(_date_repairs[0]));
        console.log(date_repairs);

        let _date_Detection = fuites.Date_Detection_essay_echancete.split('/');
        let date_Detection = new Date(parseFloat(_date_Detection[2]), parseFloat(_date_Detection[1]) - 1, parseFloat(_date_Detection[0]));
        console.log(date_Detection);

        let model = {
            Electronique_detection: fuites.Electronique_detection,
            Soapy_water: fuites.Soapy_water,
            Electronique_detection_soapy_water: fuites.Electronique_detection_soapy_water,
            Nitrogene: fuites.Nitrogene,
            SousVide: fuites.SousVide,
            Founds: fuites.Founds,
            Date_founds: date_founds,
            Repairs: fuites.Repairs,
            Date_repairs: date_repairs,
            Detection_essay_echancete: fuites.Detection_essay_echancete,
            Date_Detection_essay_echancete: date_Detection
        };
        console.log('======================model fuites');
        console.log(model);
        const [fuitesId] = await this.knex('Fuites').insert(model);
        return fuitesId;
    }

    public async getfuites(fuitesId: number | string) {
        const fuites = await this.knex('Form_step3C').first('*').where({ fuitesId });
        if (!fuites) { return null; }
        return FuitesModel.fromJSON(fuites);
    }

    public async updateFuites(fuites: FuitesModel) {
        const fuitesId = fuites.FuitesId;
        await this.knex('Fuites').update({
            Electronique_detection: fuites.Electronique_detection,
            Soapy_water: fuites.Soapy_water,
            Electronique_detection_soapy_water: fuites.Electronique_detection_soapy_water,
            Nitrogene: fuites.Nitrogene,
            SousVide: fuites.SousVide,
            Founds: fuites.Founds,
            Date_founds: fuites.Date_founds,
            Repairs: fuites.Repairs,
            Date_repairs: fuites.Date_repairs,
            Detection_essay_echancete: fuites.Detection_essay_echancete,
            Date_Detection_essay_echancete: fuites.Date_Detection_essay_echancete
        }).where({ fuitesId });
    }
}
