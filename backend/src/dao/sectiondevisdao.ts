import { SectionDevisModel } from '../../../common';
import { DBProvider } from '../dbprovider';

export class SectionDevisDAO {
    private knex = DBProvider.getKnexConnection();

    public async createSection_Devis(devis: SectionDevisModel) {

        const [devisId] = await this.knex('Section_Devis').insert({
            Section_DevisId: devis.Section_DevisId,
            BaseId: devis.BaseId,
            Numero: devis.Numero,
            GroupId: devis.GroupId,
            DocDestinationPath: devis.DocDestinationPath,
            Titre: devis.Titre,
            Creation_Date: new Date(),
            update_Date: new Date(),
            Delete_Date: new Date(),
            IsDelete: devis.IsDelete
        });
        return devisId;
    }

    public async getSection_Devis(Section_DevisId: number | string) {
        const devis = await this.knex('Section_Devis').first('*').where({ Section_DevisId });
        if (!devis) { return null; }
        return SectionDevisModel.fromJSON(devis);
    }

    public async getSection_DevisBaseId(BaseId: number | string) {
        const deviss = await this.knex('Section_Devis').where({ BaseId });
        if (!deviss) { return null; }
        return deviss.map(SectionDevisModel.fromJSON);
    }

    public async getSectionDevisByGroupId(BaseId: number | string, GroupId: number | string) {
        const deviss = await this.knex('Section_Devis')
            .where({
                BaseId: BaseId,
                GroupId: GroupId
            });
        if (!deviss) { return null; }
        return deviss.map(SectionDevisModel.fromJSON);
    }

    public async updateSection_Devis(devis: SectionDevisModel) {
        const devisId = devis.Section_DevisId;
        await this.knex('Section_Devis').update({
            Numero: devis.Numero,
            GroupId: devis.GroupId,
            DocDestinationPath: devis.DocDestinationPath,
            Titre: devis.Titre,
            BaseId: devis.BaseId,
            update_Date: new Date(),
        }).where({ devisId });
    }

    public async deleteSection_Devis(devisId: number) {
        await this.knex('Section_Devis').update({
            IsDelete: true,
            update_Date: new Date(),
            Delete_Date: new Date()
        }).where({ devisId });
    }

    public async getSection_Deviss() {
        const deviss = await this.knex('Section_Devis').where('IsDelete', '=', false);

        return deviss.map(SectionDevisModel.fromJSON);
    }
}
