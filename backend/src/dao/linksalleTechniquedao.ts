import { LinksalleTechniqueModel } from '../../../common/';
import { DBProvider } from '../dbprovider';

export class LinkSalleTechniqueDao {
    private knex = DBProvider.getKnexConnection();

    public async create(model: LinksalleTechniqueModel) {
        const [linkId] = await this.knex('Link').insert({
            Name: model.Name,
            DocDestinationPath: model.DocDestinationPath,
            Creation_Date: new Date(),
            update_Date: new Date(),
            IsDelete: false
        });
        return linkId;
    }

    public async getLink(LinkId: number | string) {
        const data = await this.knex('Link').first('*').where({ LinkId });
        if (!data) { return null; }
        return LinksalleTechniqueModel.fromJSON(data);
    }

    public async update(model: LinksalleTechniqueModel) {
        const LinkId = model.LinkId;
        await this.knex('Link').update({
            Name: model.Name,
            DocDestinationPath: model.DocDestinationPath,
            update_Date: new Date(),
            IsDelete: model.IsDelete
        }).where({ LinkId });
    }

    public async delete(LinkId: number) {
        await this.knex('Link').update({
            IsDelete: true,
            update_Date: new Date(),
            Delete_Date: new Date()
        }).where({ LinkId });
    }

    public async getLinks() {
        const data = await this.knex('Link').where('IsDelete', '=', false);

        return data.map(LinksalleTechniqueModel.fromJSON);
    }
}
