import { DocumentTechniqueModel } from '../../../common/dist';
import { DBProvider } from '../dbprovider';

export class DocumentSalleTechniqueDao {
    private knex = DBProvider.getKnexConnection();

    public async create(model: DocumentTechniqueModel) {

        const [docSalletechniqueId] = await this.knex('Document_Salle_Technique').insert({
            Name: model.Name,
            DocDestinationPath: model.DocDestinationPath,
            Creation_Date: new Date(),
            update_Date: new Date(),
            IsDelete: false
        });
        return docSalletechniqueId;
    }

    public async getDocumentSalleTechnique(Document_salle_techniqueId: number | string) {
        const data = await this.knex('Document_Salle_Technique').first('*').where({ Document_salle_techniqueId });
        if (!data) { return null; }
        return DocumentTechniqueModel.fromJSON(data);
    }

    public async update(model: DocumentTechniqueModel) {
        const Document_salle_techniqueId = model.DocuemntTechniqueId;
        await this.knex('Document_Salle_Technique').update({
            Name: model.Name,
            DocDestinationPath: model.DocDestinationPath,
            update_Date: new Date(),
            IsDelete: model.IsDelete
        }).where({ Document_salle_techniqueId });
    }

    public async delete(Document_salle_techniqueId: number) {
        await this.knex('Document_Salle_Technique').update({
            IsDelete: true,
            update_Date: new Date(),
            Delete_Date: new Date()
        }).where({ Document_salle_techniqueId });
    }

    public async getDocumentSalleTechniques() {
        const data = await this.knex('Document_Salle_Technique').where('IsDelete', '=', false);

        return data.map(DocumentTechniqueModel.fromJSON);
    }
}
