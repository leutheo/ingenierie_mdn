import { CompanyModel } from 'common';
import { DBProvider } from '../dbprovider';

export class CompanyDAO {
    private knex = DBProvider.getKnexConnection();

    public async createCompany(company: CompanyModel) {

        const [CompanyId] = await this.knex('company').insert({
            Name: company.Name,
            Phone_number: company.Phone_number,
            Technician_name1: company.Technician_name1,
            Technician_signature1: company.Technician_signature1,
            Technician_name2: company.Technician_name2,
            Technician_signature2: company.Technician_signature2,
            Form_step3CId: company.Form_step3CId,
        });
        return CompanyId;
    }

    public async getCompany(CompanyId: number | string) {
        const company = await this.knex('company').first('*').where({ CompanyId });
        if (!company) { return null; }
        return CompanyModel.fromJSON(company);
    }

    public async updateCompany(company: CompanyModel) {
        const companyId = company.CompanyId;
        await this.knex('company').update({
            Name: company.Name,
            Phone_number: company.Phone_number,
            Technician_name1: company.Technician_name1,
            Technician_signature1: company.Technician_signature1,
            Technician_name2: company.Technician_name2,
            Technician_signature2: company.Technician_signature2,
            Form_step3CId: company.Form_step3CId,
        }).where({ companyId });
    }

    public async deleteCompany(companyId: number) {
        await this.knex('company').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ companyId });
    }

    public async getCompanys() {
        const company = await this.knex('company').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return company.map(CompanyModel.fromJSON);
    }
}
