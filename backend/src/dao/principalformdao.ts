import { Principal_formModel } from 'common';
import { DBProvider } from '../dbprovider';

export class Principal_formDAO {
    private knex = DBProvider.getKnexConnection();

    public async createPrincipal_form(principal_form: Principal_formModel) {
        console.log(principal_form);
        const [principal_formId] = await this.knex('principal_form').insert({
            BaseId: principal_form.BaseId,
            UserId: principal_form.UserId, Creation_Date: new Date(),
            Update_Date: new Date(), Delete_Date: new Date(),
            IsDelete: principal_form.IsDelete,
        });
        return principal_formId;
    }

    public async getPrincipal_form(Principal_formId: number | string) {
        const principal_form = await this.knex('principal_form').first('*').where({ Principal_formId });
        if (!principal_form) { return null; }
        return Principal_formModel.fromJSON(principal_form);
    }

    public async updatePrincipal_form(principal_form: Principal_formModel) {
        const principal_formId = principal_form.Principal_formId;
        await this.knex('principal_form').update({
            StepId: principal_form.StepId, BaseId: principal_form.BaseId,
            Description: principal_form.Description, Creation_Date: principal_form.Creation_Date,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString(),
            IsDelete: new Date().toLocaleString(),
        }).where({ principal_formId });
    }

    public async deletePrincipal_form(Principal_formId: number) {
        await this.knex('principal_form').update({
            IsDelete: true,
            update_Date: new Date().toLocaleString(), Delete_Date: new Date().toLocaleString()
        }).where({ Principal_formId });
    }

    public async getPrincipal_forms() {
        const Principal_forms = await this.knex('Principal_form').where('IsActif', '=', true).andWhere('IsDelete', '=', false);

        return Principal_forms.map(Principal_formModel.fromJSON);
    }


}
