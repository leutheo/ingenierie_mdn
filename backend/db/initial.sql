
CREATE TABLE `ingenierie_mdn`.`Role` (
`RoleId` INT NOT NULL AUTO_INCREMENT,
`Name` VARCHAR(255) NOT NULL,
`Creation_Date` DATETIME NOT NULL,
`Update_Date` DATETIME NOT NULL,
`IsDelete` BOOLEAN NOT NULL DEFAULT 0,
PRIMARY KEY (`RoleId`));
  
INSERT INTO `ingenierie_mdn`.`Role` VALUES(default, 'Employe', SYSDATE(), SYSDATE(), 0);
INSERT INTO `ingenierie_mdn`.`Role` VALUES(default, 'Admin', SYSDATE(), SYSDATE(), 0);

CREATE TABLE `ingenierie_mdn`.`Form_step_type` (
`Form_step_typeId` INT NOT NULL AUTO_INCREMENT,
`Name` VARCHAR(255) NOT NULL,
`Creation_Date` DATETIME NOT NULL,
PRIMARY KEY (`Form_step_typeId`));

INSERT INTO `ingenierie_mdn`.`Form_step_type` VALUES(default, 'Step1', SYSDATE());
INSERT INTO `ingenierie_mdn`.`Form_step_type` VALUES(default, 'Step2', SYSDATE());
INSERT INTO `ingenierie_mdn`.`Form_step_type` VALUES(default, 'Step3', SYSDATE());
INSERT INTO `ingenierie_mdn`.`Form_step_type` VALUES(default, 'Step3A', SYSDATE());
INSERT INTO `ingenierie_mdn`.`Form_step_type` VALUES(default, 'Step3B', SYSDATE());
INSERT INTO `ingenierie_mdn`.`Form_step_type` VALUES(default, 'Step3C', SYSDATE());

CREATE TABLE `ingenierie_mdn`.`User` (
  `UserId` INT NOT NULL AUTO_INCREMENT,
  `RoleId` INT NOT NULL,
  `BaseId` INT  NULL,
  `LastName` VARCHAR(255) NOT NULL,
  `FirstName` VARCHAR(255) NOT NULL,
  `Email` VARCHAR(255) NOT NULL,
  `Password` VARCHAR(255) NOT NULL,
  `CompanyName` VARCHAR(255) NOT NULL,
  `IsActif` BOOLEAN NOT NULL DEFAULT 0,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
  PRIMARY KEY (`UserId`),
  FOREIGN KEY (`RoleId`) REFERENCES `ingenierie_mdn`.`Role`(`RoleId`));
  
CREATE TABLE `ingenierie_mdn`.`Base` (
  `BaseId` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
  PRIMARY KEY (`BaseId`));

CREATE TABLE `ingenierie_mdn`.`Contact` (
  `ContactId` INT NOT NULL AUTO_INCREMENT,
  `LastName` VARCHAR(255) NOT NULL,
  `FirstName` VARCHAR(255) NOT NULL,
  `Email` VARCHAR(255) NOT NULL,
  `Phone_Number` VARCHAR(255) NOT NULL,
  `BaseId` INT NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
  PRIMARY KEY (`ContactId`),
  FOREIGN KEY (`BaseId`) REFERENCES `ingenierie_mdn`.`Base`(`BaseId`));
  
  CREATE TABLE `ingenierie_mdn`.`Section_Devis_Groupe` (
  `Section_Devis_GroupeId` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
  PRIMARY KEY (`Section_Devis_GroupeId`));

  CREATE TABLE `ingenierie_mdn`.`Section_Devis` (
  `Section_DevisId` INT NOT NULL AUTO_INCREMENT,
  `Numero` INT NOT NULL,
  `BaseId` INT NOT NULL,
  `GroupId` INT NOT NULL,
  `DocDestinationPath` VARCHAR(255) NOT NULL,
  `Titre` VARCHAR(255) NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
  PRIMARY KEY (`Section_DevisId`),
  FOREIGN KEY (`BaseId`) REFERENCES `ingenierie_mdn`.`Base`(`BaseId`),
  FOREIGN KEY (`GroupId`) REFERENCES `ingenierie_mdn`.`Section_Devis_Groupe`(`Section_Devis_GroupeId`));
  
  CREATE TABLE `ingenierie_mdn`.`Document` (
  `DocumentId` INT NOT NULL AUTO_INCREMENT,
  `Titre` VARCHAR(255) NOT NULL,
  `Description` VARCHAR(255) NOT NULL,
  `DocDesitnationPath` VARCHAR(255) NOT NULL,
  `BaseId` INT NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0, 
  PRIMARY KEY (`DocumentId`),
  FOREIGN KEY (`BaseId`) REFERENCES `ingenierie_mdn`.`Base`(`BaseId`)
  );

  CREATE TABLE `ingenierie_mdn`.`Formulaire` (
  `FormulaireId` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `Description` VARCHAR(255) NOT NULL,
  `BaseId` INT NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0, 
  PRIMARY KEY (`FormulaireId`),
  FOREIGN KEY (`BaseId`) REFERENCES `ingenierie_mdn`.`Base`(`BaseId`));
  

  
  CREATE TABLE `ingenierie_mdn`.`Document_Salle_Technique` (
  `Document_salle_techniqueId` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `DocDestinationPath` VARCHAR(255) NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0, 
  PRIMARY KEY (`Document_salle_techniqueId`));
  
  CREATE TABLE `ingenierie_mdn`.`Link` (
  `LinkId` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `DocDestinationPath` VARCHAR(255) NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0, 
  PRIMARY KEY (`LinkId`));
  
  CREATE TABLE `ingenierie_mdn`.`Salle_Technique` (
  `Salle_TechniqueId` INT NOT NULL AUTO_INCREMENT,
  `Document_salle_techniqueId` INT NULL,
  `LinkId` INT NULL,
  `BaseId` INT NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0, 
  PRIMARY KEY (`Salle_TechniqueId`),
  FOREIGN KEY (`BaseId`) REFERENCES `ingenierie_mdn`.`Base`(`BaseId`),
  FOREIGN KEY (`Document_salle_techniqueId`) REFERENCES `ingenierie_mdn`.`Document_salle_technique`(`Document_salle_techniqueId`));
  
   CREATE TABLE `ingenierie_mdn`.`Principal_form` (
  `Principal_formId` INT NOT NULL AUTO_INCREMENT,
  `BaseId` INT NOT NULL,
  `UserId` INT NOT NULL,
  `Creation_Date` DATETIME NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0, 
  PRIMARY KEY (`Principal_formId`),
  FOREIGN KEY (`BaseId`) REFERENCES `ingenierie_mdn`.`Base`(`BaseId`),
  FOREIGN KEY (`UserId`) REFERENCES `ingenierie_mdn`.`User`(`UserId`)
  );

   CREATE TABLE `ingenierie_mdn`.`Step` (
  `StepId` INT NOT NULL AUTO_INCREMENT,
  `Principal_formId` INT NOT NULL,
  `Form_StepId` INT NOT NULL,
  `Form_Step_IsValid` BOOLEAN NOT NULL DEFAULT 0,
  `Form_step_typeId` INT NOT NULL,
  `Creation_Date` DATETIME NOT NULL,
  `Update_Date` DATETIME NULL,
  `Delete_Date` DATETIME NULL,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
   PRIMARY KEY (`StepId`),
   FOREIGN KEY (`Form_step_typeId` ) REFERENCES `ingenierie_mdn`.`Form_step_type`(`Form_step_typeId`),
   FOREIGN KEY (`Principal_formId` ) REFERENCES `ingenierie_mdn`.`Principal_form`(`Principal_formId`)
   );
  
  CREATE TABLE `ingenierie_mdn`.`Form_step1`(
  `Form_step1Id` INT NOT NULL AUTO_INCREMENT,
  `Inspector_Name` VARCHAR(255) NOT NULL,
  `Edifice` VARCHAR(255) NOT NULL,
  `Project_Number` INT NOT NULL,
  `Piece` VARCHAR(255) NOT NULL,
  `Actual_date` DATETIME NOT NULL,
  `System_Type` VARCHAR(255) NOT NULL,
  `Manufacturier` VARCHAR(255) NOT NULL,
  `Model` VARCHAR(255) NOT NULL,
  `Halocarbon_type` VARCHAR(255) NOT NULL,
  `Infos_sup` VARCHAR(255) NOT NULL,
  `Typehalocarbure` VARCHAR(255) NOT NULL,
  `GH_Name` VARCHAR(255) NOT NULL,
  `GH_Signature` VARCHAR(255) NOT NULL,
  `IsActif` BOOLEAN NOT NULL DEFAULT 0,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
   PRIMARY KEY (`Form_step1Id`));
   
   CREATE TABLE `ingenierie_mdn`.`Form_step2`(
  `Form_step2Id` INT NOT NULL AUTO_INCREMENT,
  `Actual_date` DATETIME NOT NULL,
  `Edifice` VARCHAR(255) NOT NULL,
  `Piece` VARCHAR(255) NOT NULL,
  `Company_Name` VARCHAR(255) NOT NULL,
  `Phone_Number` VARCHAR(255) NOT NULL,
  `Adress` VARCHAR(255) NOT NULL,
  `Manufacturier` VARCHAR(255) NOT NULL,
  `System_Type` VARCHAR(255) NOT NULL,
  `Model` VARCHAR(255) NOT NULL,
  `Nunber_serie` INT NULL,
  `Quantity_refrigerant` INT NULL,
  `Capacity` INT NULL,
  `Infos_sup` VARCHAR(255) NOT NULL,
  `GH_Name` VARCHAR(255) NOT NULL,
  `GH_Signature` VARCHAR(255) NOT NULL,
  `IsActif` BOOLEAN NOT NULL DEFAULT 0,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
   PRIMARY KEY (`Form_step2Id`));
   
   CREATE TABLE `ingenierie_mdn`.`Form_step3`(
  `Form_step3Id` INT NOT NULL AUTO_INCREMENT,
  `Actual_date` DATETIME NOT NULL,
  `Edifice` VARCHAR(255) NOT NULL,
  `Piece` VARCHAR(255) NOT NULL,
  `Inspector_Name` VARCHAR(255) NOT NULL,
  `Inspector_Phone_Number` VARCHAR(255) NOT NULL,
  `System_Type` VARCHAR(255) NOT NULL,
  `Project_Number` INT NOT NULL,
  `Blue_Plakette` VARCHAR(255) NOT NULL,
  `Manufacturier` VARCHAR(255) NOT NULL,
  `Model` VARCHAR(255) NOT NULL,
  `Number_serie` INT NULL,
  `Description` VARCHAR(255) NOT NULL,
  `Date_Rejet` DATETIME NULL,
  `Final_reparation` DATETIME NULL,
  `Technicien_Lastname` VARCHAR(255) NULL,
  `Technicien_Firstname` VARCHAR(255) NULL,
  `Signature` VARCHAR(255) NOT NULL,
  `IsActif` BOOLEAN NOT NULL DEFAULT 0,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
   PRIMARY KEY (`Form_step3Id`));
   
  CREATE TABLE `ingenierie_mdn`.`Form_step3A`(
  `Form_step3AId` INT NOT NULL AUTO_INCREMENT,
  `Unity_Name` VARCHAR(255) NOT NULL,
  `Name` VARCHAR(255) NOT NULL,
  `Actual_date` DATETIME NOT NULL,
  `Edifice` VARCHAR(255) NOT NULL,
  `Piece` VARCHAR(255) NOT NULL,
  `Unity_adress` VARCHAR(255) NOT NULL,
  `Retrait_reason` VARCHAR(255) NOT NULL,
  `Manufacturier` VARCHAR(255) NOT NULL,
  `Model` VARCHAR(255) NOT NULL,
  `Type_refrigerant` VARCHAR(255) NOT NULL,
  `Number_serie` INT NULL,
  `System_type` VARCHAR(255) NOT NULL,
  `GH_coments` VARCHAR(255) NULL,
  `Technicien_Lastname` VARCHAR(255) NULL,
  `Technicien_Firstname` VARCHAR(255) NULL,
  `Signature` VARCHAR(255) NOT NULL,
  `IsActif` BOOLEAN NOT NULL DEFAULT 0,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
   PRIMARY KEY (`Form_step3AId`));
   
   CREATE TABLE `ingenierie_mdn`.`Form_step3B`(
  `Form_step3BId` INT NOT NULL AUTO_INCREMENT,
  `Actual_date` DATETIME NOT NULL,
  `System_type` VARCHAR(255) NOT NULL,
  `Actual_UnityId` INT NOT NULL,
  `Unity_adress` VARCHAR(255) NOT NULL,
  `Retrait_reason` VARCHAR(255) NOT NULL,
  `Condition` VARCHAR(255) NOT NULL,
  `Technicien_Lastname` VARCHAR(255) NULL,
  `Technicien_Firstname` VARCHAR(255) NULL,
  `Signature` VARCHAR(255) NOT NULL,
  `IsActif` BOOLEAN NOT NULL DEFAULT 0,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
   PRIMARY KEY (`Form_step3BId`));
   
   CREATE TABLE `ingenierie_mdn`.`Fuites`(
  `FuitesId` INT NOT NULL AUTO_INCREMENT,
  `Electronique_detection` BOOLEAN NOT NULL DEFAULT 0,
  `Soapy_water` BOOLEAN NOT NULL DEFAULT 0,
  `Electronique_detection_soapy_water` BOOLEAN NOT NULL DEFAULT 0,
  `Nitrogene` BOOLEAN NOT NULL DEFAULT 0,
  `SousVide` BOOLEAN NOT NULL DEFAULT 0,
  `Alternative_method` BOOLEAN NOT NULL DEFAULT 0,
  `Founds` BOOLEAN NOT NULL DEFAULT 0,
  `Date_founds` DATETIME NULL,
  `Repairs` BOOLEAN NOT NULL DEFAULT 0,
  `Date_repairs` DATETIME NULL,
  `Detection_essay_echancete` BOOLEAN NOT NULL DEFAULT 0,
  `Date_Detection_essay_echancete` DATETIME NULL,
   PRIMARY KEY (`FuitesId`));
   
   CREATE TABLE `ingenierie_mdn`.`Form_step3C`(
  `Form_step3CId` INT NOT NULL AUTO_INCREMENT,
  `Edifice` VARCHAR(255) NOT NULL,
  `Piece` VARCHAR(255) NOT NULL,
  `Bon_Travail` VARCHAR(255) NOT NULL,
  `Unity_id` VARCHAR(255) NOT NULL,
  `Condition` VARCHAR(255) NOT NULL,
  `Technicien_Lastname` VARCHAR(255) NULL,
  `Technicien_Firstname` VARCHAR(255) NULL,
  `Signature` VARCHAR(255) NOT NULL,
  `FuitesId` INT NOT NULL,
  `IsActif` BOOLEAN NOT NULL DEFAULT 0,
  `IsDelete` BOOLEAN NOT NULL DEFAULT 0,
   PRIMARY KEY (`Form_step3CId`),
   FOREIGN KEY (`FuitesId`) REFERENCES `ingenierie_mdn`.`Fuites`(`FuitesId`));
   
   CREATE TABLE `ingenierie_mdn`.`Company`(
  `CompanyId` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `Phone_number` VARCHAR(255) NOT NULL,
  `Technician_name1` VARCHAR(255) NOT NULL,
  `Technician_signature1` VARCHAR(255) NOT NULL,
  `Technician_name2` VARCHAR(255) NOT NULL,
  `Technician_signature2` VARCHAR(255) NOT NULL,
  `Form_step3CId` INT NOT NULL,
   PRIMARY KEY (`CompanyId`),
   FOREIGN KEY (`Form_step3CId`) REFERENCES `ingenierie_mdn`.`Form_step3C`(`Form_step3CId`));

   CREATE TABLE `ingenierie_mdn`.`Technician`(
  `TechnicianId` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(255) NOT NULL,
  `CarteId` INT,
  `Expiration_Date` DATETIME NULL,
  `CarteId_Refrigeration_compet` VARCHAR(255) NOT NULL,
  `Expiration_Date_CarteId_Refrigeration_compet` VARCHAR(255) NOT NULL,
  `Form_step3Id` INT NOT NULL,
   PRIMARY KEY (`TechnicianId`),
   FOREIGN KEY (`Form_step3Id`) REFERENCES `ingenierie_mdn`.`Form_step3`(`Form_step3Id`));
