export interface CardItem {
    title: string,
    description: string,
    img: string,
    alt: string,
    creationDate: string,
    imgPath: string;
}
