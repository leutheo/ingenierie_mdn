import { UserModel } from "common/src";

export interface userContext {
    user?: UserModel;
}

export interface GlobalState {
    isLoggedIn: boolean;
    counter: number;
    userContext: userContext;
}

export interface GlobalReducer {
    stateReducer: GlobalState,
    testReducer: String;
}
