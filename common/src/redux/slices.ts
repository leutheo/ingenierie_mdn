import { combineReducers, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { UserModel } from '../model/usermodel';
import { purgeLocalStorage } from './localStorage';
import { GlobalState } from './type';

const defaultState: GlobalState = {
    counter: 0,
    isLoggedIn: false,
    userContext: {}
};

const stateSlice = createSlice({
    name: "state",
    initialState: defaultState,
    reducers: {
        isLoggedIn: (state, action: PayloadAction<boolean>) => {
            state.isLoggedIn = action.payload;
        },
        setUserDetails: (state, action: PayloadAction<UserModel>) => {
            state.userContext = state.userContext || {};
            state.userContext.user = action.payload;
        },
        resetState: () => defaultState,
        logOut: () => {
            // const { counter } = state;
            // console.log(counter);
            // state = defaultState;
            purgeLocalStorage();
            return defaultState;
        }
    }
});

//export actions under slices
export const {
    isLoggedIn: isUserLoggedAction,
    setUserDetails: setUserDetailActions,
    resetState: resetStateAction,
    logOut: logOutAction
} = stateSlice.actions;

//TODO: use the optimal way for combining reducer using const
//combine reducer from all slice
export const combineReducer = combineReducers({
    stateReducer: stateSlice.reducer
});
// This way of combining reducers does not work with persistedReducer
// const reducers = {
//     stateReducer: stateSlice.reducer,
//     testReducer: testSlice.reducer
// };
