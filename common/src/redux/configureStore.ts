import { configureStore } from '@reduxjs/toolkit';
import { throttle } from 'lodash';
import { persistReducer } from 'reduxjs-toolkit-persist';
import autoMergeLevel2 from 'reduxjs-toolkit-persist/lib/stateReconciler/autoMergeLevel2';
import storage from 'reduxjs-toolkit-persist/lib/storage';
import { loadState, saveState } from './localStorage';
import { combineReducer } from './slices';

// persist config
//TODO: - analyse if it is better to use sessionStorage rather then using the default localstorage
//      - anylise what is the best State Reconciler rather using autoMergeLevel2
const persistConfig = {
    key: 'root',
    storage: storage,
    stateReconciler: autoMergeLevel2,
    debug: process.env.NODE_ENV !== 'production'
};
const persistedReducer = persistReducer(persistConfig, combineReducer);

// export reducers under slices
const store = configureStore({
    reducer: persistedReducer,
    devTools: process.env.NODE_ENV !== 'production',
    preloadedState: loadState(),
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            thunk: true,
            serializableCheck: false,
        }),
});

store.subscribe(throttle(() => {
    saveState(store.getState());
}, 1000));


export default store;
