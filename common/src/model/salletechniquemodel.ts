import { DocumentTechniqueModel } from "./DocumentTechniqueModel";
import { LinksalleTechniqueModel } from "./linksalletechniquemodel";

export class SalleTechniqueModel {
    public Salle_TechniqueId: number;
    public Document_salle_techniqueId: number;
    public LinkId: number;
    public BaseId: number;
    public Creation_Date: string;
    public Update_Date: string;
    public Delete_Date: string;
    public IsDelete: boolean;
    public linkTitre: string;
    public linkDocDestinationPath: string;
    public DocDestinationPath: string;
    public documentTitre: string;
    public linkSalleTechniques: LinksalleTechniqueModel[] = [];
    public documentSalleTechniques: DocumentTechniqueModel[] = [];

    constructor() {
    }

    public static fromJSON(jsonSalleTechniqueModel: SalleTechniqueModel) {
        const salletechniqueModel = new SalleTechniqueModel;
        Object.assign(salletechniqueModel, jsonSalleTechniqueModel);
        return salletechniqueModel;
    }
}
