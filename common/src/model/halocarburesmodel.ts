export class HalocarburesModel {
    public FormulaireId: number;
    public Name: number;
    public Description: string;
    public BaseId: number;
    public Creation_Date: string;
    public Update_Date: string;
    public Delete_Date: string;
    public IsDelete: boolean;

    constructor() {
    }

    public static fromJSON(jsonHalocarburesModel: HalocarburesModel) {
        const halocarburesmodel = new HalocarburesModel;
        Object.assign(halocarburesmodel, jsonHalocarburesModel);
        return halocarburesmodel;
    }
}
