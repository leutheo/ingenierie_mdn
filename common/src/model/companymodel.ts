export class CompanyModel {
    public CompanyId: number;
    public Name: string;
    public Phone_number: string;
    public Technician_name1: string;
    public Technician_signature1: string;
    public Technician_name2: string;
    public Technician_signature2: string;
    public Form_step3CId: number;

    constructor() {

    }

    public static fromJSON(jsonCompanyModel: CompanyModel) {
        const companyModel = new CompanyModel;
        Object.assign(companyModel, jsonCompanyModel);
        return companyModel;
    }
}
