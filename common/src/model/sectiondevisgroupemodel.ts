
export class SectionDevisGroupeModel {
    public Section_Devis_GroupeId: number;
    public Name: string;
    public CreationDate: string;
    public UpdateDate: string;
    public DeleteDate: string;
    public IsDelete: boolean;


    constructor() {
    }

    public static fromJSON(jsonSectionDevisGroupeModel: SectionDevisGroupeModel) {
        const sectiondevisgroupeModel = new SectionDevisGroupeModel;
        Object.assign(sectiondevisgroupeModel, jsonSectionDevisGroupeModel);
        return sectiondevisgroupeModel;
    }
}
