export class StepModel {
  public StepId: number;
  public Principal_formId: number;
  public Form_StepId: number;
  public Form_Step_IsValid: boolean;
  public Form_step_typeId: number;
  public Creation_Date: string;
  public Update_Date: string;
  public Delete_Date: string;
  public IsDelete: boolean;

  constructor() {

  }

  public static fromJSON(jsonStepModel: StepModel) {
    const stepModel = new StepModel;
    Object.assign(stepModel, jsonStepModel);
    return stepModel;
  }
}
