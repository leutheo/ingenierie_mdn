export class FuitesModel {
    public FuitesId: number;
    public Electronique_detection: boolean;
    public Soapy_water: boolean;
    public Electronique_detection_soapy_water: boolean;
    public Nitrogene: boolean;
    public SousVide: boolean;
    public Alternative_method: boolean;
    public Founds: boolean;
    public Date_founds: string;
    public Repairs: boolean;
    public Date_repairs: string;
    public Detection_essay_echancete: boolean;
    public Date_Detection_essay_echancete: string;

    constructor() {

    }

    public static fromJSON(jsonFuitesModel: FuitesModel) {
        const fuitesModel = new FuitesModel;
        Object.assign(fuitesModel, jsonFuitesModel);
        return fuitesModel;
    }
}
