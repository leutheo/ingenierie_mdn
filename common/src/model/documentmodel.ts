import { BaseModel } from '../model';

export class DocumentModel {
    public DocumentId: number;
    public Titre: string;
    public Description: string;
    public DocDesitnationPath: string;
    public BaseId: number;
    public Creation_Date: string;
    public Update_Date: string;
    public Delete_Date: string;
    public IsDelete: boolean;
    public Bases: BaseModel[];

    constructor() {
    }

    public static fromJSON(jsonDocumentModel: DocumentModel) {
        const documentModel = new DocumentModel;
        Object.assign(documentModel, jsonDocumentModel);
        return documentModel;
    }
}
