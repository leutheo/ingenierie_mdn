import { BaseModel } from '../model';

export class ContactModel {
    public ContactId: number;
    public LastName: string;
    public FirstName: string;
    public Email: string;
    public Phone_Number: number;
    public BaseId: number;
    public Creation_Date: string;
    public Update_Date: string;
    public Delete_Date: string;
    public IsDelete: boolean;
    public Bases: BaseModel[];

    constructor() {
    }

    public static fromJSON(jsonContactModel: ContactModel) {
        const contactModel = new ContactModel;
        Object.assign(contactModel, jsonContactModel);
        return contactModel;
    }
}
