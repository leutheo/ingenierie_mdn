
export class ConnexionModel {

    public Email: string;
    public Password: string;

    constructor() {
    }

    public static fromJSON(jsonConnexionModel: ConnexionModel) {
        const connexionModel = new ConnexionModel;
        Object.assign(connexionModel, jsonConnexionModel);
        return connexionModel;
    }
}
