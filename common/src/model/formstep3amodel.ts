export class Formstep3AModel {
    public Form_step3AId: number;
    public Unity_Name: string;
    public Name: string;
    public Actual_date: string;
    public Edifice: string;
    public Piece: string;
    public Unity_adress: string;
    public Retrait_reason: string;
    public Manufacturier: string;
    public Model: string;
    public Type_refrigerant: string;
    public Number_serie: number;
    public System_type: string;
    public GH_coments: string;
    public Technicien_Lastname: string;
    public Technicien_Firstname: string;
    public Signature: string;

    constructor() {

    }

    public static fromJSON(jsonFormstep3aModel: Formstep3AModel) {
        const formstep3aModel = new Formstep3AModel;
        Object.assign(formstep3aModel, jsonFormstep3aModel);
        return formstep3aModel;
    }
}
