export class Formstep3CModel {
    public Form_step3CId: number;
    public Edifice: string;
    public Piece: string;
    public Bon_Travail: string;
    public Unity_id: number;
    public Condition: string;
    public Technicien_Lastname: string;
    public Technicien_Firstname: string;
    public Signature: string;
    public FuitesId: number;

    constructor() {

    }

    public static fromJSON(jsonFormstep3cModel: Formstep3CModel) {
        console.log('------ ----');
        console.log(jsonFormstep3cModel);
        const formstep3cModel = new Formstep3CModel;
        Object.assign(formstep3cModel, jsonFormstep3cModel);
        return formstep3cModel;
    }
}
