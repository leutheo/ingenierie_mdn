export class LinksalleTechniqueModel {
    public LinkId: number;
    public Name: string;
    public Creation_Date: string;
    public Update_Date: string;
    public Delete_Date: string;
    public IsDelete: boolean;
    public DocDestinationPath: string;

    constructor() {

    }

    public static fromJSON(jsonLinkSalleTechniqueModel: LinksalleTechniqueModel) {
        const model = new LinksalleTechniqueModel;
        Object.assign(model, jsonLinkSalleTechniqueModel);
        return model;
    }
}
