export class DocumentTechniqueModel {
    public DocuemntTechniqueId: number;
    public Name: string;
    public Creation_Date: string;
    public Update_Date: string;
    public Delete_Date: string;
    public IsDelete: boolean;
    public DocDestinationPath: string;

    constructor() {

    }

    public static fromJSON(jsonDocumentTechniqueModel: DocumentTechniqueModel) {
        const model = new DocumentTechniqueModel;
        Object.assign(model, jsonDocumentTechniqueModel);
        return model;
    }
}
