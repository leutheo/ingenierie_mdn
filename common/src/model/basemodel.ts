
export class BaseModel {
    public BaseId: number;
    public Name: string;
    public CreationDate: string;
    public UpdateDate: string;
    public DeleteDate: string;
    public IsDelete: boolean;


    constructor() {
    }

    public static fromJSON(jsonBaseModel: BaseModel) {
        const baseModel = new BaseModel;
        Object.assign(baseModel, jsonBaseModel);
        return baseModel;
    }
}
