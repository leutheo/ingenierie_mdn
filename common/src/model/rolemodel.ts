export class RoleModel {
    public RoleId: number;
    public Name: string;
    public Create_Date: string;
    public Update_Date: string;
    public IsDelete: string;
    constructor() {

    }

    public static fromJSON(jsonRoleModel: RoleModel) {
        const roleModel = new RoleModel;
        Object.assign(roleModel, jsonRoleModel);
        return roleModel;
    }
}
