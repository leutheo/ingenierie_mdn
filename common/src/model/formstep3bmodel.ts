export class Formstep3BModel {
    public Form_step3BId: number;
    public Actual_date: string;
    public System_type: string;
    public Actual_UnityId: number;
    public Unity_adress: string;
    public Retrait_reason: string;
    public Condition: string;
    public Technicien_Lastname: string;
    public Technicien_Firstname: string;
    public Signature: string;

    constructor() {

    }

    public static fromJSON(jsonFormstep3bModel: Formstep3BModel) {
        const formstep3bModel = new Formstep3BModel;
        Object.assign(formstep3bModel, jsonFormstep3bModel);
        return formstep3bModel;
    }
}
