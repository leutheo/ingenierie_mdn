export class Formstep2Model {
    public Form_step2Id: number;
    public Actual_date: string;
    public Edifice: string;
    public Piece: string;
    public Company_Name: string;
    public Phone_Number: string;
    public Adress: string;
    public Manufacturier: string;
    public System_Type: string;
    public Model: string;
    public Nunber_serie: number;
    public Quantity_refrigerant: number;
    public Capacity: number;
    public Infos_sup: string;
    public GH_Name: string;
    public GH_Signature: string;
    public IsActif: boolean;
    public IsDelete: boolean;
    public BaseId: number;
    public UserId: number;

    constructor() {

    }

    public static fromJSON(jsonFormstep2Model: Formstep2Model) {
        const formstep2Model = new Formstep2Model;
        Object.assign(formstep2Model, jsonFormstep2Model);
        return formstep2Model;
    }

}
