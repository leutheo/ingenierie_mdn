
export class Formstep3Model {
    public Form_step3Id: number;
    public Actual_date: string;
    public Edifice: string;
    public Piece: string;
    public Inspector_Name: string;
    public Inspector_Phone_Number: string;
    public System_Type: string;
    public Project_Number: number;
    public Blue_Plakette: string;
    public Manufacturier: string;
    public Model: string;
    public Number_serie: number;
    public Description: string;
    public Date_Rejet: string;
    public Final_reparation: string;
    public Technicien_Lastname: string;
    public Technicien_Firstname: string;
    public Signature: string;
    public IsActif: boolean;
    public IsDelete: boolean;
    public BaseId: number;
    public UserId: number;


    constructor() {

    }

    public static fromJSON(jsonFormstep3Model: Formstep3Model) {
        const formstep3Model = new Formstep3Model;
        Object.assign(formstep3Model, jsonFormstep3Model);
        return formstep3Model;
    }
}
