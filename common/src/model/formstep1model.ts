export class Formstep1Model {
    public Form_step1Id: number;
    public Inspector_Name: string;
    public Edifice: string;
    public Project_Number: number;
    public Piece: string;
    public Actual_date: string;
    public System_Type: string;
    public Manufacturier: string;
    public Model: string;
    public Halocarbon_type: string;
    public Typehalocarbure: string;
    public Infos_sup: string;
    public GH_Name: string;
    public GH_Signature: string;
    public IsActif: boolean;
    public IsDelete: boolean;
    public BaseId: number;
    public UserId: number;

    constructor() {

    }

    public static fromJSON(jsonFormstep1Model: Formstep1Model) {
        const formstep1Model = new Formstep1Model;
        Object.assign(formstep1Model, jsonFormstep1Model);
        return formstep1Model;
    }

}
