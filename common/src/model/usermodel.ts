export class UserModel {
    public UserId: number;
    public LastName: string;
    public FirstName: string;
    public Email: string;
    public Password: string;
    public CompanyName: string;
    public IsActif: boolean;
    public CreationDate: string;
    public UpdateDate: string;
    public DeleteDate: string;
    public IsDelete: boolean;
    public RoleId: number;
    public BaseId: number;

    constructor() {
    }

    public static fromJSON(jsonUserModel: UserModel) {
        const userModel = new UserModel;
        Object.assign(userModel, jsonUserModel);
        return userModel;
    }
}
