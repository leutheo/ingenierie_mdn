export class SectionDevisModel {
    public Section_DevisId: number;
    public Titre: string;
    public Numero: number;
    public BaseId: number;
    public GroupId: number;
    public Creation_Date: string;
    public Update_Date: string;
    public Delete_Date: string;
    public IsDelete: boolean;
    public DocDestinationPath: string;

    constructor() {
    }

    public static fromJSON(jsonSectionDevisModel: SectionDevisModel) {
        const sectiondevisModel = new SectionDevisModel;
        Object.assign(sectiondevisModel, jsonSectionDevisModel);
        return sectiondevisModel;
    }
}
