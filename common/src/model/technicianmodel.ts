export class TechnicianModel {
    public TechnicianId: number;
    public Name: string;
    public CarteId: number;
    public Expiration_Date: string;
    public CarteId_Refrigeration_compet: string;
    public Expiration_Date_CarteId_Refrigeration_compet: string;
    public Form_step3Id: number;

    constructor() {

    }

    public static fromJSON(jsonTechnicianModel: TechnicianModel) {
        const technicianModel = new TechnicianModel;
        Object.assign(technicianModel, jsonTechnicianModel);
        return technicianModel;
    }
}
