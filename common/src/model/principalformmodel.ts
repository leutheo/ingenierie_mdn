export class Principal_formModel {
    public Principal_formId: number;
    public StepId: number;
    public BaseId: number;
    public Description: string;
    public Creation_Date: string;
    public Update_Date: string;
    public Delete_Date: string;
    public IsDelete: string;
    public UserId: number;

    constructor() {

    }

    public static fromJSON(jsonPrincipal_formModel: Principal_formModel) {
        const principal_formModel = new Principal_formModel;
        Object.assign(principal_formModel, jsonPrincipal_formModel);
        return principal_formModel;
    }
}
